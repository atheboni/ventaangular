IF NOT EXISTS (SELECT 1 FROM sysdatabases WHERE name = 'SystVenta')
BEGIN
	EXEC ('CREATE DATABASE SystVenta')
	PRINT 'Se cre� la Base de Datos SystVenta'
END

BEGIN TRANSACTION ScriptSystVenta
	USE SystVenta

	/*SCHEMAS*/	
	IF NOT EXISTS (SELECT 1 FROM sys.schemas WHERE name = 'Almacen')
	BEGIN
		EXEC ('USE SystVenta')
		EXEC ('CREATE SCHEMA [Almacen]')
		PRINT 'Se cre� el Schema Almacen'
	END

	IF NOT EXISTS (SELECT 1 FROM sys.schemas WHERE name = 'Configuracion')
	BEGIN
		EXEC ('USE SystVenta')
		EXEC ('CREATE SCHEMA [Configuracion]')
		PRINT 'Se cre� el Schema Configuracion'
	END

	IF NOT EXISTS (SELECT 1 FROM sys.schemas WHERE name = 'Compras')
	BEGIN
		EXEC ('USE SystVenta')
		EXEC ('CREATE SCHEMA [Compras]')
		PRINT 'Se cre� el Schema Compras'
	END

	IF NOT EXISTS (SELECT 1 FROM sys.schemas WHERE name = 'Ventas')
	BEGIN
		EXEC ('USE SystVenta')
		EXEC ('CREATE SCHEMA [Ventas]')
		PRINT 'Se cre� el Schema Ventas'
	END

	/*TABLAS*/
	IF OBJECT_ID('Almacen.tbl_CatCategorias') IS NULL
	BEGIN
		CREATE TABLE Almacen.tbl_CatCategorias
		(
			fi_IdCatCategoria INT IDENTITY(1,1) NOT NULL,
			fc_NombreCategoria VARCHAR(250) NOT NULL,
			fb_Estatus BIT NOT NULL DEFAULT 1
			CONSTRAINT [PK_tbl_CatCategoria] PRIMARY KEY CLUSTERED(fi_IdCatCategoria)
		)
	END	

	IF OBJECT_ID('Almacen.tbl_CatMarcas') IS NULL
	BEGIN
		CREATE TABLE Almacen.tbl_CatMarcas
		(
			fi_IdCatMarca INT IDENTITY(1,1) NOT NULL,
			fc_NombreMarca VARCHAR(250) NOT NULL,
			fb_Estatus BIT NOT NULL DEFAULT 1
			CONSTRAINT [PK_tbl_CatMarca] PRIMARY KEY CLUSTERED(fi_IdCatMarca)
		)
	END	

	IF OBJECT_ID('Almacen.tbl_CatUnidadesMedida') IS NULL
	BEGIN
		CREATE TABLE Almacen.tbl_CatUnidadesMedida
		(
			fi_IdCatUnidadMedida INT IDENTITY(1,1) NOT NULL,
			fc_NombreUnidadMedida VARCHAR(250) NOT NULL,
			fb_Estatus BIT NOT NULL DEFAULT 1
			CONSTRAINT [PK_tbl_CatUnidadMedida] PRIMARY KEY CLUSTERED(fi_IdCatUnidadMedida)
		)
	END
	
	IF OBJECT_ID('Almacen.tbl_CatProductos') IS NULL
	BEGIN
		CREATE TABLE Almacen.tbl_CatProductos
		(
			fi_IdCatProducto INT IDENTITY(1,1) NOT NULL,
			fc_Codigo VARCHAR(250) NOT NULL,
			fc_NombreProducto VARCHAR(250) NOT NULL,
			fn_PrecioCompra DECIMAL(10,2) NOT NULL,
			fn_PrecioVenta DECIMAL(10,2) NOT NULL,
			fi_Stock INT NOT NULL,
			fi_StockMinimo INT NOT NULL,
			fi_IdCatCategoria INT NOT NULL,
			fi_IdCatMarca INT NOT NULL,
			fi_IdCatUnidadMedida INT NOT NULL,
			fb_Estatus BIT NOT NULL DEFAULT 1
			CONSTRAINT [PK_tbl_CatProductos] PRIMARY KEY CLUSTERED(fi_IdCatProducto)
		)
	END	

	IF OBJECT_ID('Compras.tbl_CatProveedores') IS NULL
	BEGIN
		CREATE TABLE Compras.tbl_CatProveedores
		(
			fi_IdCatProveedor INT IDENTITY(1,1) NOT NULL,
			fc_RazonSocial VARCHAR(250) NULL,
			fc_NombreProveedor VARCHAR(250) NOT NULL,
			fc_RFC VARCHAR(250) NULL,
			fc_Email VARCHAR(120) NULL,
			fc_Direccion VARCHAR(250) NOT NULL,
			fc_Telefono VARCHAR(10) NULL,
			fb_Estatus BIT NOT NULL DEFAULT 1
			CONSTRAINT [PK_tbl_CatProveedores] PRIMARY KEY CLUSTERED(fi_IdCatProveedor)
		)
	END	

	IF OBJECT_ID('Ventas.tbl_CatClientes') IS NULL
	BEGIN
		CREATE TABLE Ventas.tbl_CatClientes
		(
			fi_IdCatCliente INT IDENTITY(1,1) NOT NULL,			
			fc_NombreCliente VARCHAR(250) NOT NULL,
			fc_RFC VARCHAR(250) NULL,
			fc_Email VARCHAR(120) NULL,
			fc_Direccion VARCHAR(250) NOT NULL,
			fc_Telefono VARCHAR(10) NULL,
			fb_Estatus BIT NOT NULL DEFAULT 1
			CONSTRAINT [PK_tbl_CatClientes] PRIMARY KEY CLUSTERED(fi_IdCatCliente)
		)
	END	

	IF OBJECT_ID('Configuracion.tbl_CnfGeneral') IS NULL
	BEGIN
		CREATE TABLE Configuracion.tbl_CnfGeneral
		(
			fi_IdCnfGeneral INT IDENTITY(1,1) NOT NULL,
			fc_NombreEmpresa VARCHAR(250) NOT NULL,
			fc_Logo VARBINARY(MAX) NOT NULL,
			CONSTRAINT [PK_tbl_CnfGeneral] PRIMARY KEY CLUSTERED(fi_IdCnfGeneral)
		)
	END	

	IF OBJECT_ID('Configuracion.tbl_CatUsuarios') IS NULL
	BEGIN
		CREATE TABLE Configuracion.tbl_CatUsuarios
		(
			fi_IdCatUsuario INT IDENTITY(1,1) NOT NULL,
			fc_NombreUsuario VARCHAR(250) NOT NULL,
			fc_Usuario VARCHAR(20) NOT NULL,
			fi_NivelAcceso INT NOT NULL,
			fc_Direccion VARCHAR(250) NOT NULL,
			fc_Telefono VARCHAR(10) NULL,
			fc_Password VARCHAR(15) NOT NULL,
			fb_Estatus BIT NOT NULL DEFAULT 1
			CONSTRAINT [PK_tbl_CatUsuarios] PRIMARY KEY CLUSTERED(fi_IdCatUsuario)
		)
	END	

	IF OBJECT_ID('Configuracion.tbl_CatComprobantes') IS NULL
	BEGIN
		CREATE TABLE Configuracion.tbl_CatComprobantes
		(
			fi_IdCatComprobante INT IDENTITY(1,1) NOT NULL,
			fc_NombreComprobante VARCHAR(250) NOT NULL,
			fb_Estatus BIT NOT NULL DEFAULT 1
			CONSTRAINT [PK_tbl_CatComprobantes] PRIMARY KEY CLUSTERED(fi_IdCatComprobante)
		)
	END	

	IF OBJECT_ID('Configuracion.tbl_CatMetodosPago') IS NULL
	BEGIN
		CREATE TABLE Configuracion.tbl_CatMetodosPago
		(
			fi_IdCatMetodoPago INT IDENTITY(1,1) NOT NULL,
			fc_NombreMetodoPago VARCHAR(250) NOT NULL,
			fb_Estatus BIT NOT NULL DEFAULT 1
			CONSTRAINT [PK_tbl_CatMetodosPago] PRIMARY KEY CLUSTERED(fi_IdCatMetodoPago)
		)
	END	

	IF OBJECT_ID('Compras.tbl_CnfCompras') IS NULL
	BEGIN
		CREATE TABLE Compras.tbl_CnfCompras
		(
			fi_IdCnfCompra INT IDENTITY(1,1) NOT NULL,
			fc_NumeroCompra VARCHAR(250) NOT NULL,
			fn_DescuentoPorcentaje DECIMAL(10,2) NULL,
			fn_DescuentoCantidad DECIMAL(10,2) NULL,
			fd_FechaCompra DATETIME NOT NULL,
			fi_IdCatMetodoPago INT NOT NULL,
			fi_IdCatComprobante INT NOT NULL,
			fi_IdCatProveedor INT NOT NULL,
			fn_Total DECIMAL(10,2) NOT NULL,
			fb_Estatus BIT NOT NULL DEFAULT 1
			CONSTRAINT [PK_tbl_CnfCompras] PRIMARY KEY CLUSTERED(fi_IdCnfCompra)
		)
	END	

	IF OBJECT_ID('Compras.tbl_CnfComprasDetalle') IS NULL
	BEGIN
		CREATE TABLE Compras.tbl_CnfComprasDetalle
		(
			fi_IdCnfCompraDetalle INT IDENTITY(1,1) NOT NULL,
			fi_IdCnfCompra INT NOT NULL,
			fi_IdCatProducto INT NOT NULL,
			fi_Cantidad INT NOT NULL,
			fn_PrecioCompra DECIMAL(10,2) NOT NULL,
			fn_Total DECIMAL(10,2) NOT NULL,
			fb_Estatus BIT NOT NULL DEFAULT 1
			CONSTRAINT [PK_tbl_CnfComprasDetalle] PRIMARY KEY CLUSTERED(fi_IdCnfCompraDetalle)
		)
	END	

	IF OBJECT_ID('Ventas.tbl_CnfVentas') IS NULL
	BEGIN
		CREATE TABLE Ventas.tbl_CnfVentas
		(
			fi_IdCnfVenta INT IDENTITY(1,1) NOT NULL,
			fc_NumeroVenta VARCHAR(250) NOT NULL,
			fn_DescuentoPorcentaje DECIMAL(10,2) NULL,
			fn_DescuentoCantidad DECIMAL(10,2) NULL,
			fd_FechaVenta DATETIME NOT NULL,
			fi_IdCatMetodoPago INT NOT NULL,
			fi_IdCatComprobante INT NOT NULL,
			fi_IdCatCliente INT NOT NULL,
			fn_Total DECIMAL(10,2) NOT NULL,
			fb_Estatus BIT NOT NULL DEFAULT 1
			CONSTRAINT [PK_tbl_CnfVentas] PRIMARY KEY CLUSTERED(fi_IdCnfVenta)
		)
	END	

	IF OBJECT_ID('Ventas.tbl_CnfVentasDetalle') IS NULL
	BEGIN
		CREATE TABLE Ventas.tbl_CnfVentasDetalle
		(
			fi_IdCnfVentaDetalle INT IDENTITY(1,1) NOT NULL,
			fi_IdCnfVenta INT NOT NULL,
			fi_IdCatProducto INT NOT NULL,
			fi_Cantidad INT NOT NULL,
			fn_PrecioVenta DECIMAL(10,2) NOT NULL,
			fn_Total DECIMAL(10,2) NOT NULL,
			fb_Estatus BIT NOT NULL DEFAULT 1
			CONSTRAINT [PK_tbl_CnfVentasDetalle] PRIMARY KEY CLUSTERED(fi_IdCnfVentaDetalle)
		)
	END	
	

	/*LLAVES*/	
	IF NOT EXISTS(SELECT A.CONSTRAINT_NAME FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS A WITH(NOLOCK) WHERE A.CONSTRAINT_SCHEMA='Almacen' AND A.CONSTRAINT_NAME='FK_CatProductos_REF_CatCategorias')
	BEGIN
		ALTER TABLE Almacen.tbl_CatProductos
		ADD CONSTRAINT FK_CatProductos_REF_CatCategorias FOREIGN KEY (fi_IdCatCategoria) REFERENCES Almacen.tbl_CatCategorias (fi_IdCatCategoria)
	END	
	
	IF NOT EXISTS(SELECT A.CONSTRAINT_NAME FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS A WITH(NOLOCK) WHERE A.CONSTRAINT_SCHEMA='Almacen' AND A.CONSTRAINT_NAME='FK_CatProductos_REF_CatMarcas')
	BEGIN
		ALTER TABLE Almacen.tbl_CatProductos
		ADD CONSTRAINT FK_CatProductos_REF_CatMarcas FOREIGN KEY (fi_IdCatMarca) REFERENCES Almacen.tbl_CatMarcas (fi_IdCatMarca)
	END	
	
	IF NOT EXISTS(SELECT A.CONSTRAINT_NAME FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS A WITH(NOLOCK) WHERE A.CONSTRAINT_SCHEMA='Almacen' AND A.CONSTRAINT_NAME='FK_CatProductos_REF_CatUnidadesMedida')
	BEGIN
		ALTER TABLE Almacen.tbl_CatProductos
		ADD CONSTRAINT FK_CatProductos_REF_CatUnidadesMedida FOREIGN KEY (fi_IdCatUnidadMedida) REFERENCES Almacen.tbl_CatUnidadesMedida (fi_IdCatUnidadMedida)
	END

	IF NOT EXISTS(SELECT A.CONSTRAINT_NAME FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS A WITH(NOLOCK) WHERE A.CONSTRAINT_SCHEMA='Compras' AND A.CONSTRAINT_NAME='FK_CnfCompras_REF_CatMetodoPago')
	BEGIN
		ALTER TABLE Compras.tbl_CnfCompras
		ADD CONSTRAINT FK_CnfCompras_REF_CatMetodoPago FOREIGN KEY (fi_IdCatMetodoPago) REFERENCES Configuracion.tbl_CatMetodosPago (fi_IdCatMetodoPago)
	END

	IF NOT EXISTS(SELECT A.CONSTRAINT_NAME FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS A WITH(NOLOCK) WHERE A.CONSTRAINT_SCHEMA='Compras' AND A.CONSTRAINT_NAME='FK_CnfCompras_REF_CatComprobante')
	BEGIN
		ALTER TABLE Compras.tbl_CnfCompras
		ADD CONSTRAINT FK_CnfCompras_REF_CatComprobante FOREIGN KEY (fi_IdCatComprobante) REFERENCES Configuracion.tbl_CatComprobantes (fi_IdCatComprobante)
	END

	IF NOT EXISTS(SELECT A.CONSTRAINT_NAME FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS A WITH(NOLOCK) WHERE A.CONSTRAINT_SCHEMA='Compras' AND A.CONSTRAINT_NAME='FK_CnfCompras_REF_CatProveedor')
	BEGIN
		ALTER TABLE Compras.tbl_CnfCompras
		ADD CONSTRAINT FK_CnfCompras_REF_CatProveedor FOREIGN KEY (fi_IdCatProveedor) REFERENCES Compras.tbl_CatProveedores (fi_IdCatProveedor)
	END

	IF NOT EXISTS(SELECT A.CONSTRAINT_NAME FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS A WITH(NOLOCK) WHERE A.CONSTRAINT_SCHEMA='Compras' AND A.CONSTRAINT_NAME='FK_CnfComprasDetalle_REF_CnfCompras')
	BEGIN
		ALTER TABLE Compras.tbl_CnfComprasDetalle
		ADD CONSTRAINT FK_CnfComprasDetalle_REF_CnfCompras FOREIGN KEY (fi_IdCnfCompra) REFERENCES Compras.tbl_CnfCompras (fi_IdCnfCompra)
	END

	IF NOT EXISTS(SELECT A.CONSTRAINT_NAME FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS A WITH(NOLOCK) WHERE A.CONSTRAINT_SCHEMA='Compras' AND A.CONSTRAINT_NAME='FK_CnfComprasDetalle_REF_CatProducto')
	BEGIN
		ALTER TABLE Compras.tbl_CnfComprasDetalle
		ADD CONSTRAINT FK_CnfComprasDetalle_REF_CatProducto FOREIGN KEY (fi_IdCatProducto) REFERENCES Almacen.tbl_CatProductos (fi_IdCatProducto)
	END

	IF NOT EXISTS(SELECT A.CONSTRAINT_NAME FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS A WITH(NOLOCK) WHERE A.CONSTRAINT_SCHEMA='Ventas' AND A.CONSTRAINT_NAME='FK_CnfVentas_REF_CatMetodoPago')
	BEGIN
		ALTER TABLE Ventas.tbl_CnfVentas
		ADD CONSTRAINT FK_CnfVentas_REF_CatMetodoPago FOREIGN KEY (fi_IdCatMetodoPago) REFERENCES Configuracion.tbl_CatMetodosPago (fi_IdCatMetodoPago)
	END

	IF NOT EXISTS(SELECT A.CONSTRAINT_NAME FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS A WITH(NOLOCK) WHERE A.CONSTRAINT_SCHEMA='Ventas' AND A.CONSTRAINT_NAME='FK_CnfVentas_REF_CatComprobante')
	BEGIN
		ALTER TABLE Ventas.tbl_CnfVentas
		ADD CONSTRAINT FK_CnfVentas_REF_CatComprobante FOREIGN KEY (fi_IdCatComprobante) REFERENCES Configuracion.tbl_CatComprobantes (fi_IdCatComprobante)
	END

	IF NOT EXISTS(SELECT A.CONSTRAINT_NAME FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS A WITH(NOLOCK) WHERE A.CONSTRAINT_SCHEMA='Ventas' AND A.CONSTRAINT_NAME='FK_CnfVentas_REF_CatClientes')
	BEGIN
		ALTER TABLE Ventas.tbl_CnfVentas
		ADD CONSTRAINT FK_CnfVentas_REF_CatClientes FOREIGN KEY (fi_IdCatCliente) REFERENCES Ventas.tbl_CatClientes (fi_IdCatCliente)
	END

	IF NOT EXISTS(SELECT A.CONSTRAINT_NAME FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS A WITH(NOLOCK) WHERE A.CONSTRAINT_SCHEMA='Ventas' AND A.CONSTRAINT_NAME='FK_CnfVentasDetalle_REF_CnfVentas')
	BEGIN
		ALTER TABLE Ventas.tbl_CnfVentasDetalle
		ADD CONSTRAINT FK_CnfVentasDetalle_REF_CnfVentas FOREIGN KEY (fi_IdCnfVenta) REFERENCES Ventas.tbl_CnfVentas (fi_IdCnfVenta)
	END

	IF NOT EXISTS(SELECT A.CONSTRAINT_NAME FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS A WITH(NOLOCK) WHERE A.CONSTRAINT_SCHEMA='Ventas' AND A.CONSTRAINT_NAME='FK_CnfVentasDetalle_REF_CatProducto')
	BEGIN
		ALTER TABLE Ventas.tbl_CnfVentasDetalle
		ADD CONSTRAINT FK_CnfVentasDetalle_REF_CatProducto FOREIGN KEY (fi_IdCatProducto) REFERENCES Almacen.tbl_CatProductos (fi_IdCatProducto)
	END

	IF NOT EXISTS (SELECT 1 FROM Configuracion.tbl_CatUsuarios WHERE fc_Usuario = 'admin')
	BEGIN
		INSERT INTO Configuracion.tbl_CatUsuarios (fc_NombreUsuario, fc_Usuario, fi_NivelAcceso, fc_Direccion, fc_Password, fb_Estatus) 
		VALUES ('ADMINISTRATOR', 'admin', 1, 'DOMICILIO', 'admin', 1)
	END

	IF NOT EXISTS (SELECT 1 FROM Configuracion.tbl_CnfGeneral)
	BEGIN
		INSERT INTO Configuracion.tbl_CnfGeneral (fc_NombreEmpresa, fc_Logo) VALUES ('EMPRESA', 'LOGO')
	END

ROLLBACK TRANSACTION ScriptSystVenta