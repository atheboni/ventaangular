import { Component, AfterViewInit } from '@angular/core';
import { MainService } from './services/main.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  isLogedIn = false;

  constructor(private mainService: MainService, private router: Router) {
    mainService.changeEmitted$.subscribe(
      isLogedIn => {
        this.updateIsLogedIn(isLogedIn);
      });
  }

  updateIsLogedIn(isLogedIn) {
    this.isLogedIn = isLogedIn;
  }

  logout() {
    this.mainService.logout();    
    this.router.navigate(['/']);
  }  
}
