import { Injectable } from '@angular/core';
import { EndpointFactory } from './endpoint-factory.service';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Cliente } from '../models/ventas/cliente.model';
import { Venta } from '../models/ventas/venta.model';

@Injectable()
export class VentasEndpoint extends EndpointFactory {
  private readonly _getClientesUrl: string = "/api/Ventas/GetClientes";
  private readonly _getClienteUrl: string = "/api/Ventas/GetCliente";
  private readonly _getClienteExistUrl: string = "/api/Ventas/GetClienteExist";
  private readonly _saveClienteUrl: string = "/api/Ventas/SaveCliente";
  private readonly _deleteClienteUrl: string = "/api/Ventas/DeleteCliente";
  private readonly _bulkDeleteClienteUrl: string = "/api/Ventas/BulkDeleteCliente";

  private readonly _getVentasUrl: string = "/api/Ventas/GetVentas";
  private readonly _saveVentaUrl: string = "/api/Ventas/SaveVenta";
  private readonly _getVentaDetailUrl: string = "/api/Ventas/GetVentaDetail";
  private readonly _deleteVentaUrl: string = "/api/Ventas/DeleteVenta";

  get getClientesUrl() { return environment.baseUrl + this._getClientesUrl; }
  get getClienteUrl() { return environment.baseUrl + this._getClienteUrl; }
  get getClienteExistUrl() { return environment.baseUrl + this._getClienteExistUrl; }
  get saveClienteUrl() { return environment.baseUrl + this._saveClienteUrl; }
  get deleteClienteUrl() { return environment.baseUrl + this._deleteClienteUrl; }
  get bulkDeleteClienteUrl() { return environment.baseUrl + this._bulkDeleteClienteUrl; }

  get getVentasUrl() { return environment.baseUrl + this._getVentasUrl; }
  get saveVentaUrl() { return environment.baseUrl + this._saveVentaUrl; }
  get getVentaDetailUrl() { return environment.baseUrl + this._getVentaDetailUrl; }
  get deleteVentaUrl() { return environment.baseUrl + this._deleteVentaUrl; }

  constructor(http: HttpClient) {
    super(http);
  }

  /***************************************************************************************
                                  CLIENTE ENDPOINTS
    ***************************************************************************************/
  getClientesEndpoint<T>(): Observable<T> {
    let endpointUrl = this.getClientesUrl;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getClientesEndpoint());
      });
  }

  getExistClienteEndpoint<T>(clienteName: string): Observable<T> {

    let endpointUrl = `${this.getClienteExistUrl}/${clienteName}`;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getExistClienteEndpoint(clienteName));
      });
  }

  postSaveClienteEndpoint<T>(newCliente: Cliente): Observable<T> {
    let endpointUrl = this.saveClienteUrl;

    return this.http.post<T>(endpointUrl, JSON.stringify(newCliente), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.postSaveClienteEndpoint(newCliente));
      });
  }

  getDeleteClienteEndpoint<T>(clienteId: number): Observable<T> {
    let endpointUrl = `${this.deleteClienteUrl}/${clienteId}`;

    return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getDeleteClienteEndpoint(clienteId));
      });
  }

  getBulkDeleteClienteEndpoint<T>(clienteIds: number[]): any {
    let endpointUrl = this.bulkDeleteClienteUrl;

    return this.http.put<T>(endpointUrl, JSON.stringify(clienteIds), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getBulkDeleteClienteEndpoint(clienteIds));
      });
  }

  /***************************************************************************************
                                  VENTAS ENDPOINTS
    ***************************************************************************************/
  getVentasEndpoint<T>(): Observable<T> {
    let endpointUrl = this.getVentasUrl;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getVentasEndpoint());
      });
  }

  postSaveVentaEndpoint<T>(newVenta: Venta): Observable<T> {
    let endpointUrl = this.saveVentaUrl;

    return this.http.post<T>(endpointUrl, JSON.stringify(newVenta), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.postSaveVentaEndpoint(newVenta));
      });
  }

  getVentaDetailEndpoint<T>(ventaId: number): Observable<T> {
    let endpointUrl = `${this.getVentaDetailUrl}/${ventaId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getVentaDetailEndpoint(ventaId));
      });
  }

  getDeleteVentaEndpoint<T>(ventaId: number): Observable<T> {
    let endpointUrl = `${this.deleteVentaUrl}/${ventaId}`;

    return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getDeleteVentaEndpoint(ventaId));
      });
  }
}
