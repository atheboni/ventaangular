import { Injectable } from '@angular/core';
import { AlmacenEndpoint } from './almacen-endpoint.service';
import { Categoria } from '../models/almacen/categoria.model';
import { Marca } from '../models/almacen/marca.model';
import { Producto } from '../models/almacen/producto.model';
import { UnidadMedida } from '../models/almacen/unidadMedida.model';

@Injectable()
export class AlmacenService {
  constructor(private almacenEndpoint: AlmacenEndpoint) {

  }

  /***************************************************************************************
                                  CATEGORIA ENDPOINTS
    ***************************************************************************************/
  getCategorias() {
    return this.almacenEndpoint.getCategoriasEndpoint<Categoria[]>();
  }

  getExistCategoria(categoriaName: string) {
    return this.almacenEndpoint.getExistCategoriaEndpoint<Categoria[]>(categoriaName);
  }

  saveCategoria(newCategoria: Categoria) {
    return this.almacenEndpoint.postSaveCategoriaEndpoint<number>(newCategoria);
  }

  getDeleteCategoria(categoriaId: number) {
    return this.almacenEndpoint.getDeleteCategoriaEndpoint<number>(categoriaId);
  }

  getBulkDeleteCategoria(categoriaIds: number[]) {
    return this.almacenEndpoint.getBulkDeleteCategoriaEndpoint<number>(categoriaIds);
  }

  /***************************************************************************************
                                  MARCA ENDPOINTS
    ***************************************************************************************/
  getMarcas() {
    return this.almacenEndpoint.getMarcasEndpoint<Marca[]>();
  }

  getExistMarca(marcaName: string) {
    return this.almacenEndpoint.getExistMarcaEndpoint<Marca[]>(marcaName);
  }

  saveMarca(newMarca: Marca) {
    return this.almacenEndpoint.postSaveMarcaEndpoint<number>(newMarca);
  }

  getDeleteMarca(marcaId: number) {
    return this.almacenEndpoint.getDeleteMarcaEndpoint<number>(marcaId);
  }

  getBulkDeleteMarca(marcaIds: number[]) {
    return this.almacenEndpoint.getBulkDeleteMarcaEndpoint<number>(marcaIds);
  }

  /***************************************************************************************
                                  PRODUCTO ENDPOINTS
    ***************************************************************************************/
  getProductos() {
    return this.almacenEndpoint.getProductosEndpoint<Producto[]>();
  }

  getExistProducto(productoName: string) {
    return this.almacenEndpoint.getExistProductoEndpoint<Producto[]>(productoName);
  }

  saveProducto(newProducto: Producto) {
    return this.almacenEndpoint.postSaveProductoEndpoint<number>(newProducto);
  }

  getDeleteProducto(productoId: number) {
    return this.almacenEndpoint.getDeleteProductoEndpoint<number>(productoId);
  }

  getBulkDeleteProducto(productoIds: number[]) {
    return this.almacenEndpoint.getBulkDeleteProductoEndpoint<number>(productoIds);
  }

  /***************************************************************************************
                                  UNIDADES DE MEDIDA ENDPOINTS
    ***************************************************************************************/
  getUnidadMedidas() {
    return this.almacenEndpoint.getUnidadMedidasEndpoint<UnidadMedida[]>();
  }

  getExistUnidadMedida(unidadMedidaName: string) {
    return this.almacenEndpoint.getExistUnidadMedidaEndpoint<UnidadMedida[]>(unidadMedidaName);
  }

  saveUnidadMedida(newUnidadMedida: UnidadMedida) {
    return this.almacenEndpoint.postSaveUnidadMedidaEndpoint<number>(newUnidadMedida);
  }

  getDeleteUnidadMedida(unidadMedidaId: number) {
    return this.almacenEndpoint.getDeleteUnidadMedidaEndpoint<number>(unidadMedidaId);
  }

  getBulkDeleteUnidadMedida(unidadMedidaIds: number[]) {
    return this.almacenEndpoint.getBulkDeleteUnidadMedidaEndpoint<number>(unidadMedidaIds);
  }
}
