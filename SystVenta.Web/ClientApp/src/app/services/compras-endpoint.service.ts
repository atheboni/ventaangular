import { Injectable } from '@angular/core';
import { EndpointFactory } from './endpoint-factory.service';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Proveedor } from '../models/compras/proveedor.model';
import { Compra } from '../models/compras/compra.model';

@Injectable()
export class ComprasEndpoint extends EndpointFactory {
  private readonly _getProveedoresUrl: string = "/api/Compras/GetProveedores";
  private readonly _getProveedorUrl: string = "/api/Compras/GetProveedor";
  private readonly _getProveedorExistUrl: string = "/api/Compras/GetProveedorExist";
  private readonly _saveProveedorUrl: string = "/api/Compras/SaveProveedor";
  private readonly _deleteProveedorUrl: string = "/api/Compras/DeleteProveedor";
  private readonly _bulkDeleteProveedorUrl: string = "/api/Compras/BulkDeleteProveedor";

  private readonly _getComprasUrl: string = "/api/Compras/GetCompras";
  private readonly _saveCompraUrl: string = "/api/Compras/SaveCompra";
  private readonly _getCompraDetailUrl: string = "/api/Compras/GetCompraDetail";

  get getProveedoresUrl() { return environment.baseUrl + this._getProveedoresUrl; }
  get getProveedorUrl() { return environment.baseUrl + this._getProveedorUrl; }
  get getProveedorExistUrl() { return environment.baseUrl + this._getProveedorExistUrl; }
  get saveProveedorUrl() { return environment.baseUrl + this._saveProveedorUrl; }
  get deleteProveedorUrl() { return environment.baseUrl + this._deleteProveedorUrl; }
  get bulkDeleteProveedorUrl() { return environment.baseUrl + this._bulkDeleteProveedorUrl; }

  get getComprasUrl() { return environment.baseUrl + this._getComprasUrl; }
  get saveCompraUrl() { return environment.baseUrl + this._saveCompraUrl; }
  get getCompraDetailUrl() { return environment.baseUrl + this._getCompraDetailUrl; }

  constructor(http: HttpClient) {
    super(http);
  }

  /***************************************************************************************
                                  PROVEEDOR ENDPOINTS
    ***************************************************************************************/
  getProveedoresEndpoint<T>(): Observable<T> {
    let endpointUrl = this.getProveedoresUrl;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getProveedoresEndpoint());
      });
  }

  getExistProveedorEndpoint<T>(proveedorName: string): Observable<T> {

    let endpointUrl = `${this.getProveedorExistUrl}/${proveedorName}`;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getExistProveedorEndpoint(proveedorName));
      });
  }

  postSaveProveedorEndpoint<T>(newProveedor: Proveedor): Observable<T> {
    let endpointUrl = this.saveProveedorUrl;

    return this.http.post<T>(endpointUrl, JSON.stringify(newProveedor), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.postSaveProveedorEndpoint(newProveedor));
      });
  }

  getDeleteProveedorEndpoint<T>(proveedorId: number): Observable<T> {
    let endpointUrl = `${this.deleteProveedorUrl}/${proveedorId}`;

    return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getDeleteProveedorEndpoint(proveedorId));
      });
  }

  getBulkDeleteProveedorEndpoint<T>(proveedorIds: number[]): any {
    let endpointUrl = this.bulkDeleteProveedorUrl;

    return this.http.put<T>(endpointUrl, JSON.stringify(proveedorIds), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getBulkDeleteProveedorEndpoint(proveedorIds));
      });
  }

  /***************************************************************************************
                                  COMPRAS ENDPOINTS
    ***************************************************************************************/
  getComprasEndpoint<T>(): Observable<T> {
    let endpointUrl = this.getComprasUrl;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getComprasEndpoint());
      });
  }

  postSaveCompraEndpoint<T>(newCompra: Compra): Observable<T> {
    let endpointUrl = this.saveCompraUrl;

    return this.http.post<T>(endpointUrl, JSON.stringify(newCompra), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.postSaveCompraEndpoint(newCompra));
      });
  }

  getCompraDetailEndpoint<T>(compraId: number): Observable<T> {
    let endpointUrl = `${this.getCompraDetailUrl}/${compraId}`;    
    
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getCompraDetailEndpoint(compraId));
      });
  }
}
