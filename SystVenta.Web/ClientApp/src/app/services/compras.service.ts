import { Injectable } from '@angular/core';
import { ComprasEndpoint } from './compras-endpoint.service';
import { Proveedor } from '../models/compras/proveedor.model';
import { Compras } from '../models/compras/compras.model';
import { Compra } from '../models/compras/compra.model';

@Injectable()
export class ComprasService {
  constructor(private comprasEndpoint: ComprasEndpoint) {

  }

  /***************************************************************************************
                                  PROVEEDOR ENDPOINTS
    ***************************************************************************************/
  getProveedores() {
    return this.comprasEndpoint.getProveedoresEndpoint<Proveedor[]>();
  }

  getExistProveedor(proveedorName: string) {
    return this.comprasEndpoint.getExistProveedorEndpoint<Proveedor[]>(proveedorName);
  }

  saveProveedor(newProveedor: Proveedor) {
    return this.comprasEndpoint.postSaveProveedorEndpoint<number>(newProveedor);
  }

  getDeleteProveedor(proveedorId: number) {
    return this.comprasEndpoint.getDeleteProveedorEndpoint<number>(proveedorId);
  }

  getBulkDeleteProveedor(proveedorIds: number[]) {
    return this.comprasEndpoint.getBulkDeleteProveedorEndpoint<number>(proveedorIds);
  }

  /***************************************************************************************
                                  COMPRAS ENDPOINTS
    ***************************************************************************************/
  getCompras() {
    return this.comprasEndpoint.getComprasEndpoint<Compras[]>();
  }

  saveCompra(newCompra: Compra) {
    return this.comprasEndpoint.postSaveCompraEndpoint<number>(newCompra);
  }

  getCompraDetail(compraId: number) {
    return this.comprasEndpoint.getCompraDetailEndpoint<Compra>(compraId);
  }
}
