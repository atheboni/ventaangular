import { Injectable } from '@angular/core';
import { EndpointFactory } from './endpoint-factory.service';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Categoria } from '../models/almacen/categoria.model';
import { Marca } from '../models/almacen/marca.model';
import { Producto } from '../models/almacen/producto.model';
import { UnidadMedida } from '../models/almacen/unidadMedida.model';

@Injectable()
export class AlmacenEndpoint extends EndpointFactory {
  private readonly _getCategoriasUrl: string = "/api/Almacen/GetCategorias";
  private readonly _getCategoriaUrl: string = "/api/Almacen/GetCategoria";
  private readonly _getCategoriaExistUrl: string = "/api/Almacen/GetCategoriaExist";
  private readonly _saveCategoriaUrl: string = "/api/Almacen/SaveCategoria";
  private readonly _deleteCategoriaUrl: string = "/api/Almacen/DeleteCategoria";
  private readonly _bulkDeleteCategoriaUrl: string = "/api/Almacen/BulkDeleteCategoria";

  private readonly _getMarcasUrl: string = "/api/Almacen/GetMarcas";
  private readonly _getMarcaUrl: string = "/api/Almacen/GetMarca";
  private readonly _getMarcaExistUrl: string = "/api/Almacen/GetMarcaExist";
  private readonly _saveMarcaUrl: string = "/api/Almacen/SaveMarca";
  private readonly _deleteMarcaUrl: string = "/api/Almacen/DeleteMarca";
  private readonly _bulkDeleteMarcaUrl: string = "/api/Almacen/BulkDeleteMarca";

  private readonly _getProductosUrl: string = "/api/Almacen/GetProductos";
  private readonly _getProductoUrl: string = "/api/Almacen/GetProducto";
  private readonly _getProductoExistUrl: string = "/api/Almacen/GetProductoExist";
  private readonly _saveProductoUrl: string = "/api/Almacen/SaveProducto";
  private readonly _deleteProductoUrl: string = "/api/Almacen/DeleteProducto";
  private readonly _bulkDeleteProductoUrl: string = "/api/Almacen/BulkDeleteProducto";

  private readonly _getUnidadMedidasUrl: string = "/api/Almacen/GetUnidadesMedida";
  private readonly _getUnidadMedidaUrl: string = "/api/Almacen/GetUnidadMedida";
  private readonly _getUnidadMedidaExistUrl: string = "/api/Almacen/GetUnidadMedidaExist";
  private readonly _saveUnidadMedidaUrl: string = "/api/Almacen/SaveUnidadMedida";
  private readonly _deleteUnidadMedidaUrl: string = "/api/Almacen/DeleteUnidadMedida";
  private readonly _bulkDeleteUnidadMedidaUrl: string = "/api/Almacen/BulkDeleteUnidadMedida";

  get getCategoriasUrl() { return environment.baseUrl + this._getCategoriasUrl; }
  get getCategoriaUrl() { return environment.baseUrl + this._getCategoriaUrl; }
  get getCategoriaExistUrl() { return environment.baseUrl + this._getCategoriaExistUrl; }
  get saveCategoriaUrl() { return environment.baseUrl + this._saveCategoriaUrl; }
  get deleteCategoriaUrl() { return environment.baseUrl + this._deleteCategoriaUrl; }
  get bulkDeleteCategoriaUrl() { return environment.baseUrl + this._bulkDeleteCategoriaUrl; }

  get getMarcasUrl() { return environment.baseUrl + this._getMarcasUrl; }
  get getMarcaUrl() { return environment.baseUrl + this._getMarcaUrl; }
  get getMarcaExistUrl() { return environment.baseUrl + this._getMarcaExistUrl; }
  get saveMarcaUrl() { return environment.baseUrl + this._saveMarcaUrl; }
  get deleteMarcaUrl() { return environment.baseUrl + this._deleteMarcaUrl; }
  get bulkDeleteMarcaUrl() { return environment.baseUrl + this._bulkDeleteMarcaUrl; }

  get getProductosUrl() { return environment.baseUrl + this._getProductosUrl; }
  get getProductoUrl() { return environment.baseUrl + this._getProductoUrl; }
  get getProductoExistUrl() { return environment.baseUrl + this._getProductoExistUrl; }
  get saveProductoUrl() { return environment.baseUrl + this._saveProductoUrl; }
  get deleteProductoUrl() { return environment.baseUrl + this._deleteProductoUrl; }
  get bulkDeleteProductoUrl() { return environment.baseUrl + this._bulkDeleteProductoUrl; }

  get getUnidadMedidasUrl() { return environment.baseUrl + this._getUnidadMedidasUrl; }
  get getUnidadMedidaUrl() { return environment.baseUrl + this._getUnidadMedidaUrl; }
  get getUnidadMedidaExistUrl() { return environment.baseUrl + this._getUnidadMedidaExistUrl; }
  get saveUnidadMedidaUrl() { return environment.baseUrl + this._saveUnidadMedidaUrl; }
  get deleteUnidadMedidaUrl() { return environment.baseUrl + this._deleteUnidadMedidaUrl; }
  get bulkDeleteUnidadMedidaUrl() { return environment.baseUrl + this._bulkDeleteUnidadMedidaUrl; }

  constructor(http: HttpClient) {
    super(http);
  }

  /***************************************************************************************
                                  CATEGORIA ENDPOINTS
    ***************************************************************************************/
  getCategoriasEndpoint<T>(): Observable<T> {
    let endpointUrl = this.getCategoriasUrl;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getCategoriasEndpoint());
      });
  }

  getExistCategoriaEndpoint<T>(categoriaName: string): Observable<T> {

    let endpointUrl = `${this.getCategoriaExistUrl}/${categoriaName}`;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getExistCategoriaEndpoint(categoriaName));
      });
  }

  postSaveCategoriaEndpoint<T>(newCategoria: Categoria): Observable<T> {
    let endpointUrl = this.saveCategoriaUrl;

    return this.http.post<T>(endpointUrl, JSON.stringify(newCategoria), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.postSaveCategoriaEndpoint(newCategoria));
      });
  }

  getDeleteCategoriaEndpoint<T>(categoriaId: number): Observable<T> {
    let endpointUrl = `${this.deleteCategoriaUrl}/${categoriaId}`;

    return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getDeleteCategoriaEndpoint(categoriaId));
      });
  }

  getBulkDeleteCategoriaEndpoint<T>(categoriaIds: number[]): any {
    let endpointUrl = this.bulkDeleteCategoriaUrl;

    return this.http.put<T>(endpointUrl, JSON.stringify(categoriaIds), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getBulkDeleteCategoriaEndpoint(categoriaIds));
      });
  }

  /***************************************************************************************
                                  MARCA ENDPOINTS
    ***************************************************************************************/
  getMarcasEndpoint<T>(): Observable<T> {
    let endpointUrl = this.getMarcasUrl;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getMarcasEndpoint());
      });
  }

  getExistMarcaEndpoint<T>(marcaName: string): Observable<T> {

    let endpointUrl = `${this.getMarcaExistUrl}/${marcaName}`;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getExistMarcaEndpoint(marcaName));
      });
  }

  postSaveMarcaEndpoint<T>(newMarca: Marca): Observable<T> {
    let endpointUrl = this.saveMarcaUrl;

    return this.http.post<T>(endpointUrl, JSON.stringify(newMarca), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.postSaveMarcaEndpoint(newMarca));
      });
  }

  getDeleteMarcaEndpoint<T>(marcaId: number): Observable<T> {
    let endpointUrl = `${this.deleteMarcaUrl}/${marcaId}`;

    return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getDeleteMarcaEndpoint(marcaId));
      });
  }

  getBulkDeleteMarcaEndpoint<T>(marcaIds: number[]): any {
    let endpointUrl = this.bulkDeleteMarcaUrl;

    return this.http.put<T>(endpointUrl, JSON.stringify(marcaIds), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getBulkDeleteMarcaEndpoint(marcaIds));
      });
  }

  /***************************************************************************************
                                  PRODUCTO ENDPOINTS
    ***************************************************************************************/
  getProductosEndpoint<T>(): Observable<T> {
    let endpointUrl = this.getProductosUrl;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getProductosEndpoint());
      });
  }

  getExistProductoEndpoint<T>(productoName: string): Observable<T> {

    let endpointUrl = `${this.getProductoExistUrl}/${productoName}`;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getExistProductoEndpoint(productoName));
      });
  }

  postSaveProductoEndpoint<T>(newProducto: Producto): Observable<T> {
    let endpointUrl = this.saveProductoUrl;

    return this.http.post<T>(endpointUrl, JSON.stringify(newProducto), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.postSaveProductoEndpoint(newProducto));
      });
  }

  getDeleteProductoEndpoint<T>(productoId: number): Observable<T> {
    let endpointUrl = `${this.deleteProductoUrl}/${productoId}`;

    return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getDeleteProductoEndpoint(productoId));
      });
  }

  getBulkDeleteProductoEndpoint<T>(productoIds: number[]): any {
    let endpointUrl = this.bulkDeleteProductoUrl;

    return this.http.put<T>(endpointUrl, JSON.stringify(productoIds), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getBulkDeleteProductoEndpoint(productoIds));
      });
  }

  /***************************************************************************************
                                    UNIDADES MEDIDA ENDPOINTS
    ***************************************************************************************/
  getUnidadMedidasEndpoint<T>(): Observable<T> {
    let endpointUrl = this.getUnidadMedidasUrl;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getUnidadMedidasEndpoint());
      });
  }

  getExistUnidadMedidaEndpoint<T>(unidadMedidaName: string): Observable<T> {

    let endpointUrl = `${this.getUnidadMedidaExistUrl}/${unidadMedidaName}`;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getExistUnidadMedidaEndpoint(unidadMedidaName));
      });
  }

  postSaveUnidadMedidaEndpoint<T>(newUnidadMedida: UnidadMedida): Observable<T> {
    let endpointUrl = this.saveUnidadMedidaUrl;

    return this.http.post<T>(endpointUrl, JSON.stringify(newUnidadMedida), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.postSaveUnidadMedidaEndpoint(newUnidadMedida));
      });
  }

  getDeleteUnidadMedidaEndpoint<T>(unidadMedidaId: number): Observable<T> {
    let endpointUrl = `${this.deleteUnidadMedidaUrl}/${unidadMedidaId}`;

    return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getDeleteUnidadMedidaEndpoint(unidadMedidaId));
      });
  }

  getBulkDeleteUnidadMedidaEndpoint<T>(unidadMedidaIds: number[]): any {
    let endpointUrl = this.bulkDeleteUnidadMedidaUrl;

    return this.http.put<T>(endpointUrl, JSON.stringify(unidadMedidaIds), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getBulkDeleteUnidadMedidaEndpoint(unidadMedidaIds));
      });
  }
}
