import { Injectable } from '@angular/core';
import { ConfiguracionEndpoint } from './configuracion-endpoint.service';
import { ConfiguracionGeneral } from '../models/configuracion/configuracionGeneral.model';
import { Comprobante } from '../models/configuracion/comprobante.model';
import { MetodoPago } from '../models/configuracion/metodoPago.model';
import { Usuario } from '../models/configuracion/usuario.model';
import { Login } from '../models/configuracion/login.model';

@Injectable()
export class ConfiguracionService {
  constructor(private configuracionEndpoint: ConfiguracionEndpoint) {

  }

  /***************************************************************************************
                                 LOGIN ENDPOINT
   ***************************************************************************************/
  login(loginInfo: Login) {
    return this.configuracionEndpoint.postLoginEndpoint<Usuario[]>(loginInfo);
  }

  /***************************************************************************************
                            CONFIGURACIÓN GENERAL ENDPOINTS
    ***************************************************************************************/
  getConfiguracionGeneral() {
    return this.configuracionEndpoint.getConfiguracionGeneralEndpoint<ConfiguracionGeneral[]>();
  }
  
  saveConfiguracionGeneral(newConfiguracionGeneral: ConfiguracionGeneral) {
    return this.configuracionEndpoint.postSaveConfiguracionGeneralEndpoint<number>(newConfiguracionGeneral);
  }

  /***************************************************************************************
                                  COMPROBANTE ENDPOINTS
    ***************************************************************************************/
  getComprobantes() {
    return this.configuracionEndpoint.getComprobantesEndpoint<Comprobante[]>();
  }

  getExistComprobante(comprobanteName: string) {
    return this.configuracionEndpoint.getExistComprobanteEndpoint<Comprobante[]>(comprobanteName);
  }

  saveComprobante(newComprobante: Comprobante) {
    return this.configuracionEndpoint.postSaveComprobanteEndpoint<number>(newComprobante);
  }

  getDeleteComprobante(comprobanteId: number) {
    return this.configuracionEndpoint.getDeleteComprobanteEndpoint<number>(comprobanteId);
  }

  getBulkDeleteComprobante(comprobanteIds: number[]) {
    return this.configuracionEndpoint.getBulkDeleteComprobanteEndpoint<number>(comprobanteIds);
  }

  /***************************************************************************************
                                  METODO PAGO ENDPOINTS
    ***************************************************************************************/
  getMetodosPago() {
    return this.configuracionEndpoint.getMetodosPagoEndpoint<MetodoPago[]>();
  }

  getExistMetodoPago(metodoPagoName: string) {
    return this.configuracionEndpoint.getExistMetodoPagoEndpoint<MetodoPago[]>(metodoPagoName);
  }

  saveMetodoPago(newMetodoPago: MetodoPago) {
    return this.configuracionEndpoint.postSaveMetodoPagoEndpoint<number>(newMetodoPago);
  }

  getDeleteMetodoPago(metodoPagoId: number) {
    return this.configuracionEndpoint.getDeleteMetodoPagoEndpoint<number>(metodoPagoId);
  }

  getBulkDeleteMetodoPago(metodoPagoIds: number[]) {
    return this.configuracionEndpoint.getBulkDeleteMetodoPagoEndpoint<number>(metodoPagoIds);
  }

  /***************************************************************************************
                                  USUARIO ENDPOINTS
    ***************************************************************************************/
  getUsuarios() {
    return this.configuracionEndpoint.getUsuariosEndpoint<Usuario[]>();
  }

  getExistUsuario(usuarioName: string) {
    return this.configuracionEndpoint.getExistUsuarioEndpoint<Usuario[]>(usuarioName);
  }

  saveUsuario(newUsuario: Usuario) {
    return this.configuracionEndpoint.postSaveUsuarioEndpoint<number>(newUsuario);
  }

  getDeleteUsuario(usuarioId: number) {
    return this.configuracionEndpoint.getDeleteUsuarioEndpoint<number>(usuarioId);
  }

  getBulkDeleteUsuario(usuarioIds: number[]) {
    return this.configuracionEndpoint.getBulkDeleteUsuarioEndpoint<number>(usuarioIds);
  }
}
