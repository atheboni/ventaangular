import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';

@Injectable()
export class EndpointFactory {

  static readonly apiVersion: string = "1";

  constructor(protected http: HttpClient) {

  }

  protected handleError(error, continuation: () => Observable<any>) {
    console.log(error);
    console.log("Error de http" + error.status);
    return Observable.throw(error);
  }

  protected getRequestHeaders(): { headers: HttpHeaders | { [header: string]: string | string[]; } } {
    let headers = new HttpHeaders({
      //'Authorization': 'Bearer ' + this.authService.accessToken,
      'Content-Type': 'application/json',
      'Accept': `application/vnd.iman.v${EndpointFactory.apiVersion}+json, application/json, text/plain, */*`,
      'App-Version': '2.5.3'
    });

    return { headers: headers };
  }
}
