import { Injectable } from '@angular/core';
import { ReportesEndpoint } from './reportes-endpoint.service';
import { ReporteCompras } from '../models/reportes/reporteCompras.model';
import { ReporteVentas } from '../models/reportes/reporteVentas.model';
import { Producto } from '../models/almacen/producto.model';
import { ReporteMasVendidos } from '../models/reportes/reporteMasVendidos.model';
import { ReporteFiltros } from '../models/reportes/reporteFiltros.model';

@Injectable()
export class ReportesService {
  constructor(private reportesEndpoint: ReportesEndpoint) {

  }

  /***************************************************************************************
                                  PROVEEDOR ENDPOINTS
    ***************************************************************************************/
  getReporteCompras(reporteFiltros: ReporteFiltros) {
    return this.reportesEndpoint.getReporteComprasEndpoint<ReporteCompras[]>(reporteFiltros);
  }

  getReporteVentas(reporteFiltros: ReporteFiltros) {
    return this.reportesEndpoint.getReporteVentasEndpoint<ReporteVentas[]>(reporteFiltros);
  }

  getMayorStock() {
    return this.reportesEndpoint.getMayorStockEndpoint<Producto[]>();
  }

  getMenorStock() {
    return this.reportesEndpoint.getMenorStockEndpoint<Producto[]>();
  }

  getMasVendidos() {
    return this.reportesEndpoint.getMasVendidosEndpoint<ReporteMasVendidos[]>();
  }
}
