import { Injectable } from '@angular/core';
import { EndpointFactory } from './endpoint-factory.service';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { ReporteFiltros } from '../models/reportes/reporteFiltros.model';

@Injectable()
export class ReportesEndpoint extends EndpointFactory {
  private readonly _getReporteComprasUrl: string = "/api/Reportes/GetReporteCompras";
  private readonly _getReporteVentasUrl: string = "/api/Reportes/GetReporteVentas";
  private readonly _getMayorStockUrl: string = "/api/Reportes/GetMayorStock";
  private readonly _getMenorStockUrl: string = "/api/Reportes/GetMenorStock";
  private readonly _getMasVendidosUrl: string = "/api/Reportes/GetMasVendidos";

  get getReporteComprasUrl() { return environment.baseUrl + this._getReporteComprasUrl; }
  get getReporteVentasUrl() { return environment.baseUrl + this._getReporteVentasUrl; }
  get getMayorStockUrl() { return environment.baseUrl + this._getMayorStockUrl; }
  get getMenorStockUrl() { return environment.baseUrl + this._getMenorStockUrl; }
  get getMasVendidosUrl() { return environment.baseUrl + this._getMasVendidosUrl; }

  constructor(http: HttpClient) {
    super(http);
  }

  /***************************************************************************************
                                  PROVEEDOR ENDPOINTS
    ***************************************************************************************/
  getReporteComprasEndpoint<T>(reporteFiltros: ReporteFiltros): Observable<T> {
    let endpointUrl = this.getReporteComprasUrl;
    return this.http.post<T>(endpointUrl, JSON.stringify(reporteFiltros), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getReporteComprasEndpoint(reporteFiltros));
      });
  }

  getReporteVentasEndpoint<T>(reporteFiltros: ReporteFiltros): Observable<T> {
    let endpointUrl = this.getReporteVentasUrl;
    return this.http.post<T>(endpointUrl, JSON.stringify(reporteFiltros), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getReporteVentasEndpoint(reporteFiltros));
      });
  }

  getMayorStockEndpoint<T>(): Observable<T> {
    let endpointUrl = this.getMayorStockUrl;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getMayorStockEndpoint());
      });
  }

  getMenorStockEndpoint<T>(): Observable<T> {
    let endpointUrl = this.getMenorStockUrl;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getMenorStockEndpoint());
      });
  }

  getMasVendidosEndpoint<T>(): Observable<T> {
    let endpointUrl = this.getMasVendidosUrl;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getMasVendidosEndpoint());
      });
  }
}
