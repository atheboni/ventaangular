import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { LocalStoreManager } from './local-store-manager.service';
import { Usuario } from '../models/configuracion/usuario.model';
import { Router } from '@angular/router';
import { ConfiguracionGeneral } from '../models/configuracion/configuracionGeneral.model';

@Injectable()
export class MainService {
  constructor(private localStorage: LocalStoreManager, private router: Router) {

  }

  // Observable string sources
  private emitChangeSource = new Subject<any>();
  // Observable string streams
  changeEmitted$ = this.emitChangeSource.asObservable();
  // Service message commands
  emitChange(count: any) {
    this.emitChangeSource.next(count);
  }

  login(user?: Usuario, configuracionGeneral?: ConfiguracionGeneral) {
    if (user) {
      this.localStorage.saveSyncedSessionData(user, 'current_user');
    }

    if (configuracionGeneral) {
      this.localStorage.saveSyncedSessionData(configuracionGeneral, 'configuracion_general');
    }
    this.emitChangeSource.next(true);
  }

  logout() {

    var allsuspects = document.getElementsByTagName("script")
    for (var i = allsuspects.length; i >= 0; i--) { //search backwards within nodelist for matching elements to remove
      if (allsuspects[i] && allsuspects[i].getAttribute("src") != null && allsuspects[i].getAttribute("src").indexOf("hoe.js") != -1)
        allsuspects[i].parentNode.removeChild(allsuspects[i]) //remove element by calling parentNode.removeChild()
    }


    this.localStorage.deleteData('current_user');
    this.localStorage.deleteData('configuracion_general');
    this.emitChangeSource.next(false);
  }

  get nombreUsuario() {
    var usuario = this.localStorage.getDataObject<Usuario>('current_user');
    if (usuario != null) {
      return usuario.fc_NombreUsuario;
    }
    else {
      return '';
    }
  }

  set nombreUsuario(_nombreUsuario: string) {
    var usuario = this.localStorage.getDataObject<Usuario>('current_user');
    if (usuario != null) {
      usuario.fc_NombreUsuario = _nombreUsuario;

      this.localStorage.saveSyncedSessionData(usuario, 'current_user');
      this.emitChangeSource.next(true);
    }
  }

  get nivelUsuario() {
    var usuario = this.localStorage.getDataObject<Usuario>('current_user');
    if (usuario != null) {
      return usuario.fi_NivelAcceso;
    }
    else {
      return 0;
    }
  }

  get nombreEmpresa() {
    var ConfiguracionGeneral = this.localStorage.getDataObject<ConfiguracionGeneral>('configuracion_general');
    if (ConfiguracionGeneral != null) {
      return ConfiguracionGeneral.fc_NombreEmpresa;
    }
    else {
      return '';
    }
  }

  set nombreEmpresa(_nombreEmpresa: string) {
    var ConfiguracionGeneral = this.localStorage.getDataObject<ConfiguracionGeneral>('configuracion_general');
    if (ConfiguracionGeneral != null) {
      ConfiguracionGeneral.fc_NombreEmpresa = _nombreEmpresa;

      this.localStorage.saveSyncedSessionData(ConfiguracionGeneral, 'configuracion_general');
      this.emitChangeSource.next(true);
    }
  }

  get logo() {
    var ConfiguracionGeneral = this.localStorage.getDataObject<ConfiguracionGeneral>('configuracion_general');
    if (ConfiguracionGeneral != null) {
      return 'data:image/jpeg;base64,' + ConfiguracionGeneral.imgBase64;
    }
    else {
      return '';
    }
  }

  set logo(_logo: string) {
    var ConfiguracionGeneral = this.localStorage.getDataObject<ConfiguracionGeneral>('configuracion_general');
    if (ConfiguracionGeneral != null) {
      ConfiguracionGeneral.imgBase64 = _logo;

      this.localStorage.saveSyncedSessionData(ConfiguracionGeneral, 'configuracion_general');
      this.emitChangeSource.next(true);
    }
  }  
}
