import { Injectable } from '@angular/core';
import {
  Router, CanActivate, ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { LocalStoreManager } from './local-store-manager.service';
import { Usuario } from '../models/configuracion/usuario.model';
import { MainService } from './main.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private localStorage: LocalStoreManager, private mainService: MainService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.localStorage.getDataObject<Usuario>('current_user') != null) {     

      this.mainService.login();
      // logged in so return true
      return true;
    }

    // not logged in so redirect to login page with the return url
    this.router.navigate(['/']);
    return false;
  }
}
