import { Injectable } from '@angular/core';
import { EndpointFactory } from './endpoint-factory.service';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Comprobante } from '../models/configuracion/comprobante.model';
import { MetodoPago } from '../models/configuracion/metodoPago.model';
import { Usuario } from '../models/configuracion/usuario.model';
import { Login } from '../models/configuracion/login.model';
import { ConfiguracionGeneral } from '../models/configuracion/configuracionGeneral.model';

@Injectable()
export class ConfiguracionEndpoint extends EndpointFactory {
  private readonly _getLoginUrl: string = "/api/Configuracion/Login";

  private readonly _getConfiguracionGeneralUrl: string = "/api/Configuracion/GetConfiguracionGeneral";
  private readonly _saveConfiguracionGeneralUrl: string = "/api/Configuracion/SaveConfiguracionGeneral";

  private readonly _getComprobantesUrl: string = "/api/Configuracion/GetComprobantes";
  private readonly _getComprobanteUrl: string = "/api/Configuracion/GetComprobante";
  private readonly _getComprobanteExistUrl: string = "/api/Configuracion/GetComprobanteExist";
  private readonly _saveComprobanteUrl: string = "/api/Configuracion/SaveComprobante";
  private readonly _deleteComprobanteUrl: string = "/api/Configuracion/DeleteComprobante";
  private readonly _bulkDeleteComprobanteUrl: string = "/api/Configuracion/BulkDeleteComprobante";

  private readonly _getMetodosPagoUrl: string = "/api/Configuracion/GetMetodosPago";
  private readonly _getMetodoPagoUrl: string = "/api/Configuracion/GetMetodoPago";
  private readonly _getMetodoPagoExistUrl: string = "/api/Configuracion/GetMetodoPagoExist";
  private readonly _saveMetodoPagoUrl: string = "/api/Configuracion/SaveMetodoPago";
  private readonly _deleteMetodoPagoUrl: string = "/api/Configuracion/DeleteMetodoPago";
  private readonly _bulkDeleteMetodoPagoUrl: string = "/api/Configuracion/BulkDeleteMetodoPago";

  private readonly _getUsuariosUrl: string = "/api/Configuracion/GetUsuarios";
  private readonly _getUsuarioUrl: string = "/api/Configuracion/GetUsuario";
  private readonly _getUsuarioExistUrl: string = "/api/Configuracion/GetUsuarioExist";
  private readonly _saveUsuarioUrl: string = "/api/Configuracion/SaveUsuario";
  private readonly _deleteUsuarioUrl: string = "/api/Configuracion/DeleteUsuario";
  private readonly _bulkDeleteUsuarioUrl: string = "/api/Configuracion/BulkDeleteUsuario";

  get getLoginUrl() { return environment.baseUrl + this._getLoginUrl; }

  get getConfiguracionGeneralUrl() { return environment.baseUrl + this._getConfiguracionGeneralUrl; }
  get saveConfiguracionGeneralUrl() { return environment.baseUrl + this._saveConfiguracionGeneralUrl; }

  get getComprobantesUrl() { return environment.baseUrl + this._getComprobantesUrl; }
  get getComprobanteUrl() { return environment.baseUrl + this._getComprobanteUrl; }
  get getComprobanteExistUrl() { return environment.baseUrl + this._getComprobanteExistUrl; }
  get saveComprobanteUrl() { return environment.baseUrl + this._saveComprobanteUrl; }
  get deleteComprobanteUrl() { return environment.baseUrl + this._deleteComprobanteUrl; }
  get bulkDeleteComprobanteUrl() { return environment.baseUrl + this._bulkDeleteComprobanteUrl; }

  get getMetodosPagoUrl() { return environment.baseUrl + this._getMetodosPagoUrl; }
  get getMetodoPagoUrl() { return environment.baseUrl + this._getMetodoPagoUrl; }
  get getMetodoPagoExistUrl() { return environment.baseUrl + this._getMetodoPagoExistUrl; }
  get saveMetodoPagoUrl() { return environment.baseUrl + this._saveMetodoPagoUrl; }
  get deleteMetodoPagoUrl() { return environment.baseUrl + this._deleteMetodoPagoUrl; }
  get bulkDeleteMetodoPagoUrl() { return environment.baseUrl + this._bulkDeleteMetodoPagoUrl; }

  get getUsuariosUrl() { return environment.baseUrl + this._getUsuariosUrl; }
  get getUsuarioUrl() { return environment.baseUrl + this._getUsuarioUrl; }
  get getUsuarioExistUrl() { return environment.baseUrl + this._getUsuarioExistUrl; }
  get saveUsuarioUrl() { return environment.baseUrl + this._saveUsuarioUrl; }
  get deleteUsuarioUrl() { return environment.baseUrl + this._deleteUsuarioUrl; }
  get bulkDeleteUsuarioUrl() { return environment.baseUrl + this._bulkDeleteUsuarioUrl; }

  constructor(http: HttpClient) {
    super(http);
  }

  /***************************************************************************************
                                  LOGIN ENDPOINT
    ***************************************************************************************/
  postLoginEndpoint<T>(loginInfo: Login): Observable<T> {
    let endpointUrl = this.getLoginUrl;

    return this.http.post<T>(endpointUrl, JSON.stringify(loginInfo), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.postLoginEndpoint(loginInfo));
      });
  }

  /***************************************************************************************
                            CONFIGURACIÓN GENERAL ENDPOINTS
    ***************************************************************************************/
  getConfiguracionGeneralEndpoint<T>(): Observable<T> {
    let endpointUrl = this.getConfiguracionGeneralUrl;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getConfiguracionGeneralEndpoint());
      });
  }  

  postSaveConfiguracionGeneralEndpoint<T>(newConfiguracionGeneral: ConfiguracionGeneral): Observable<T> {
    let endpointUrl = this.saveConfiguracionGeneralUrl;

    return this.http.post<T>(endpointUrl, JSON.stringify(newConfiguracionGeneral), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.postSaveConfiguracionGeneralEndpoint(newConfiguracionGeneral));
      });
  }  

  /***************************************************************************************
                                  COMPROBANTE ENDPOINTS
    ***************************************************************************************/
  getComprobantesEndpoint<T>(): Observable<T> {
    let endpointUrl = this.getComprobantesUrl;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getComprobantesEndpoint());
      });
  }

  getExistComprobanteEndpoint<T>(comprobanteName: string): Observable<T> {

    let endpointUrl = `${this.getComprobanteExistUrl}/${comprobanteName}`;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getExistComprobanteEndpoint(comprobanteName));
      });
  }

  postSaveComprobanteEndpoint<T>(newComprobante: Comprobante): Observable<T> {
    let endpointUrl = this.saveComprobanteUrl;

    return this.http.post<T>(endpointUrl, JSON.stringify(newComprobante), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.postSaveComprobanteEndpoint(newComprobante));
      });
  }

  getDeleteComprobanteEndpoint<T>(comprobanteId: number): Observable<T> {
    let endpointUrl = `${this.deleteComprobanteUrl}/${comprobanteId}`;

    return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getDeleteComprobanteEndpoint(comprobanteId));
      });
  }

  getBulkDeleteComprobanteEndpoint<T>(comprobanteIds: number[]): any {
    let endpointUrl = this.bulkDeleteComprobanteUrl;

    return this.http.put<T>(endpointUrl, JSON.stringify(comprobanteIds), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getBulkDeleteComprobanteEndpoint(comprobanteIds));
      });
  }

  /***************************************************************************************
                                  METODO PAGO ENDPOINTS
    ***************************************************************************************/
  getMetodosPagoEndpoint<T>(): Observable<T> {
    let endpointUrl = this.getMetodosPagoUrl;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getMetodosPagoEndpoint());
      });
  }

  getExistMetodoPagoEndpoint<T>(metodoPagoName: string): Observable<T> {

    let endpointUrl = `${this.getMetodoPagoExistUrl}/${metodoPagoName}`;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getExistMetodoPagoEndpoint(metodoPagoName));
      });
  }

  postSaveMetodoPagoEndpoint<T>(newMetodoPago: MetodoPago): Observable<T> {
    let endpointUrl = this.saveMetodoPagoUrl;

    return this.http.post<T>(endpointUrl, JSON.stringify(newMetodoPago), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.postSaveMetodoPagoEndpoint(newMetodoPago));
      });
  }

  getDeleteMetodoPagoEndpoint<T>(metodoPagoId: number): Observable<T> {
    let endpointUrl = `${this.deleteMetodoPagoUrl}/${metodoPagoId}`;

    return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getDeleteMetodoPagoEndpoint(metodoPagoId));
      });
  }

  getBulkDeleteMetodoPagoEndpoint<T>(metodoPagoIds: number[]): any {
    let endpointUrl = this.bulkDeleteMetodoPagoUrl;

    return this.http.put<T>(endpointUrl, JSON.stringify(metodoPagoIds), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getBulkDeleteMetodoPagoEndpoint(metodoPagoIds));
      });
  }

  /***************************************************************************************
                                  USUARIO ENDPOINTS
    ***************************************************************************************/
  getUsuariosEndpoint<T>(): Observable<T> {
    let endpointUrl = this.getUsuariosUrl;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getUsuariosEndpoint());
      });
  }

  getExistUsuarioEndpoint<T>(usuarioName: string): Observable<T> {

    let endpointUrl = `${this.getUsuarioExistUrl}/${usuarioName}`;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getExistUsuarioEndpoint(usuarioName));
      });
  }

  postSaveUsuarioEndpoint<T>(newUsuario: Usuario): Observable<T> {
    let endpointUrl = this.saveUsuarioUrl;

    return this.http.post<T>(endpointUrl, JSON.stringify(newUsuario), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.postSaveUsuarioEndpoint(newUsuario));
      });
  }

  getDeleteUsuarioEndpoint<T>(usuarioId: number): Observable<T> {
    let endpointUrl = `${this.deleteUsuarioUrl}/${usuarioId}`;

    return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getDeleteUsuarioEndpoint(usuarioId));
      });
  }

  getBulkDeleteUsuarioEndpoint<T>(usuarioIds: number[]): any {
    let endpointUrl = this.bulkDeleteUsuarioUrl;

    return this.http.put<T>(endpointUrl, JSON.stringify(usuarioIds), this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getBulkDeleteUsuarioEndpoint(usuarioIds));
      });
  }
}
