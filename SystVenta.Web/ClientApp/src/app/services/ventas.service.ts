import { Injectable } from '@angular/core';
import { VentasEndpoint } from './ventas-endpoint.service';
import { Cliente } from '../models/ventas/cliente.model';
import { Ventas } from '../models/ventas/ventas.model';
import { Venta } from '../models/ventas/venta.model';

@Injectable()
export class VentasService {
  constructor(private ventasEndpoint: VentasEndpoint) {

  }

  /***************************************************************************************
                                  CLIENTE ENDPOINTS
    ***************************************************************************************/
  getClientes() {
    return this.ventasEndpoint.getClientesEndpoint<Cliente[]>();
  }

  getExistCliente(clienteName: string) {
    return this.ventasEndpoint.getExistClienteEndpoint<Cliente[]>(clienteName);
  }

  saveCliente(newCliente: Cliente) {
    return this.ventasEndpoint.postSaveClienteEndpoint<number>(newCliente);
  }

  getDeleteCliente(clienteId: number) {
    return this.ventasEndpoint.getDeleteClienteEndpoint<number>(clienteId);
  }

  getBulkDeleteCliente(clienteIds: number[]) {
    return this.ventasEndpoint.getBulkDeleteClienteEndpoint<number>(clienteIds);
  }

  /***************************************************************************************
                                  VENTAS ENDPOINTS
    ***************************************************************************************/
  getVentas() {
    return this.ventasEndpoint.getVentasEndpoint<Ventas[]>();
  }

  saveVenta(newVenta: Venta) {
    return this.ventasEndpoint.postSaveVentaEndpoint<number>(newVenta);
  }

  getVentaDetail(ventaId: number) {
    return this.ventasEndpoint.getVentaDetailEndpoint<Venta>(ventaId);
  }

  getDeleteVenta(ventaId: number) {
    return this.ventasEndpoint.getDeleteVentaEndpoint<number>(ventaId);
  }
}
