import { Component, AfterViewChecked, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ConfiguracionGeneral } from '../../../models/configuracion/configuracionGeneral.model';

import { ConfiguracionEndpoint } from '../../../services/configuracion-endpoint.service';
import { EndpointFactory } from '../../../services/endpoint-factory.service';
import { MessageService } from 'primeng/api';
import { ConfiguracionService } from '../../../services/configuracion.service';

import { HttpClient, HttpRequest, HttpEventType, HttpResponse } from '@angular/common/http'
import { environment } from '../../../../environments/environment';
import { MainService } from '../../../services/main.service';

@Component({
  selector: 'configuracion-configuracion-general',
  templateUrl: './configuracion-general.component.html',
  styleUrls: ['./configuracion-general.component.css'],
  providers: [
    EndpointFactory,
    ConfiguracionEndpoint,
    ConfiguracionService,
    MessageService
  ]
})
/** ConfiguracionGeneral component*/
export class ConfiguracionGeneralComponent implements AfterViewChecked, OnInit {
  configuracionGeneralForm: FormGroup = new FormGroup({});
  configuracionGeneral: ConfiguracionGeneral = new ConfiguracionGeneral();
  messageIcon: string = "";
  logoSrc: string = "data:image/jpeg;base64,";

  uploadedFiles: any[] = [];
  progress: number;
  message: string;

  /** ConfiguracionGeneral ctor */
  constructor(private mainService: MainService, private configuracionService: ConfiguracionService, private messageService: MessageService, private http: HttpClient) {

  }

  onUpload(event) {    

    var baseUrl = environment.baseUrl + "/api/Configuracion/UploadLogo";
    var fileName = "";

    if (event.files.length === 0)
      return;

    const formData = new FormData();

    for (let file of event.files) {
      fileName = file.name;
      formData.append('logo', file);
    }

    const uploadReq = new HttpRequest('POST', baseUrl, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress)
        this.progress = Math.round(100 * event.loaded / event.total);
      else if (event.type === HttpEventType.ResponseHeader && event.ok)
        this.reloadLogo();
    });


  }

  ngOnInit(): void {
    this.loadAllConfiguracionGeneral();

    this.configuracionGeneralForm = new FormGroup({
      nombreEmpresa: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
      logoEmpresa: new FormControl(''),
    });
  }

  ngAfterViewChecked(): void {
    if (document.querySelector('script[src="../../../assets/scripts/hoe.js"]') == null) {
      const node = document.createElement('script');
      node.src = '../../../assets/scripts/hoe.js';
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }  

  loadAllConfiguracionGeneral() {
    this.configuracionService.getConfiguracionGeneral().subscribe((configuracionGeneralResult: ConfiguracionGeneral[]) => {
      this.onAllConfiguracionGeneralsLoadSuccessful(configuracionGeneralResult);
    }, error => {
      this.showError(error);
    });
  }

  onAllConfiguracionGeneralsLoadSuccessful(configuracionGeneralResult: ConfiguracionGeneral[]) {
    if (configuracionGeneralResult.length > 0) {
      this.configuracionGeneral = configuracionGeneralResult[0];
      this.logoSrc += this.configuracionGeneral.imgBase64;
      this.mainService.nombreEmpresa = this.configuracionGeneral.fc_NombreEmpresa;
    }
  }

  reloadLogo() {
    this.configuracionService.getConfiguracionGeneral().subscribe((configuracionGeneralResult: ConfiguracionGeneral[]) => {
      if (configuracionGeneralResult.length > 0) {
        this.configuracionGeneral.fc_Logo = configuracionGeneralResult[0].fc_Logo;
        this.configuracionGeneral.imgBase64 = configuracionGeneralResult[0].imgBase64;
        this.logoSrc = "data:image/jpeg;base64," + this.configuracionGeneral.imgBase64;
        this.mainService.logo = this.configuracionGeneral.imgBase64;
      }      
    }, error => {
      this.showError(error);
    });
  }

  showError(error: any) {
    this.messageIcon = 'pi-exclamation-triangle';
    this.messageService.add({
      key: 'aceptar',
      sticky: true,
      severity: 'error',
      summary: 'Error',
      detail: error
    });
  }

  saveConfiguracionGeneral(configuracionGeneral: ConfiguracionGeneral) {
    this.configuracionService.saveConfiguracionGeneral(configuracionGeneral).subscribe((courseResult: number) => {
      this.messageIcon = 'pi-check-circle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'success',
        summary: 'Éxito',
        detail: 'Tus Cambios se han guardado con Éxito'
      });

      this.loadAllConfiguracionGeneral();
      this.configuracionGeneralForm.reset();
    }, error => {
      this.showError(error);
    });
  }

  closeError() {
    this.messageService.clear('aceptar');
  }
}
