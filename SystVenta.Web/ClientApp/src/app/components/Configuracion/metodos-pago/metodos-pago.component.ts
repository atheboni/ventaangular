import { Component, AfterViewChecked, OnInit } from '@angular/core';
import { MetodoPago } from '../../../models/configuracion/metodoPago.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ConfiguracionEndpoint } from '../../../services/configuracion-endpoint.service';
import { EndpointFactory } from '../../../services/endpoint-factory.service';
import { MessageService } from 'primeng/api';
import { ConfiguracionService } from '../../../services/configuracion.service';

@Component({
  selector: 'configuracion-metodos-pago',
  templateUrl: './metodos-pago.component.html',
  styleUrls: ['./metodos-pago.component.css'],
  providers: [
    EndpointFactory,
    ConfiguracionEndpoint,
    ConfiguracionService,
    MessageService
  ]
})
/** MetodosPago component*/
export class MetodosPagoComponent implements AfterViewChecked, OnInit {
  metodoPagosArray: MetodoPago[];
  metodoPagoSelected: MetodoPago = new MetodoPago();
  isEdit: boolean = false;
  metodoPagosForBulkDelete: number[] = [];
  metodoPagoForm: FormGroup = new FormGroup({});
  messageIcon: string = "";
  selectedMetodoPagos: MetodoPago[];

  acceptConfirm: any;

  cols: any[] = [
    { field: 'fc_NombreMetodoPago', header: 'Nombre Metodo de Pago', class: 'gridName' },
  ];

  /** MetodosPago ctor */
  constructor(private configuracionService: ConfiguracionService, private messageService: MessageService) {

  }

  ngOnInit(): void {
    this.loadAllMetodoPagos();

    this.metodoPagoForm = new FormGroup({
      metodoPagoName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
    });
  }

  ngAfterViewChecked(): void {
    if (document.querySelector('script[src="../../../assets/scripts/hoe.js"]') == null) {
      const node = document.createElement('script');
      node.src = '../../../assets/scripts/hoe.js';
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }  

  loadAllMetodoPagos() {
    this.configuracionService.getMetodosPago().subscribe((metodoPagos: MetodoPago[]) => {
      this.onAllMetodoPagosLoadSuccessful(metodoPagos);
    }, error => {
      this.showError(error);
    });
  }

  onAllMetodoPagosLoadSuccessful(metodoPagos: MetodoPago[]) {
    this.metodoPagosArray = metodoPagos;
  }

  showError(error: any) {
    this.messageIcon = 'pi-exclamation-triangle';
    this.messageService.add({
      key: 'aceptar',
      sticky: true,
      severity: 'error',
      summary: 'Error',
      detail: error
    });
  }

  newMetodoPago() {
    this.metodoPagoSelected = new MetodoPago();
    this.isEdit = true;
  }

  validateExistMetodoPago(metodoPago: MetodoPago) {
    if (typeof this.metodoPagosArray.find(x => x.fc_NombreMetodoPago.toLowerCase() == metodoPago.fc_NombreMetodoPago.toLowerCase() && x.fi_IdCatMetodoPago != metodoPago.fi_IdCatMetodoPago) !== 'undefined') {
      this.messageIcon = 'pi-exclamation-triangle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'error',
        summary: 'Error',
        detail: 'El Metodo de Pago ya existe'
      });
    }
    else {
      this.validateExistDisabledMetodoPago(metodoPago);
    }
  }

  validateExistDisabledMetodoPago(metodoPago: MetodoPago) {
    this.configuracionService.getExistMetodoPago(metodoPago.fc_NombreMetodoPago).subscribe((existentMetodoPago: MetodoPago[]) => {
      if (existentMetodoPago.length > 0) {
        this.confirmDuplicateMetodoPago(existentMetodoPago[0]);
      }
      else {
        this.saveMetodoPago(metodoPago);
      }
    }, error => {
      this.showError(error);
    });
  }

  enableMetodoPago() {
    this.metodoPagoSelected.fb_Estatus = true;
    this.saveMetodoPago(this.metodoPagoSelected);

    this.rejectConfirm();
  }

  confirmDuplicateMetodoPago(metodoPago: MetodoPago) {
    this.metodoPagoSelected = metodoPago;
    this.acceptConfirm = this.enableMetodoPago;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: 'El Metodo de Pago ya existe, pero está deshabilitado',
      detail: '¿Deseas Habilitarlo?',

    });
  }

  saveMetodoPago(metodoPago: MetodoPago) {
    this.configuracionService.saveMetodoPago(metodoPago).subscribe((courseResult: number) => {
      this.messageIcon = 'pi-check-circle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'success',
        summary: 'Éxito',
        detail: 'Tus Cambios se han guardado con Éxito'
      });

      this.loadAllMetodoPagos();
      this.metodoPagoForm.reset();
      this.isEdit = false;
    }, error => {
      this.showError(error);
    });
  }

  getMetodoPagoDetail(metodoPago: MetodoPago) {
    this.metodoPagoSelected = Object.assign({}, metodoPago);
    this.isEdit = true;
  }

  disableMetodoPago() {
    this.configuracionService.getDeleteMetodoPago(this.metodoPagoSelected.fi_IdCatMetodoPago).subscribe(results => {
      var index = -1;
      index = this.metodoPagosArray.indexOf(this.metodoPagoSelected);
      if (index > -1) {
        this.metodoPagosArray.splice(index, 1);
      }

      this.rejectConfirm();
    }, error => {
      this.showError(error);
    });
  }

  deleteMetodoPago(metodoPago: MetodoPago) {

    this.metodoPagoSelected = metodoPago;
    this.acceptConfirm = this.disableMetodoPago;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: '¿Estas seguro?',
      detail: 'Eliminar Metodo de Pago',

    });
  }

  addTobulk(checkedMetodoPago: MetodoPago) {
    if (!checkedMetodoPago.isForBulkDelete) {
      checkedMetodoPago.isForBulkDelete = true;
      this.metodoPagosForBulkDelete.push(checkedMetodoPago.fi_IdCatMetodoPago);
    } else {
      checkedMetodoPago.isForBulkDelete = false;
      var index = -1;
      index = this.metodoPagosForBulkDelete.indexOf(checkedMetodoPago.fi_IdCatMetodoPago);
      if (index > -1) {
        this.metodoPagosForBulkDelete.splice(index, 1);
      }
    }
  }

  bulkDelete() {
    this.acceptConfirm = this.deleteBulk;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: '¿Estas seguro?',
      detail: 'Eliminar Metodos de Pago',

    });
  }

  deleteBulk() {
    if (this.metodoPagosForBulkDelete.length > 0) {
      this.configuracionService.getBulkDeleteMetodoPago(this.metodoPagosForBulkDelete).subscribe(results => {
        for (var i = 0; i < this.metodoPagosForBulkDelete.length; i++) {
          var index = -1;
          index = this.metodoPagosArray.indexOf(this.metodoPagosArray.find(x => x.fi_IdCatMetodoPago == this.metodoPagosForBulkDelete[i]));
          if (index > -1) {
            this.metodoPagosArray.splice(index, 1);
          }
        }
        this.bulkDeleteCompleted();
      }, error => {
        this.showError(error);
      });
    }
  }

  bulkDeleteCompleted() {
    this.metodoPagosForBulkDelete = [];
    this.rejectConfirm();
  }

  closeError() {
    this.messageService.clear('aceptar');
  }

  rejectConfirm() {
    this.messageService.clear('confirmation');
  }
}
