import { Component, AfterViewChecked, OnInit } from '@angular/core';
import { Usuario } from '../../../models/configuracion/usuario.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ConfiguracionEndpoint } from '../../../services/configuracion-endpoint.service';
import { EndpointFactory } from '../../../services/endpoint-factory.service';
import { MessageService } from 'primeng/api';
import { ConfiguracionService } from '../../../services/configuracion.service';

@Component({
  selector: 'configuracion-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css'],
  providers: [
    EndpointFactory,
    ConfiguracionEndpoint,
    ConfiguracionService,
    MessageService
  ]
})
/** Usuarios component*/
export class UsuariosComponent implements AfterViewChecked, OnInit {
  usuariosArray: Usuario[];
  usuarioSelected: Usuario = new Usuario();
  isEdit: boolean = false;
  usuariosForBulkDelete: number[] = [];
  usuarioForm: FormGroup = new FormGroup({});
  messageIcon: string = "";
  selectedUsuarios: Usuario[];

  acceptConfirm: any;

  cols: any[] = [
    { field: 'fc_NombreUsuario', header: 'Nombre', class: 'gridName' },
    { field: 'fc_Usuario', header: 'Usuario', class: 'gridName' },
    { field: 'fi_NivelAcceso', header: 'Nivel', class: 'gridName' },
    { field: 'fc_Direccion', header: 'Dirección', class: 'gridName' },    
    { field: 'fc_Telefono', header: 'Teléfono', class: 'gridName' },
  ];

  /** Usuarios ctor */
  constructor(private configuracionService: ConfiguracionService, private messageService: MessageService) {

  }

  ngOnInit(): void {
    this.loadAllUsuarios();

    this.usuarioForm = new FormGroup({
      usuarioName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
      usuarioUserName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
      usuarioNivel: new FormControl('', [Validators.required, Validators.min(1)]),
      usuarioDireccion: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(150)]),
      usuarioTelefono: new FormControl('', [Validators.minLength(1), Validators.maxLength(100)]),
      usuarioPassword: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(100)]),
      usuarioConfPassword: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(100)]),
    });
  }

  ngAfterViewChecked(): void {
    if (document.querySelector('script[src="../../../assets/scripts/hoe.js"]') == null) {
      const node = document.createElement('script');
      node.src = '../../../assets/scripts/hoe.js';
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }  

  loadAllUsuarios() {
    this.configuracionService.getUsuarios().subscribe((usuarios: Usuario[]) => {
      this.onAllUsuariosLoadSuccessful(usuarios);
    }, error => {
      this.showError(error);
    });
  }

  onAllUsuariosLoadSuccessful(usuarios: Usuario[]) {
    this.usuariosArray = usuarios;
  }

  showError(error: any) {
    this.messageIcon = 'pi-exclamation-triangle';
    this.messageService.add({
      key: 'aceptar',
      sticky: true,
      severity: 'error',
      summary: 'Error',
      detail: error
    });
  }

  newUsuario() {
    this.usuarioSelected = new Usuario();
    this.isEdit = true;
  }

  validateExistUsuario(usuario: Usuario) {
    if (usuario.fc_Password != usuario.fc_ConfirmPassword) {
      this.messageIcon = 'pi-exclamation-triangle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'error',
        summary: 'Error',
        detail: 'Los Passwords no coinciden'
      });
    }
    else if (typeof this.usuariosArray.find(x => x.fc_Usuario.toLowerCase() == usuario.fc_Usuario.toLowerCase() && x.fi_IdCatUsuario != usuario.fi_IdCatUsuario) !== 'undefined') {
      this.messageIcon = 'pi-exclamation-triangle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'error',
        summary: 'Error',
        detail: 'El Usuario ya existe'
      });
    }
    else {
      this.validateExistDisabledUsuario(usuario);
    }
  }

  validateExistDisabledUsuario(usuario: Usuario) {
    this.configuracionService.getExistUsuario(usuario.fc_Usuario).subscribe((existentUsuarios: Usuario[]) => {
      if (existentUsuarios.length > 0) {
        this.confirmDuplicateUsuario(existentUsuarios[0]);
      }
      else {
        this.saveUsuario(usuario);
      }
    }, error => {
      this.showError(error);
    });
  }

  enableUsuario() {
    this.usuarioSelected.fb_Estatus = true;
    this.saveUsuario(this.usuarioSelected);

    this.rejectConfirm();
  }

  confirmDuplicateUsuario(usuario: Usuario) {
    this.usuarioSelected = usuario;
    this.acceptConfirm = this.enableUsuario;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: 'El Usuario ya existe, pero está deshabilitado',
      detail: '¿Deseas Habilitarlo?',

    });
  }

  saveUsuario(usuario: Usuario) {
    this.configuracionService.saveUsuario(usuario).subscribe((courseResult: number) => {
      this.messageIcon = 'pi-check-circle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'success',
        summary: 'Éxito',
        detail: 'Tus Cambios se han guardado con Éxito'
      });

      this.loadAllUsuarios();
      this.usuarioForm.reset();
      this.isEdit = false;
    }, error => {
      this.showError(error);
    });
  }

  getUsuarioDetail(usuario: Usuario) {
    this.usuarioSelected = Object.assign({}, usuario);
    this.usuarioSelected.fc_ConfirmPassword = this.usuarioSelected.fc_Password;
    this.isEdit = true;
  }

  disableUsuario() {
    this.configuracionService.getDeleteUsuario(this.usuarioSelected.fi_IdCatUsuario).subscribe(results => {
      var index = -1;
      index = this.usuariosArray.indexOf(this.usuarioSelected);
      if (index > -1) {
        this.usuariosArray.splice(index, 1);
      }

      this.rejectConfirm();
    }, error => {
      this.showError(error);
    });
  }

  deleteUsuario(usuario: Usuario) {

    this.usuarioSelected = usuario;
    this.acceptConfirm = this.disableUsuario;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: '¿Estas seguro?',
      detail: 'Eliminar Usuario',

    });
  }

  addTobulk(checkedUsuario: Usuario) {
    if (!checkedUsuario.isForBulkDelete) {
      checkedUsuario.isForBulkDelete = true;
      this.usuariosForBulkDelete.push(checkedUsuario.fi_IdCatUsuario);
    } else {
      checkedUsuario.isForBulkDelete = false;
      var index = -1;
      index = this.usuariosForBulkDelete.indexOf(checkedUsuario.fi_IdCatUsuario);
      if (index > -1) {
        this.usuariosForBulkDelete.splice(index, 1);
      }
    }
  }

  bulkDelete() {
    this.acceptConfirm = this.deleteBulk;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: '¿Estas seguro?',
      detail: 'Eliminar Usuarios',

    });
  }

  deleteBulk() {
    if (this.usuariosForBulkDelete.length > 0) {
      this.configuracionService.getBulkDeleteUsuario(this.usuariosForBulkDelete).subscribe(results => {
        for (var i = 0; i < this.usuariosForBulkDelete.length; i++) {
          var index = -1;
          index = this.usuariosArray.indexOf(this.usuariosArray.find(x => x.fi_IdCatUsuario == this.usuariosForBulkDelete[i]));
          if (index > -1) {
            this.usuariosArray.splice(index, 1);
          }
        }
        this.bulkDeleteCompleted();
      }, error => {
        this.showError(error);
      });
    }
  }

  bulkDeleteCompleted() {
    this.usuariosForBulkDelete = [];
    this.rejectConfirm();
  }

  closeError() {
    this.messageService.clear('aceptar');
  }

  rejectConfirm() {
    this.messageService.clear('confirmation');
  }  
}
