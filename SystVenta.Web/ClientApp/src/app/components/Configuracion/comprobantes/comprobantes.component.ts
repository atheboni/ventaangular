import { Component, AfterViewChecked, OnInit } from '@angular/core';
import { Comprobante } from '../../../models/configuracion/comprobante.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ConfiguracionEndpoint } from '../../../services/configuracion-endpoint.service';
import { EndpointFactory } from '../../../services/endpoint-factory.service';
import { MessageService } from 'primeng/api';
import { ConfiguracionService } from '../../../services/configuracion.service';


@Component({
  selector: 'configuracion-comprobantes',
  templateUrl: './comprobantes.component.html',
  styleUrls: ['./comprobantes.component.css'],
  providers: [
    EndpointFactory,
    ConfiguracionEndpoint,
    ConfiguracionService,
    MessageService
  ]
})
/** Comprobantes component*/
export class ComprobantesComponent implements AfterViewChecked, OnInit {
  comprobantesArray: Comprobante[];
  comprobanteSelected: Comprobante = new Comprobante();
  isEdit: boolean = false;
  comprobantesForBulkDelete: number[] = [];
  comprobanteForm: FormGroup = new FormGroup({});
  messageIcon: string = "";
  selectedComprobantes: Comprobante[];

  acceptConfirm: any;

  cols: any[] = [
    { field: 'fc_NombreComprobante', header: 'Nombre Comprobante', class: 'gridName' },
  ];

  /** Comprobantes ctor */
  constructor(private configuracionService: ConfiguracionService, private messageService: MessageService) {

  }

  ngOnInit(): void {
    this.loadAllComprobantes();

    this.comprobanteForm = new FormGroup({
      comprobanteName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
    });
  }

  ngAfterViewChecked(): void {
    if (document.querySelector('script[src="../../../assets/scripts/hoe.js"]') == null) {
      const node = document.createElement('script');
      node.src = '../../../assets/scripts/hoe.js';
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }  

  loadAllComprobantes() {
    this.configuracionService.getComprobantes().subscribe((comprobantes: Comprobante[]) => {
      this.onAllComprobantesLoadSuccessful(comprobantes);
    }, error => {
      this.showError(error);
    });
  }

  onAllComprobantesLoadSuccessful(comprobantes: Comprobante[]) {
    this.comprobantesArray = comprobantes;
  }

  showError(error: any) {
    this.messageIcon = 'pi-exclamation-triangle';
    this.messageService.add({
      key: 'aceptar',
      sticky: true,
      severity: 'error',
      summary: 'Error',
      detail: error
    });
  }

  newComprobante() {
    this.comprobanteSelected = new Comprobante();
    this.isEdit = true;
  }

  validateExistComprobante(comprobante: Comprobante) {
    if (typeof this.comprobantesArray.find(x => x.fc_NombreComprobante.toLowerCase() == comprobante.fc_NombreComprobante.toLowerCase() && x.fi_IdCatComprobante != comprobante.fi_IdCatComprobante) !== 'undefined') {
      this.messageIcon = 'pi-exclamation-triangle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'error',
        summary: 'Error',
        detail: 'El Comprobante ya existe'
      });
    }
    else {
      this.validateExistDisabledComprobante(comprobante);
    }
  }

  validateExistDisabledComprobante(comprobante: Comprobante) {
    this.configuracionService.getExistComprobante(comprobante.fc_NombreComprobante).subscribe((existentComprobante: Comprobante[]) => {
      if (existentComprobante.length > 0) {
        this.confirmDuplicateComprobante(existentComprobante[0]);
      }
      else {
        this.saveComprobante(comprobante);
      }
    }, error => {
      this.showError(error);
    });
  }

  enableComprobante() {
    this.comprobanteSelected.fb_Estatus = true;
    this.saveComprobante(this.comprobanteSelected);

    this.rejectConfirm();
  }

  confirmDuplicateComprobante(comprobante: Comprobante) {
    this.comprobanteSelected = comprobante;
    this.acceptConfirm = this.enableComprobante;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: 'El Comprobante ya existe, pero está deshabilitado',
      detail: '¿Deseas Habilitarlo?',

    });
  }

  saveComprobante(comprobante: Comprobante) {
    this.configuracionService.saveComprobante(comprobante).subscribe((courseResult: number) => {
      this.messageIcon = 'pi-check-circle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'success',
        summary: 'Éxito',
        detail: 'Tus Cambios se han guardado con Éxito'
      });

      this.loadAllComprobantes();
      this.comprobanteForm.reset();
      this.isEdit = false;
    }, error => {
      this.showError(error);
    });
  }

  getComprobanteDetail(comprobante: Comprobante) {
    this.comprobanteSelected = Object.assign({}, comprobante);
    this.isEdit = true;
  }

  disableComprobante() {
    this.configuracionService.getDeleteComprobante(this.comprobanteSelected.fi_IdCatComprobante).subscribe(results => {
      var index = -1;
      index = this.comprobantesArray.indexOf(this.comprobanteSelected);
      if (index > -1) {
        this.comprobantesArray.splice(index, 1);
      }

      this.rejectConfirm();
    }, error => {
      this.showError(error);
    });
  }

  deleteComprobante(comprobante: Comprobante) {

    this.comprobanteSelected = comprobante;
    this.acceptConfirm = this.disableComprobante;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: '¿Estas seguro?',
      detail: 'Eliminar Comprobante',

    });
  }

  addTobulk(checkedComprobante: Comprobante) {
    if (!checkedComprobante.isForBulkDelete) {
      checkedComprobante.isForBulkDelete = true;
      this.comprobantesForBulkDelete.push(checkedComprobante.fi_IdCatComprobante);
    } else {
      checkedComprobante.isForBulkDelete = false;
      var index = -1;
      index = this.comprobantesForBulkDelete.indexOf(checkedComprobante.fi_IdCatComprobante);
      if (index > -1) {
        this.comprobantesForBulkDelete.splice(index, 1);
      }
    }
  }

  bulkDelete() {
    this.acceptConfirm = this.deleteBulk;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: '¿Estas seguro?',
      detail: 'Eliminar Comprobantes',

    });
  }

  deleteBulk() {
    if (this.comprobantesForBulkDelete.length > 0) {
      this.configuracionService.getBulkDeleteComprobante(this.comprobantesForBulkDelete).subscribe(results => {
        for (var i = 0; i < this.comprobantesForBulkDelete.length; i++) {
          var index = -1;
          index = this.comprobantesArray.indexOf(this.comprobantesArray.find(x => x.fi_IdCatComprobante == this.comprobantesForBulkDelete[i]));
          if (index > -1) {
            this.comprobantesArray.splice(index, 1);
          }
        }
        this.bulkDeleteCompleted();
      }, error => {
        this.showError(error);
      });
    }
  }

  bulkDeleteCompleted() {
    this.comprobantesForBulkDelete = [];
    this.rejectConfirm();
  }

  closeError() {
    this.messageService.clear('aceptar');
  }

  rejectConfirm() {
    this.messageService.clear('confirmation');
  }
}
