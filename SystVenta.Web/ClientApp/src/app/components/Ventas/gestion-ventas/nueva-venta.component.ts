import { Component, OnInit, AfterViewChecked, Input, Output, EventEmitter } from '@angular/core';
import { EndpointFactory } from '../../../services/endpoint-factory.service';
import { AlmacenService } from '../../../services/almacen.service';
import { AlmacenEndpoint } from '../../../services/almacen-endpoint.service';
import { ConfiguracionService } from '../../../services/configuracion.service';
import { ConfiguracionEndpoint } from '../../../services/configuracion-endpoint.service';
import { VentasService } from '../../../services/ventas.service';
import { VentasEndpoint } from '../../../services/ventas-endpoint.service';
import { MessageService } from 'primeng/api';
import { Producto } from '../../../models/almacen/producto.model';
import { VentaDetalle } from '../../../models/ventas/ventaDetalle.model';
import { Venta } from '../../../models/ventas/venta.model';

import { Comprobante } from '../../../models/configuracion/comprobante.model';
import { MetodoPago } from '../../../models/configuracion/metodoPago.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Cliente } from '../../../models/ventas/cliente.model';

import { Router } from '@angular/router';

@Component({
  selector: 'configuracion-nueva-venta',
  templateUrl: './nueva-venta.component.html',
  styleUrls: ['./nueva-venta.component.css'],
  providers: [
    EndpointFactory,
    AlmacenEndpoint,
    AlmacenService,
    ConfiguracionEndpoint,
    ConfiguracionService,
    VentasEndpoint,
    VentasService,
    MessageService
  ]
})
/** NuevaVenta component*/
export class NuevaVentaComponent implements AfterViewChecked, OnInit {
  comprobantesArray: Comprobante[];
  metodoPagosArray: MetodoPago[];
  clientesArray: Cliente[];

  filteredProductos: any[];
  messageIcon: string = "";

  ventaForm: FormGroup = new FormGroup({});
  @Input() ventaModel: Venta = new Venta();
  @Input() isEdition: boolean = false;
  @Output() cancelEdit = new EventEmitter();

  productosArray: VentaDetalle[] = [];
  productoSelected: VentaDetalle;

  autoInput: string = "";
  acceptConfirm: any;
  closeAccept: any;

  montoRecibido: number = 0;
  montoCambio: number = 0;


  cols: any[] = [
    { field: 'fc_Codigo', header: 'Código', class: 'gridName' },
    { field: 'fc_NombreProducto', header: 'Nombre Producto', class: 'gridName' },
    { field: 'fi_Cantidad', header: 'Cantidad', class: 'Precio Venta' },
    { field: 'fn_PrecioVenta', header: 'Precio', class: 'gridName' },
    { field: 'fn_Total', header: 'Total', class: 'gridName' },
  ];

  /** NuevaVenta ctor */
  constructor(private almacenService: AlmacenService, private messageService: MessageService, private configuracionService: ConfiguracionService,
    private ventasService: VentasService, private router: Router) {

  }

  ngOnInit() {    
    this.loadAllComprobantes();
    this.loadAllMetodoPagos();
    this.loadAllClientes();

    this.ventaForm = new FormGroup({
      ventaNumero: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(4)]),
      ventaMontoRecibido: new FormControl('', [Validators.pattern("^[0-9\.]*$"), Validators.min(0), Validators.maxLength(10)]),
      ventaDescuento: new FormControl('', [Validators.pattern("^[0-9\.]*$"), Validators.min(0), Validators.max(100), Validators.maxLength(3)]),
      ventaComprobante: new FormControl('', [Validators.required, Validators.min(1)]),
      ventaMetodoPago: new FormControl('', [Validators.required, Validators.min(1)]),
      ventaCliente: new FormControl('', [Validators.required, Validators.min(1)]),
    });

    if (this.isEdition) {
      this.ventaModel.fd_FechaVenta = new Date(this.ventaModel.fd_FechaVenta);
      this.productosArray = this.ventaModel.ventaDetalles;
      this.updateTotal()
    }
  }

  ngAfterViewChecked(): void {
    if (document.querySelector('script[src="../../../assets/scripts/hoe.js"]') == null) {
      const node = document.createElement('script');
      node.src = '../../../assets/scripts/hoe.js';
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }  

  filterProduct(event) {
    let query = event.query;

    this.almacenService.getProductos().subscribe(results => {
      this.filteredProductos = this.filterProducts(query, results);
    }, error => this.showError(error));
  }

  filterProducts(query, products: Producto[]): any[] {
    let filtered: any[] = [];
    for (let i = 0; i < products.length; i++) {
      let producto = products[i];
      if (producto.fc_NombreProducto != null && ((producto.fc_NombreProducto.toLowerCase().indexOf(query.toLowerCase()) != -1) || (producto.fc_Codigo.toLowerCase().indexOf(query.toLowerCase()) != -1))) {
        producto.nombreAutocomplete = producto.fc_Codigo + ' - ' + producto.fc_NombreProducto;
        filtered.push(producto);
      }
    }
    return filtered;
  }

  productoExiste(producto: Producto) {
    var productoVenta = this.productosArray.find(x => x.fc_Codigo == producto.fc_Codigo);
    return typeof productoVenta !== 'undefined';
  }

  selectProducto(event) {
    if (this.productoExiste(event)) {
      var productoVenta = this.productosArray.find(x => x.fc_Codigo == event.fc_Codigo);
      productoVenta.fi_Cantidad += 1;
    }
    else {
      this.productoSelected = new VentaDetalle();

      this.productoSelected.fc_Codigo = event.fc_Codigo;
      this.productoSelected.fc_NombreProducto = event.fc_NombreProducto;
      this.productoSelected.fi_Cantidad = 1;
      this.productoSelected.fn_PrecioVenta = event.fn_PrecioVenta;
      this.productoSelected.fn_Total = event.fn_PrecioVenta;
      this.productoSelected.fb_Estatus = true;

      this.productosArray.push(this.productoSelected);
    }
    this.updateTotal(this.productoSelected);
    this.autoInput = "";
  }







  loadAllComprobantes() {
    this.configuracionService.getComprobantes().subscribe((comprobantes: Comprobante[]) => {
      this.onAllComprobantesLoadSuccessful(comprobantes);
    }, error => {
      this.showError(error);
    });
  }

  onAllComprobantesLoadSuccessful(comprobantes: Comprobante[]) {
    this.comprobantesArray = comprobantes;
  }

  loadAllMetodoPagos() {
    this.configuracionService.getMetodosPago().subscribe((metodoPagos: MetodoPago[]) => {
      this.onAllMetodoPagosLoadSuccessful(metodoPagos);
    }, error => {
      this.showError(error);
    });
  }

  onAllMetodoPagosLoadSuccessful(metodoPagos: MetodoPago[]) {
    this.metodoPagosArray = metodoPagos;
  }

  loadAllClientes() {
    this.ventasService.getClientes().subscribe((clientes: Cliente[]) => {
      this.onAllClientesLoadSuccessful(clientes);
    }, error => {
      this.showError(error);
    });
  }

  onAllClientesLoadSuccessful(clientes: Cliente[]) {
    this.clientesArray = clientes;
  }

  showError(error: any) {
    this.closeAccept = this.closeError;

    this.messageIcon = 'pi-exclamation-triangle';
    this.messageService.add({
      key: 'aceptar',
      sticky: true,
      severity: 'error',
      summary: 'Error',
      detail: error
    });
  }

  closeError() {
    this.messageService.clear('aceptar');
  }

  rejectConfirm() {
    this.messageService.clear('confirmation');
  }

  updateTotal(producto?: VentaDetalle) {

    if (producto) {
      producto.fn_Total = producto.fi_Cantidad * producto.fn_PrecioVenta;
    }

    var total = 0;

    for (var i = 0; i < this.productosArray.length; i++) {
      total += +this.productosArray[i].fn_Total;
    }

    this.ventaModel.fn_Total = total;
    this.aplicaDescuento();
  }

  disableProducto() {
    var index = -1;
    index = this.productosArray.indexOf(this.productoSelected);
    if (index > -1) {
      this.productosArray.splice(index, 1);
    }

    this.rejectConfirm();
    this.updateTotal();
  }

  deleteProducto(producto: VentaDetalle) {
    this.productoSelected = producto;
    this.acceptConfirm = this.disableProducto;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: '¿Estas seguro?',
      detail: 'Eliminar Producto(s)',

    });
  }

  isValid() {
    return this.ventaForm.status === 'VALID' && this.productosArray.length > 0 && this.montoRecibido >= this.ventaModel.fn_Total;
  }

  aplicaDescuento() {
    if (this.ventaModel.fn_DescuentoPorcentaje) {
      this.ventaModel.fn_DescuentoCantidad = this.ventaModel.fn_Total * this.ventaModel.fn_DescuentoPorcentaje / 100;
    }
    else {
      this.ventaModel.fn_DescuentoCantidad = 0;
    }

    this.ventaModel.fn_Total = this.ventaModel.fn_Total - this.ventaModel.fn_DescuentoCantidad;
    this.calculaCambio();
  }

  calculaCambio() {
    if (this.montoRecibido >= this.ventaModel.fn_Total) {
      this.montoCambio = this.montoRecibido - this.ventaModel.fn_Total;
    }
  }

  realizarVenta() {
    this.ventaModel.ventaDetalles = this.productosArray;
    this.ventaModel.fb_Estatus = true;

    this.ventasService.saveVenta(this.ventaModel).subscribe((courseResult: number) => {

      this.closeAccept = this.finalizaVenta;

      this.messageIcon = 'pi-check-circle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'success',
        summary: 'Éxito',
        detail: 'Tus Cambios se han guardado con Éxito'
      });
    }, error => {
      this.showError(error);
    });
  }

  finalizaVenta() {
    if (this.isEdition)
      this.cancelEdit.emit();
    else
      this.router.navigate(['gestionventas']);    
  }

  cancelarVenta() {
    if (this.isEdition)
      this.cancelEdit.emit();
    else
      this.finalizaVenta();
  }
}
