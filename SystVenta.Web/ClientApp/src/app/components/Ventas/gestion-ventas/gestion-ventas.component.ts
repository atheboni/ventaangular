import { Component, AfterViewChecked, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { VentasEndpoint } from '../../../services/ventas-endpoint.service';
import { EndpointFactory } from '../../../services/endpoint-factory.service';
import { MessageService } from 'primeng/api';
import { VentasService } from '../../../services/ventas.service';
import { Ventas } from '../../../models/ventas/ventas.model';
import { Venta } from '../../../models/ventas/venta.model';

@Component({
  selector: 'ventas-gestion-ventas',
  templateUrl: './gestion-ventas.component.html',
  styleUrls: ['./gestion-ventas.component.css'],
  providers: [
    EndpointFactory,
    VentasEndpoint,
    VentasService,
    MessageService
  ]
})
/** GestionVentas component*/
export class GestionVentasComponent implements AfterViewChecked, OnInit {
  ventasArray: Ventas[];
  ventaSelected: Ventas = new Ventas();
  isEdit: boolean = false;
  ventasForBulkDelete: number[] = [];
  ventasForm: FormGroup = new FormGroup({});
  messageIcon: string = "";
  selectedVentass: Ventas[];
  ventaModel: Venta;

  acceptConfirm: any;

  cols: any[] = [
    { field: 'fc_NumeroVenta', header: 'No. de Venta', class: 'gridName' },
    { field: 'fd_FechaVenta', header: 'Fecha', class: 'gridName' },
    { field: 'cliente', header: 'Cliente', class: 'gridName' },
    { field: 'metodoPago', header: 'Forma de Pago', class: 'gridName' },
    { field: 'fn_Total', header: 'Total', class: 'gridName' },
  ];

  /** GestionVentas ctor */
  constructor(private ventasService: VentasService, private messageService: MessageService) {

  }

  ngOnInit(): void {
    this.loadAllVentas();
  }

  ngAfterViewChecked(): void {
    if (document.querySelector('script[src="../../../assets/scripts/hoe.js"]') == null) {
      const node = document.createElement('script');
      node.src = '../../../assets/scripts/hoe.js';
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }  

  loadAllVentas() {
    this.ventasService.getVentas().subscribe((ventas: Ventas[]) => {
      this.onAllVentasLoadSuccessful(ventas);
    }, error => {
      this.showError(error);
    });
  }

  onAllVentasLoadSuccessful(ventas: Ventas[]) {
    this.ventasArray = ventas;
  }

  showError(error: any) {
    this.messageIcon = 'pi-exclamation-triangle';
    this.messageService.add({
      key: 'aceptar',
      sticky: true,
      severity: 'error',
      summary: 'Error',
      detail: error
    });
  }

  getVentaDetail(venta: Ventas) {
    this.ventasService.getVentaDetail(venta.fi_IdCnfVenta).subscribe((ventaDetail: Venta) => {
      this.ventaModel = ventaDetail;
    }, error => {
      this.showError(error);
    });
    this.isEdit = true;
  }

  disableVenta() {
    this.ventasService.getDeleteVenta(this.ventaSelected.fi_IdCnfVenta).subscribe(results => {
      var index = -1;
      index = this.ventasArray.indexOf(this.ventaSelected);
      if (index > -1) {
        this.ventasArray.splice(index, 1);
      }

      this.rejectConfirm();
    }, error => {
      this.showError(error);
    });
  }

  deleteVenta(venta: Ventas) {

    this.ventaSelected = venta;
    this.acceptConfirm = this.disableVenta;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: '¿Estas seguro?',
      detail: 'Eliminar Venta',

    });
  }  

  closeError() {
    this.messageService.clear('aceptar');
  }

  rejectConfirm() {
    this.messageService.clear('confirmation');
  }
}
