import { Component, AfterViewChecked, OnInit } from '@angular/core';
import { Cliente } from '../../../models/ventas/cliente.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { VentasEndpoint } from '../../../services/ventas-endpoint.service';
import { EndpointFactory } from '../../../services/endpoint-factory.service';
import { MessageService } from 'primeng/api';
import { VentasService } from '../../../services/ventas.service';

@Component({
  selector: 'ventas-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css'],
  providers: [
    EndpointFactory,
    VentasEndpoint,
    VentasService,
    MessageService
  ]
})
/** Clientes component*/
export class ClientesComponent implements AfterViewChecked, OnInit {
  clientesArray: Cliente[];
  clienteSelected: Cliente = new Cliente();
  isEdit: boolean = false;
  clientesForBulkDelete: number[] = [];
  clienteForm: FormGroup = new FormGroup({});
  messageIcon: string = "";
  selectedClientes: Cliente[];

  acceptConfirm: any;

  public emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  cols: any[] = [
    { field: 'fc_NombreCliente', header: 'Nombre Cliente', class: 'gridName' },
    { field: 'fc_RFC', header: 'RFC', class: 'gridName' },
    { field: 'fc_Email', header: 'Email', class: 'gridName' },
    { field: 'fc_Direccion', header: 'Dirección', class: 'gridName' },
    { field: 'fc_Telefono', header: 'Teléfono', class: 'gridName' },
  ];

  /** Clientes ctor */
  constructor(private ventasService: VentasService, private messageService: MessageService) {

  }

  ngOnInit(): void {
    this.loadAllClientes();

    this.clienteForm = new FormGroup({
      clienteName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
      clienteRFC: new FormControl('', [Validators.minLength(1), Validators.maxLength(100)]),
      clienteEmail: new FormControl('', [Validators.email, Validators.pattern(this.emailPattern)]),
      clienteDireccion: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(150)]),
      clienteTelefono: new FormControl('', [Validators.minLength(1), Validators.maxLength(100)]),      
    });
  }

  ngAfterViewChecked(): void {
    if (document.querySelector('script[src="../../../assets/scripts/hoe.js"]') == null) {
      const node = document.createElement('script');
      node.src = '../../../assets/scripts/hoe.js';
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }  

  loadAllClientes() {
    this.ventasService.getClientes().subscribe((clientes: Cliente[]) => {
      this.onAllClientesLoadSuccessful(clientes);
    }, error => {
      this.showError(error);
    });
  }

  onAllClientesLoadSuccessful(clientes: Cliente[]) {
    this.clientesArray = clientes;
  }

  showError(error: any) {
    this.messageIcon = 'pi-exclamation-triangle';
    this.messageService.add({
      key: 'aceptar',
      sticky: true,
      severity: 'error',
      summary: 'Error',
      detail: error
    });
  }

  newCliente() {
    this.clienteSelected = new Cliente();
    this.isEdit = true;
  }

  validateExistCliente(cliente: Cliente) {
    if (typeof this.clientesArray.find(x => x.fc_NombreCliente.toLowerCase() == cliente.fc_NombreCliente.toLowerCase() && x.fi_IdCatCliente != cliente.fi_IdCatCliente) !== 'undefined') {
      this.messageIcon = 'pi-exclamation-triangle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'error',
        summary: 'Error',
        detail: 'El Cliente ya existe'
      });
    }
    else {
      this.validateExistDisabledCliente(cliente);
    }
  }

  validateExistDisabledCliente(cliente: Cliente) {
    this.ventasService.getExistCliente(cliente.fc_NombreCliente).subscribe((existentCliente: Cliente[]) => {
      if (existentCliente.length > 0) {
        this.confirmDuplicateCliente(existentCliente[0]);
      }
      else {
        this.saveCliente(cliente);
      }
    }, error => {
      this.showError(error);
    });
  }

  enableCliente() {
    this.clienteSelected.fb_Estatus = true;
    this.saveCliente(this.clienteSelected);

    this.rejectConfirm();
  }

  confirmDuplicateCliente(cliente: Cliente) {
    this.clienteSelected = cliente;
    this.acceptConfirm = this.enableCliente;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: 'El Cliente ya existe, pero está deshabilitado',
      detail: '¿Deseas Habilitarlo?',

    });
  }

  saveCliente(cliente: Cliente) {
    this.ventasService.saveCliente(cliente).subscribe((courseResult: number) => {
      this.messageIcon = 'pi-check-circle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'success',
        summary: 'Éxito',
        detail: 'Tus Cambios se han guardado con Éxito'
      });

      this.loadAllClientes();
      this.clienteForm.reset();
      this.isEdit = false;
    }, error => {
      this.showError(error);
    });
  }

  getClienteDetail(cliente: Cliente) {
    this.clienteSelected = Object.assign({}, cliente);
    this.isEdit = true;
  }

  disableCliente() {
    this.ventasService.getDeleteCliente(this.clienteSelected.fi_IdCatCliente).subscribe(results => {
      var index = -1;
      index = this.clientesArray.indexOf(this.clienteSelected);
      if (index > -1) {
        this.clientesArray.splice(index, 1);
      }

      this.rejectConfirm();
    }, error => {
      this.showError(error);
    });
  }

  deleteCliente(cliente: Cliente) {

    this.clienteSelected = cliente;
    this.acceptConfirm = this.disableCliente;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: '¿Estas seguro?',
      detail: 'Eliminar Cliente',

    });
  }

  addTobulk(checkedCliente: Cliente) {
    if (!checkedCliente.isForBulkDelete) {
      checkedCliente.isForBulkDelete = true;
      this.clientesForBulkDelete.push(checkedCliente.fi_IdCatCliente);
    } else {
      checkedCliente.isForBulkDelete = false;
      var index = -1;
      index = this.clientesForBulkDelete.indexOf(checkedCliente.fi_IdCatCliente);
      if (index > -1) {
        this.clientesForBulkDelete.splice(index, 1);
      }
    }
  }

  bulkDelete() {
    this.acceptConfirm = this.deleteBulk;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: '¿Estas seguro?',
      detail: 'Eliminar Clientes',

    });
  }

  deleteBulk() {
    if (this.clientesForBulkDelete.length > 0) {
      this.ventasService.getBulkDeleteCliente(this.clientesForBulkDelete).subscribe(results => {
        for (var i = 0; i < this.clientesForBulkDelete.length; i++) {
          var index = -1;
          index = this.clientesArray.indexOf(this.clientesArray.find(x => x.fi_IdCatCliente == this.clientesForBulkDelete[i]));
          if (index > -1) {
            this.clientesArray.splice(index, 1);
          }
        }
        this.bulkDeleteCompleted();
      }, error => {
        this.showError(error);
      });
    }
  }

  bulkDeleteCompleted() {
    this.clientesForBulkDelete = [];
    this.rejectConfirm();
  }

  closeError() {
    this.messageService.clear('aceptar');
  }

  rejectConfirm() {
    this.messageService.clear('confirmation');
  }  
}
