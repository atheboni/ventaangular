import { Component, AfterViewChecked, OnInit } from '@angular/core';
import { ReportesService } from '../../../services/reportes.service';
import { ReportesEndpoint } from '../../../services/reportes-endpoint.service';
import { EndpointFactory } from '../../../services/endpoint-factory.service';
import { MessageService } from 'primeng/api';
import { ReporteCompras } from '../../../models/reportes/reporteCompras.model';
import { ReporteVentas } from '../../../models/reportes/reporteVentas.model';
import { ReporteComprasVentas } from '../../../models/reportes/reporteComprasVentas.model';
import { ReporteFiltros } from '../../../models/reportes/reporteFiltros.model';

@Component({
  selector: 'reportes-rep-compras-ventas',
  templateUrl: './rep-compras-ventas.component.html',
  styleUrls: ['./rep-compras-ventas.component.css'],
  providers: [
    EndpointFactory,
    ReportesEndpoint,
    ReportesService,
    MessageService
  ]
})
/** RepComprasVentas component*/
export class RepComprasVentasComponent implements AfterViewChecked, OnInit {
  data: any;
  messageIcon: string = "";
  dataLabels: string[] = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
  dataValuesCompras: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  dataValuesVentas: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  comprasVentasArray: ReporteComprasVentas[] = [
    { mes: 1, Compras: 0, Ventas: 0 },
    { mes: 2, Compras: 0, Ventas: 0 },
    { mes: 3, Compras: 0, Ventas: 0 },
    { mes: 4, Compras: 0, Ventas: 0 },
    { mes: 5, Compras: 0, Ventas: 0 },
    { mes: 6, Compras: 0, Ventas: 0 },
    { mes: 7, Compras: 0, Ventas: 0 },
    { mes: 8, Compras: 0, Ventas: 0 },
    { mes: 9, Compras: 0, Ventas: 0 },
    { mes: 10, Compras: 0, Ventas: 0 },
    { mes: 11, Compras: 0, Ventas: 0 },
    { mes: 12, Compras: 0, Ventas: 0 }
  ];
  currentYear: number = new Date().getFullYear();
  reporteFiltros: ReporteFiltros = { fd_FechaDesde: new Date('1/1/' + this.currentYear), fd_FechaHasta: new Date('12/1/' + this.currentYear) };

  cols: any[] = [
    { field: 'mes', header: 'Mes', class: 'gridName' },
    { field: 'Compras', header: 'Compras ($)', class: 'gridName' },
    { field: 'Ventas', header: 'Ventas ($)', class: 'gridName' },    
  ];

  /** RepComprasVentas ctor */
  constructor(private reportesService: ReportesService, private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.loadReporteCompras(this.currentYear);
  }

  ngAfterViewChecked(): void {
    if (document.querySelector('script[src="../../../assets/scripts/hoe.js"]') == null) {
      const node = document.createElement('script');
      node.src = '../../../assets/scripts/hoe.js';
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }  

  getReporte(event: any) {
    this.dataValuesCompras = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.dataValuesVentas = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.comprasVentasArray = [];
    this.loadReporteCompras(event.target.value);
  }

  loadReporteCompras(anioReporte: number) {
    this.reporteFiltros.fd_FechaDesde = new Date('1/1/' + anioReporte);
    this.reporteFiltros.fd_FechaHasta = new Date('12/1/' + anioReporte);
    this.reportesService.getReporteCompras(this.reporteFiltros).subscribe((reporteCompras: ReporteCompras[]) => {
      this.onReporteComprasLoadSuccessful(reporteCompras);
    }, error => {
      this.showError(error);
    });
  }

  onReporteComprasLoadSuccessful(reporteCompras: ReporteCompras[]) {
    if (reporteCompras.length > 0 && this.comprasVentasArray.length == 0) {
      this.comprasVentasArray = [
        { mes: 1, Compras: 0, Ventas: 0 },
        { mes: 2, Compras: 0, Ventas: 0 },
        { mes: 3, Compras: 0, Ventas: 0 },
        { mes: 4, Compras: 0, Ventas: 0 },
        { mes: 5, Compras: 0, Ventas: 0 },
        { mes: 6, Compras: 0, Ventas: 0 },
        { mes: 7, Compras: 0, Ventas: 0 },
        { mes: 8, Compras: 0, Ventas: 0 },
        { mes: 9, Compras: 0, Ventas: 0 },
        { mes: 10, Compras: 0, Ventas: 0 },
        { mes: 11, Compras: 0, Ventas: 0 },
        { mes: 12, Compras: 0, Ventas: 0 }
      ];
    }

    for (var i = 0; i < reporteCompras.length; i++) {
      this.dataValuesCompras[reporteCompras[i].mes - 1] = reporteCompras[i].fn_Total;
      this.comprasVentasArray[reporteCompras[i].mes - 1].Compras = reporteCompras[i].fn_Total;
    }

    this.loadReporteVentas();
  }

  loadReporteVentas() {
    this.reportesService.getReporteVentas(this.reporteFiltros).subscribe((reporteVentas: ReporteVentas[]) => {
      this.onReporteVentasLoadSuccessful(reporteVentas);
    }, error => {
      this.showError(error);
    });
  }

  onReporteVentasLoadSuccessful(reporteVentas: ReporteVentas[]) {
    if (reporteVentas.length > 0 && this.comprasVentasArray.length == 0) {
      this.comprasVentasArray = [
        { mes: 1, Compras: 0, Ventas: 0 },
        { mes: 2, Compras: 0, Ventas: 0 },
        { mes: 3, Compras: 0, Ventas: 0 },
        { mes: 4, Compras: 0, Ventas: 0 },
        { mes: 5, Compras: 0, Ventas: 0 },
        { mes: 6, Compras: 0, Ventas: 0 },
        { mes: 7, Compras: 0, Ventas: 0 },
        { mes: 8, Compras: 0, Ventas: 0 },
        { mes: 9, Compras: 0, Ventas: 0 },
        { mes: 10, Compras: 0, Ventas: 0 },
        { mes: 11, Compras: 0, Ventas: 0 },
        { mes: 12, Compras: 0, Ventas: 0 }
      ];
    }

    for (var i = 0; i < reporteVentas.length; i++) {
      this.dataValuesVentas[reporteVentas[i].mes - 1] = reporteVentas[i].fn_Total;
      this.comprasVentasArray[reporteVentas[i].mes - 1].Ventas = reporteVentas[i].fn_Total;
    }

    this.data = {
      labels: this.dataLabels,
      datasets: [
        {
          label: 'Compras',
          data: this.dataValuesCompras,
          fill: false,
          borderColor: '#4bc0c0'
        },
        {
          label: 'Ventas',
          data: this.dataValuesVentas,
          fill: false,
          borderColor: '#565656'
        }
      ]
    }
  }

  showError(error: any) {
    this.messageIcon = 'pi-exclamation-triangle';
    this.messageService.add({
      key: 'aceptar',
      sticky: true,
      severity: 'error',
      summary: 'Error',
      detail: error
    });
  }

  closeError() {
    this.messageService.clear('aceptar');
  }  
}
