import { Component, AfterViewChecked, OnInit } from '@angular/core';
import { ReportesService } from '../../../services/reportes.service';
import { ReportesEndpoint } from '../../../services/reportes-endpoint.service';
import { EndpointFactory } from '../../../services/endpoint-factory.service';
import { MessageService } from 'primeng/api';
import { ReporteMasVendidos } from '../../../models/reportes/reporteMasVendidos.model';

@Component({
  selector: 'reportes-rep-mas-vendidos',
  templateUrl: './rep-mas-vendidos.component.html',
  styleUrls: ['./rep-mas-vendidos.component.css'],
  providers: [
    EndpointFactory,
    ReportesEndpoint,
    ReportesService,
    MessageService
  ]
})
/** RepMasVendidos component*/
export class RepMasVendidosComponent implements AfterViewChecked, OnInit {
  data: any;
  options: any;
  messageIcon: string = "";
  dataLabels: string[] = [];
  dataValues: number[] = [];
  showRegresar: boolean = false;

  globalReporteMasVendidos: ReporteMasVendidos[];

  cols: any[] = [
    { field: 'fc_NombreProducto', header: 'Nombre Producto', class: 'gridName' },
    { field: 'fi_Stock', header: 'Stock', class: 'gridName' },
  ];

  /** RepMasVendidos ctor */
  constructor(private reportesService: ReportesService, private messageService: MessageService) {    
  }

  ngOnInit(): void {
    this.loadReporteMasVendidos();
  }

  ngAfterViewChecked(): void {
    if (document.querySelector('script[src="../../../assets/scripts/hoe.js"]') == null) {
      const node = document.createElement('script');
      node.src = '../../../assets/scripts/hoe.js';
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }  

  loadReporteMasVendidos() {
    this.showRegresar = false;
    this.reportesService.getMasVendidos().subscribe((reporteMasVendidos: ReporteMasVendidos[]) => {
      this.onReporteMasVendidosLoadSuccessful(reporteMasVendidos);
    }, error => {
      this.showError(error);
    });
  }

  onReporteMasVendidosLoadSuccessful(reporteMasVendidos: ReporteMasVendidos[]) {
    this.dataLabels = [];
    this.dataValues = [];

    this.globalReporteMasVendidos = reporteMasVendidos;

    for (var i = 0; i < reporteMasVendidos.length; i++) {
      var suma = 0;
      this.dataLabels.push(reporteMasVendidos[i].fc_NombreProducto.toUpperCase());

      for (var j = 0; j < reporteMasVendidos[i].ventas.length; j++) {
        suma += reporteMasVendidos[i].ventas[j].fn_Total;
      }

      this.dataValues.push(suma);
    }

    this.data = {
      labels: this.dataLabels,
      datasets: [
        {
          label: "Productos Más Vendidos",
          data: this.dataValues,
          backgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56",
            "#8085e9",
            "#9CCC65",

            "#f7a35c",
            "#434348",
            "#2b908f",
            "#f45b5b",
            "#90ed7d",
            "#91e8e1",
            "#90ed7d"
          ],
          hoverBackgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56",
            "#8085e9",
            "#9CCC65",

            "#f7a35c",
            "#434348",
            "#2b908f",
            "#f45b5b",
            "#90ed7d",
            "#91e8e1",
            "#90ed7d"
          ]
        }]
    };
  }

  selectData(event) {

    //event.dataset = Selected dataset
    //event.element = Selected element
    //event.element._datasetIndex = Index of the dataset in data
    //event.element._index = Index of the data in dataset
    this.showRegresar = true;

    var color = this.data.datasets[event.element._datasetIndex].backgroundColor[event.element._index];
    var producto = this.data.labels[event.element._index];

    this.dataLabels = [];
    this.dataValues = [];

    for (var i = 0; i < this.globalReporteMasVendidos[event.element._index].ventas.length; i++) {
      switch (this.globalReporteMasVendidos[event.element._index].ventas[i].mes) {
        case 1: this.dataLabels.push('Enero');
          break;
        case 2: this.dataLabels.push('Febrero');
          break;
        case 3: this.dataLabels.push('Marzo');
          break;
        case 4: this.dataLabels.push('Abril');
          break;
        case 5: this.dataLabels.push('Mayo');
          break;
        case 6: this.dataLabels.push('Junio');
          break;
        case 7: this.dataLabels.push('Julio');
          break;
        case 8: this.dataLabels.push('Agosto');
          break;
        case 9: this.dataLabels.push('Septiembre');
          break;
        case 10: this.dataLabels.push('Octubre');
          break;
        case 11: this.dataLabels.push('Noviembre');
          break;
        case 12: this.dataLabels.push('Diciembre');
          break;
      }

      this.dataValues.push(this.globalReporteMasVendidos[event.element._index].ventas[i].fn_Total);
    }

    this.data = {
      labels: this.dataLabels,
      datasets: [
        {
          label: producto,
          data: this.dataValues,
          backgroundColor: color,
          hoverBackgroundColor: color
        }]
    };
  }

  showError(error: any) {
    this.messageIcon = 'pi-exclamation-triangle';
    this.messageService.add({
      key: 'aceptar',
      sticky: true,
      severity: 'error',
      summary: 'Error',
      detail: error
    });
  }

  closeError() {
    this.messageService.clear('aceptar');
  }
}
