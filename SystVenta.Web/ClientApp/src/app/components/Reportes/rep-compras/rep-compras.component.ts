import { Component, AfterViewChecked, OnInit } from '@angular/core';
import { ReportesService } from '../../../services/reportes.service';
import { ReportesEndpoint } from '../../../services/reportes-endpoint.service';
import { EndpointFactory } from '../../../services/endpoint-factory.service';
import { MessageService } from 'primeng/api';
import { ReporteCompras } from '../../../models/reportes/reporteCompras.model';
import { ReporteFiltros } from '../../../models/reportes/reporteFiltros.model';

@Component({
  selector: 'reportes-rep-compras',
  templateUrl: './rep-compras.component.html',
  styleUrls: ['./rep-compras.component.css'],
  providers: [
    EndpointFactory,
    ReportesEndpoint,
    ReportesService,
    MessageService
  ]
})
/** RepCompras component*/
export class RepComprasComponent implements AfterViewChecked, OnInit {
  data: any;
  messageIcon: string = "";
  dataLabels: string[] = [];
  dataValues: number[] = [];
  reporteComprasArray: ReporteCompras[];
  currentYear: number = new Date().getFullYear();
  reporteFiltros: ReporteFiltros = { fd_FechaDesde: new Date('1/1/' + this.currentYear), fd_FechaHasta: new Date(Date.now()) };
  yearRangeDesde: string = this.currentYear - 3 + ":" + this.currentYear;

  cols: any[] = [
    { field: 'mes', header: 'Mes', class: 'gridName' },
    { field: 'anio', header: 'Año', class: 'gridName' },
    { field: 'fn_Total', header: 'Total ($)', class: 'gridName' },
    { field: 'cantidad', header: 'No. Compras', class: 'gridName' },
    { field: 'porcentaje', header: 'Porcentaje (%)', class: 'gridName' },
  ];

  /** RepCompras ctor */
  constructor(private reportesService: ReportesService, private messageService: MessageService) {    
  }

  ngOnInit(): void {
    this.loadReporteCompras();
  }

  ngAfterViewChecked(): void {
    if (document.querySelector('script[src="../../../assets/scripts/hoe.js"]') == null) {
      const node = document.createElement('script');
      node.src = '../../../assets/scripts/hoe.js';
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }    

  getReporte() {
    this.dataLabels = [];
    this.dataValues = [];
    this.reporteComprasArray = [];
    this.loadReporteCompras();
  }

  loadReporteCompras() {
    this.reportesService.getReporteCompras(this.reporteFiltros).subscribe((reporteCompras: ReporteCompras[]) => {
      this.onReporteComprasLoadSuccessful(reporteCompras);
    }, error => {
      this.showError(error);
    });
  }

  onReporteComprasLoadSuccessful(reporteCompras: ReporteCompras[]) {
    for (var i = 0; i < reporteCompras.length; i++) {
      switch (reporteCompras[i].mes) {
        case 1: this.dataLabels.push('Enero');
          break;
        case 2: this.dataLabels.push('Febrero');
          break;
        case 3: this.dataLabels.push('Marzo');
          break;
        case 4: this.dataLabels.push('Abril');
          break;
        case 5: this.dataLabels.push('Mayo');
          break;
        case 6: this.dataLabels.push('Junio');
          break;
        case 7: this.dataLabels.push('Julio');
          break;
        case 8: this.dataLabels.push('Agosto');
          break;
        case 9: this.dataLabels.push('Septiembre');
          break;
        case 10: this.dataLabels.push('Octubre');
          break;
        case 11: this.dataLabels.push('Noviembre');
          break;
        case 12: this.dataLabels.push('Diciembre');
          break;
      }

      this.dataValues.push(reporteCompras[i].fn_Total);
      this.reporteComprasArray = reporteCompras;
    }
    this.data = {
      labels: this.dataLabels,
      datasets: [
        {          
          data: this.dataValues,
          backgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56",
            "#8085e9",
            "#9CCC65",

            "#f7a35c",
            "#434348",
            "#2b908f",
            "#f45b5b",
            "#90ed7d",
            "#91e8e1",
            "#90ed7d"
          ],
          hoverBackgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56",
            "#8085e9",
            "#9CCC65",

            "#f7a35c",
            "#434348",
            "#2b908f",
            "#f45b5b",
            "#90ed7d",
            "#91e8e1",
            "#90ed7d"
          ]
        }]
    };
  }

  showError(error: any) {
    this.messageIcon = 'pi-exclamation-triangle';
    this.messageService.add({
      key: 'aceptar',
      sticky: true,
      severity: 'error',
      summary: 'Error',
      detail: error
    });
  }

  closeError() {
    this.messageService.clear('aceptar');
  }
}
