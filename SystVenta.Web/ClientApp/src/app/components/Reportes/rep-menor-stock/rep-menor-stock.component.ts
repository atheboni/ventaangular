import { Component, AfterViewChecked, OnInit } from '@angular/core';
import { ReportesService } from '../../../services/reportes.service';
import { ReportesEndpoint } from '../../../services/reportes-endpoint.service';
import { EndpointFactory } from '../../../services/endpoint-factory.service';
import { MessageService } from 'primeng/api';
import { Producto } from '../../../models/almacen/producto.model';

@Component({
  selector: 'reportes-rep-menor-stock',
  templateUrl: './rep-menor-stock.component.html',
  styleUrls: ['./rep-menor-stock.component.css'],
  providers: [
    EndpointFactory,
    ReportesEndpoint,
    ReportesService,
    MessageService
  ]
})
/** RepMenorStock component*/
export class RepMenorStockComponent implements AfterViewChecked, OnInit {
  data: any;
  messageIcon: string = "";
  dataLabels: string[] = [];
  dataValues: number[] = [];

  productosArray: Producto[];

  cols: any[] = [
    { field: 'fc_NombreProducto', header: 'Nombre Producto', class: 'gridName' },
    { field: 'fi_Stock', header: 'Stock', class: 'gridName' },
  ];

  /** RepMenorStock ctor */
  constructor(private reportesService: ReportesService, private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.loadMenorStock();
  }

  ngAfterViewChecked(): void {
    if (document.querySelector('script[src="../../../assets/scripts/hoe.js"]') == null) {
      const node = document.createElement('script');
      node.src = '../../../assets/scripts/hoe.js';
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }  

  loadMenorStock() {
    this.reportesService.getMenorStock().subscribe((productos: Producto[]) => {
      this.onMenorStockLoadSuccessful(productos);
    }, error => {
      this.showError(error);
    });
  }

  onMenorStockLoadSuccessful(productos: Producto[]) {
    for (var i = 0; i < productos.length; i++) {
      this.dataLabels.push(productos[i].fc_NombreProducto.toUpperCase());
      this.dataValues.push(productos[i].fi_Stock);
    }   

    this.productosArray = productos;

    this.data = {
      labels: this.dataLabels,
      datasets: [
        {
          label: "Productos Menor Stock",
          data: this.dataValues,
          backgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56",
            "#8085e9",
            "#9CCC65",

            "#f7a35c",
            "#434348",
            "#2b908f",
            "#f45b5b",
            "#90ed7d",
            "#91e8e1",
            "#90ed7d"
          ],
          hoverBackgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56",
            "#8085e9",
            "#9CCC65",

            "#f7a35c",
            "#434348",
            "#2b908f",
            "#f45b5b",
            "#90ed7d",
            "#91e8e1",
            "#90ed7d"
          ]
        }]
    };
  }

  showError(error: any) {
    this.messageIcon = 'pi-exclamation-triangle';
    this.messageService.add({
      key: 'aceptar',
      sticky: true,
      severity: 'error',
      summary: 'Error',
      detail: error
    });
  }

  closeError() {
    this.messageService.clear('aceptar');
  }
}
