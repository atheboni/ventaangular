import { Component, OnInit } from '@angular/core';
import { MainService } from '../../services/main.service';
import { Router } from '@angular/router';

import { ConfiguracionEndpoint } from '../../services/configuracion-endpoint.service';
import { EndpointFactory } from '../../services/endpoint-factory.service';
import { MessageService } from 'primeng/api';
import { ConfiguracionService } from '../../services/configuracion.service';
import { Usuario } from '../../models/configuracion/usuario.model';
import { Login } from '../../models/configuracion/login.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ConfiguracionGeneral } from '../../models/configuracion/configuracionGeneral.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [
    EndpointFactory,
    ConfiguracionEndpoint,
    ConfiguracionService,
    MessageService
  ]
})
export class HomeComponent implements OnInit {
  loginModel: Login = new Login();
  showLoginError: boolean = false;
  loginForm: FormGroup = new FormGroup({});
  usuarioLogueado: Usuario;

  constructor(private mainService: MainService, private router: Router, private configuracionService: ConfiguracionService) {    
  }

  ngOnInit() {    
    this.loginForm = new FormGroup({
      userName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(10)]),
      password: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(10)]),
    });
  }

  login() {
    this.configuracionService.login(this.loginModel).subscribe((usuario: Usuario[]) => {
      this.onLoginSuccessful(usuario);
    }, error => {
      this.showError(error);
      });    
  }

  onLoginSuccessful(usuario: Usuario[]) {
    if (usuario.length > 0) {
      this.usuarioLogueado = usuario[0];
      this.loadGeneralInfo();      
    }
    else {
      this.showLoginError = true;
    }
  }

  loadGeneralInfo() {
    this.configuracionService.getConfiguracionGeneral().subscribe((configuracionGeneralResult: ConfiguracionGeneral[]) => {
      this.onGeneralInfoLoadSuccessful(configuracionGeneralResult);
    }, error => {
      this.showError(error);
    });
  }

  onGeneralInfoLoadSuccessful(configuracionGeneralResult: ConfiguracionGeneral[]) {
    if (configuracionGeneralResult.length > 0) {

      this.mainService.login(this.usuarioLogueado, configuracionGeneralResult[0]);
      this.router.navigate(['menu']);
      this.showLoginError = false;
    }
  }

  showError(error: any) {
    //this.messageIcon = 'pi-exclamation-triangle';
    //this.messageService.add({
    //  key: 'aceptar',
    //  sticky: true,
    //  severity: 'error',
    //  summary: 'Error',
    //  detail: error
    //});
  }
}
