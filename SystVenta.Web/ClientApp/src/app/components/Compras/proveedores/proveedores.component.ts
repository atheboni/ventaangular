import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { Proveedor } from '../../../models/compras/proveedor.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ComprasEndpoint } from '../../../services/compras-endpoint.service';
import { EndpointFactory } from '../../../services/endpoint-factory.service';
import { MessageService } from 'primeng/api';
import { ComprasService } from '../../../services/compras.service';

@Component({
  selector: 'compras-proveedores',
  templateUrl: './proveedores.component.html',
  styleUrls: ['./proveedores.component.css'],
  providers: [
    EndpointFactory,
    ComprasEndpoint,
    ComprasService,
    MessageService
  ]
})
/** Proveedores component*/
export class ProveedoresComponent implements AfterViewChecked, OnInit {
  proveedorsArray: Proveedor[];
  proveedorSelected: Proveedor = new Proveedor();
  isEdit: boolean = false;
  proveedorsForBulkDelete: number[] = [];
  proveedorForm: FormGroup = new FormGroup({});
  messageIcon: string = "";
  selectedProveedores: Proveedor[];

  acceptConfirm: any;

  public emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  cols: any[] = [
    { field: 'fc_RazonSocial', header: 'Razón Social', class: 'gridName' },
    { field: 'fc_NombreProveedor', header: 'Nombre Proveedor', class: 'gridName' },
    { field: 'fc_RFC', header: 'RFC', class: 'gridName' },
    { field: 'fc_Email', header: 'Email', class: 'gridName' },
    { field: 'fc_Direccion', header: 'Dirección', class: 'gridName' },
    { field: 'fc_Telefono', header: 'Teléfono', class: 'gridName' },
  ];

  /** Proveedores ctor */
  constructor(private comprasService: ComprasService, private messageService: MessageService) {

  }

  ngOnInit(): void {
    this.loadAllProveedores();

    this.proveedorForm = new FormGroup({
      proveedorRazonSocial: new FormControl('', [Validators.minLength(1), Validators.maxLength(100)]),
      proveedorName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
      proveedorRFC: new FormControl('', [Validators.minLength(1), Validators.maxLength(100)]),
      proveedorEmail: new FormControl('', [Validators.email, Validators.pattern(this.emailPattern)]),
      proveedorDireccion: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(150)]),
      proveedorTelefono: new FormControl('', [Validators.minLength(1), Validators.maxLength(100)]),
    });
  }

  ngAfterViewChecked(): void {
    if (document.querySelector('script[src="../../../assets/scripts/hoe.js"]') == null) {
      const node = document.createElement('script');
      node.src = '../../../assets/scripts/hoe.js';
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }  

  loadAllProveedores() {
    this.comprasService.getProveedores().subscribe((proveedors: Proveedor[]) => {
      this.onAllProveedoresLoadSuccessful(proveedors);
    }, error => {
      this.showError(error);
    });
  }

  onAllProveedoresLoadSuccessful(proveedors: Proveedor[]) {
    this.proveedorsArray = proveedors;
  }

  showError(error: any) {
    this.messageIcon = 'pi-exclamation-triangle';
    this.messageService.add({
      key: 'aceptar',
      sticky: true,
      severity: 'error',
      summary: 'Error',
      detail: error
    });
  }

  newProveedor() {
    this.proveedorSelected = new Proveedor();
    this.isEdit = true;
  }

  validateExistProveedor(proveedor: Proveedor) {
    if (typeof this.proveedorsArray.find(x => x.fc_NombreProveedor.toLowerCase() == proveedor.fc_NombreProveedor.toLowerCase() && x.fi_IdCatProveedor != proveedor.fi_IdCatProveedor) !== 'undefined') {
      this.messageIcon = 'pi-exclamation-triangle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'error',
        summary: 'Error',
        detail: 'El Proveedor ya existe'
      });
    }
    else {
      this.validateExistDisabledProveedor(proveedor);
    }
  }

  validateExistDisabledProveedor(proveedor: Proveedor) {
    this.comprasService.getExistProveedor(proveedor.fc_NombreProveedor).subscribe((existentProveedor: Proveedor[]) => {
      if (existentProveedor.length > 0) {
        this.confirmDuplicateProveedor(existentProveedor[0]);
      }
      else {
        this.saveProveedor(proveedor);
      }
    }, error => {
      this.showError(error);
    });
  }

  enableProveedor() {
    this.proveedorSelected.fb_Estatus = true;
    this.saveProveedor(this.proveedorSelected);

    this.rejectConfirm();
  }

  confirmDuplicateProveedor(proveedor: Proveedor) {
    this.proveedorSelected = proveedor;
    this.acceptConfirm = this.enableProveedor;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: 'El Proveedor ya existe, pero está deshabilitado',
      detail: '¿Deseas Habilitarlo?',

    });
  }

  saveProveedor(proveedor: Proveedor) {
    this.comprasService.saveProveedor(proveedor).subscribe((courseResult: number) => {
      this.messageIcon = 'pi-check-circle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'success',
        summary: 'Éxito',
        detail: 'Tus Cambios se han guardado con Éxito'
      });

      this.loadAllProveedores();
      this.proveedorForm.reset();
      this.isEdit = false;
    }, error => {
      this.showError(error);
    });
  }

  getProveedorDetail(proveedor: Proveedor) {
    this.proveedorSelected = Object.assign({}, proveedor);
    this.isEdit = true;
  }

  disableProveedor() {
    this.comprasService.getDeleteProveedor(this.proveedorSelected.fi_IdCatProveedor).subscribe(results => {
      var index = -1;
      index = this.proveedorsArray.indexOf(this.proveedorSelected);
      if (index > -1) {
        this.proveedorsArray.splice(index, 1);
      }

      this.rejectConfirm();
    }, error => {
      this.showError(error);
    });
  }

  deleteProveedor(proveedor: Proveedor) {

    this.proveedorSelected = proveedor;
    this.acceptConfirm = this.disableProveedor;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: '¿Estas seguro?',
      detail: 'Eliminar Proveedor',

    });
  }

  addTobulk(checkedProveedor: Proveedor) {
    if (!checkedProveedor.isForBulkDelete) {
      checkedProveedor.isForBulkDelete = true;
      this.proveedorsForBulkDelete.push(checkedProveedor.fi_IdCatProveedor);
    } else {
      checkedProveedor.isForBulkDelete = false;
      var index = -1;
      index = this.proveedorsForBulkDelete.indexOf(checkedProveedor.fi_IdCatProveedor);
      if (index > -1) {
        this.proveedorsForBulkDelete.splice(index, 1);
      }
    }
  }

  bulkDelete() {
    this.acceptConfirm = this.deleteBulk;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: '¿Estas seguro?',
      detail: 'Eliminar Proveedores',

    });
  }

  deleteBulk() {
    if (this.proveedorsForBulkDelete.length > 0) {
      this.comprasService.getBulkDeleteProveedor(this.proveedorsForBulkDelete).subscribe(results => {
        for (var i = 0; i < this.proveedorsForBulkDelete.length; i++) {
          var index = -1;
          index = this.proveedorsArray.indexOf(this.proveedorsArray.find(x => x.fi_IdCatProveedor == this.proveedorsForBulkDelete[i]));
          if (index > -1) {
            this.proveedorsArray.splice(index, 1);
          }
        }
        this.bulkDeleteCompleted();
      }, error => {
        this.showError(error);
      });
    }
  }

  bulkDeleteCompleted() {
    this.proveedorsForBulkDelete = [];
    this.rejectConfirm();
  }

  closeError() {
    this.messageService.clear('aceptar');
  }

  rejectConfirm() {
    this.messageService.clear('confirmation');
  }
}
