import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ComprasEndpoint } from '../../../services/compras-endpoint.service';
import { EndpointFactory } from '../../../services/endpoint-factory.service';
import { MessageService } from 'primeng/api';
import { ComprasService } from '../../../services/compras.service';
import { Compras } from '../../../models/compras/compras.model';
import { Compra } from '../../../models/compras/compra.model';

@Component({
  selector: 'compras-gestion-compras',
  templateUrl: './gestion-compras.component.html',
  styleUrls: ['./gestion-compras.component.css'],
  providers: [
    EndpointFactory,
    ComprasEndpoint,
    ComprasService,
    MessageService
  ]
})
/** GestionCompras component*/
export class GestionComprasComponent implements AfterViewChecked, OnInit {
  comprasArray: Compras[];
  comprasSelected: Compras = new Compras();
  isEdit: boolean = false;
  comprasForBulkDelete: number[] = [];
  comprasForm: FormGroup = new FormGroup({});
  messageIcon: string = "";
  selectedComprass: Compras[];
  compraModel: Compra;

  acceptConfirm: any;

  cols: any[] = [
    { field: 'fc_NumeroCompra', header: 'No. de Compra', class: 'gridName' },
    { field: 'fd_FechaCompra', header: 'Fecha', class: 'gridName' },
    { field: 'proveedor', header: 'Proveedor', class: 'gridName' },
    { field: 'fn_Total', header: 'Total', class: 'gridName' },
  ];

  /** GestionCompras ctor */
  constructor(private comprasService: ComprasService, private messageService: MessageService) {

  }

  ngOnInit(): void {
    this.loadAllCompras();
  }

  ngAfterViewChecked(): void {
    if (document.querySelector('script[src="../../../assets/scripts/hoe.js"]') == null) {
      const node = document.createElement('script');
      node.src = '../../../assets/scripts/hoe.js';
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }  

  loadAllCompras() {
    this.comprasService.getCompras().subscribe((compras: Compras[]) => {
      this.onAllComprasLoadSuccessful(compras);
    }, error => {
      this.showError(error);
    });
  }

  onAllComprasLoadSuccessful(compras: Compras[]) {
    this.comprasArray = compras;
  }

  showError(error: any) {
    this.messageIcon = 'pi-exclamation-triangle';
    this.messageService.add({
      key: 'aceptar',
      sticky: true,
      severity: 'error',
      summary: 'Error',
      detail: error
    });
  }  

  getCompraDetail(compra: Compras) {
    this.comprasService.getCompraDetail(compra.fi_IdCnfCompra).subscribe((compraDetail: Compra) => {
      this.compraModel = compraDetail;
    }, error => {
      this.showError(error);
    });
    this.isEdit = true;
  }  

  closeError() {
    this.messageService.clear('aceptar');
  }

  rejectConfirm() {
    this.messageService.clear('confirmation');
  }
}
