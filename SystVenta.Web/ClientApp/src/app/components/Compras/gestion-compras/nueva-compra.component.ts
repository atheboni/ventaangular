import { Component, OnInit, AfterViewChecked, Input, Output, EventEmitter } from '@angular/core';
import { EndpointFactory } from '../../../services/endpoint-factory.service';
import { AlmacenService } from '../../../services/almacen.service';
import { AlmacenEndpoint } from '../../../services/almacen-endpoint.service';
import { ConfiguracionService } from '../../../services/configuracion.service';
import { ConfiguracionEndpoint } from '../../../services/configuracion-endpoint.service';
import { ComprasService } from '../../../services/compras.service';
import { ComprasEndpoint } from '../../../services/compras-endpoint.service';
import { MessageService } from 'primeng/api';
import { Producto } from '../../../models/almacen/producto.model';
import { CompraDetalle } from '../../../models/compras/compraDetalle.model';
import { Compra } from '../../../models/compras/compra.model';
import { Comprobante } from '../../../models/configuracion/comprobante.model';
import { MetodoPago } from '../../../models/configuracion/metodoPago.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Proveedor } from '../../../models/compras/proveedor.model';
import { Router } from '@angular/router';

@Component({
  selector: 'configuracion-nueva-compra',
  templateUrl: './nueva-compra.component.html',
  styleUrls: ['./nueva-compra.component.css'],
  providers: [
    EndpointFactory,
    AlmacenEndpoint,
    AlmacenService,
    ConfiguracionEndpoint,
    ConfiguracionService,
    ComprasEndpoint,
    ComprasService,
    MessageService
  ]
})
/** NuevaCompra component*/
export class NuevaCompraComponent implements AfterViewChecked, OnInit {
  comprobantesArray: Comprobante[];
  metodoPagosArray: MetodoPago[];
  proveedorsArray: Proveedor[];

  filteredProductos: any[];
  messageIcon: string = "";

  compraForm: FormGroup = new FormGroup({});
  @Input() compraModel: Compra = new Compra();
  @Input() isEdition: boolean = false;
  @Output() cancelEdit = new EventEmitter();

  productosArray: CompraDetalle[] = [];
  productoSelected: CompraDetalle;

  autoInput: string = "";
  acceptConfirm: any;
  closeAccept: any;

  maxDate: Date;

  cols: any[] = [
    { field: 'fc_Codigo', header: 'Código', class: 'gridName' },
    { field: 'fc_NombreProducto', header: 'Nombre Producto', class: 'gridName' },
    { field: 'fi_Cantidad', header: 'Cantidad', class: 'Precio Compra' },
    { field: 'fn_PrecioCompra', header: 'Precio', class: 'gridName' },
    { field: 'fn_Total', header: 'Total', class: 'gridName' },
  ];

  /** NuevaCompra ctor */
  constructor(private almacenService: AlmacenService, private messageService: MessageService, private configuracionService: ConfiguracionService,
    private comprasService: ComprasService, private router: Router) {

  }

  ngOnInit() {
    this.maxDate = new Date();
    this.maxDate.setDate(this.maxDate.getDate());

    this.loadAllComprobantes();
    this.loadAllMetodoPagos();
    this.loadAllProveedores();

    this.compraForm = new FormGroup({
      compraNumero: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(4)]),
      compraFecha: new FormControl('', []),
      compraDescuento: new FormControl('', [Validators.pattern("^[0-9\.]*$"), Validators.min(0), Validators.max(100), Validators.maxLength(3)]),
      compraComprobante: new FormControl('', [Validators.required, Validators.min(1)]),
      compraMetodoPago: new FormControl('', [Validators.required, Validators.min(1)]),
      compraProveedor: new FormControl('', [Validators.required, Validators.min(1)]),
    });

    if (this.isEdition) {
      this.compraModel.fd_FechaCompra = new Date(this.compraModel.fd_FechaCompra);
      this.productosArray = this.compraModel.compraDetalles;
      this.updateTotal()
    }
  }

  ngAfterViewChecked(): void {
    if (document.querySelector('script[src="../../../assets/scripts/hoe.js"]') == null) {
      const node = document.createElement('script');
      node.src = '../../../assets/scripts/hoe.js';
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }  

  filterProduct(event) {
    let query = event.query;

    this.almacenService.getProductos().subscribe(results => {
      this.filteredProductos = this.filterProducts(query, results);
    }, error => this.showError(error));
  }

  filterProducts(query, products: Producto[]): any[] {
    let filtered: any[] = [];
    for (let i = 0; i < products.length; i++) {
      let producto = products[i];
      if (producto.fc_NombreProducto != null && ((producto.fc_NombreProducto.toLowerCase().indexOf(query.toLowerCase()) != -1) || (producto.fc_Codigo.toLowerCase().indexOf(query.toLowerCase()) != -1))) {
        producto.nombreAutocomplete = producto.fc_Codigo + ' - ' + producto.fc_NombreProducto;
        filtered.push(producto);
      }
    }
    return filtered;
  }

  productoExiste(producto: Producto) {
    var productoCompra = this.productosArray.find(x => x.fc_Codigo == producto.fc_Codigo);
    return typeof productoCompra !== 'undefined';
  }

  selectProducto(event) {
    if (this.productoExiste(event)) {
      var productoCompra = this.productosArray.find(x => x.fc_Codigo == event.fc_Codigo);
      productoCompra.fi_Cantidad += 1;
    }
    else {
      this.productoSelected = new CompraDetalle();

      this.productoSelected.fc_Codigo = event.fc_Codigo;
      this.productoSelected.fc_NombreProducto = event.fc_NombreProducto;
      this.productoSelected.fi_Cantidad = 1;
      this.productoSelected.fn_PrecioCompra = 0;
      this.productoSelected.fn_Total = 0;
      this.productoSelected.fb_Estatus = true;

      this.productosArray.push(this.productoSelected);
    }

    this.autoInput = "";
  }







  loadAllComprobantes() {
    this.configuracionService.getComprobantes().subscribe((comprobantes: Comprobante[]) => {
      this.onAllComprobantesLoadSuccessful(comprobantes);
    }, error => {
      this.showError(error);
    });
  }

  onAllComprobantesLoadSuccessful(comprobantes: Comprobante[]) {
    this.comprobantesArray = comprobantes;
  }

  loadAllMetodoPagos() {
    this.configuracionService.getMetodosPago().subscribe((metodoPagos: MetodoPago[]) => {
      this.onAllMetodoPagosLoadSuccessful(metodoPagos);
    }, error => {
      this.showError(error);
    });
  }

  onAllMetodoPagosLoadSuccessful(metodoPagos: MetodoPago[]) {
    this.metodoPagosArray = metodoPagos;
  }

  loadAllProveedores() {
    this.comprasService.getProveedores().subscribe((proveedors: Proveedor[]) => {
      this.onAllProveedoresLoadSuccessful(proveedors);
    }, error => {
      this.showError(error);
    });
  }

  onAllProveedoresLoadSuccessful(proveedors: Proveedor[]) {
    this.proveedorsArray = proveedors;
  }

  showError(error: any) {
    this.closeAccept = this.closeError;

    this.messageIcon = 'pi-exclamation-triangle';
    this.messageService.add({
      key: 'aceptar',
      sticky: true,
      severity: 'error',
      summary: 'Error',
      detail: error
    });
  }

  closeError() {
    this.messageService.clear('aceptar');
  }

  rejectConfirm() {
    this.messageService.clear('confirmation');
  }

  updateTotal(producto?: CompraDetalle) {    

    if (producto) {      
      producto.fn_Total = producto.fi_Cantidad * producto.fn_PrecioCompra;      
    }

    var total = 0;

    for (var i = 0; i < this.productosArray.length; i++) {
      total += +this.productosArray[i].fn_Total;
    }

    this.compraModel.fn_Total = total;
    this.aplicaDescuento();
  }

  disableProducto() {
    var index = -1;
    index = this.productosArray.indexOf(this.productoSelected);
    if (index > -1) {
      this.productosArray.splice(index, 1);
    }

    this.rejectConfirm();
    this.updateTotal();
  }

  deleteProducto(producto: CompraDetalle) {
    this.productoSelected = producto;
    this.acceptConfirm = this.disableProducto;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: '¿Estas seguro?',
      detail: 'Eliminar Producto(s)',

    });
  }

  isValid() {
    return this.compraForm.status === 'VALID' && this.productosArray.length > 0;
  }

  aplicaDescuento() {
    if (this.compraModel.fn_DescuentoPorcentaje) {
      this.compraModel.fn_DescuentoCantidad = this.compraModel.fn_Total * this.compraModel.fn_DescuentoPorcentaje / 100;      
    }
    else {
      this.compraModel.fn_DescuentoCantidad = 0;
    }

    this.compraModel.fn_Total = this.compraModel.fn_Total - this.compraModel.fn_DescuentoCantidad;
  }

  realizarCompra() {
    this.compraModel.compraDetalles = this.productosArray;
    this.compraModel.fb_Estatus = true;

    this.comprasService.saveCompra(this.compraModel).subscribe((courseResult: number) => {

      this.closeAccept = this.finalizaCompra;

      this.messageIcon = 'pi-check-circle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'success',
        summary: 'Éxito',
        detail: 'Tus Cambios se han guardado con Éxito'
      });      
    }, error => {
      this.showError(error);
    });
  }

  finalizaCompra() {
    if (this.isEdition)
      this.cancelEdit.emit();
    else
      this.router.navigate(['gestioncompras']);
  }

  cancelarCompra() {
    if (this.isEdition)
      this.cancelEdit.emit();
    else
      this.finalizaCompra();
  }
}
