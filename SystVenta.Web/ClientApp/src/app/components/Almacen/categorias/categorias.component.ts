import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { Categoria } from '../../../models/almacen/categoria.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { AlmacenEndpoint } from '../../../services/almacen-endpoint.service';
import { EndpointFactory } from '../../../services/endpoint-factory.service';
import { AlmacenService } from '../../../services/almacen.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'almacen-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.css'],
  providers: [
    EndpointFactory,
    AlmacenEndpoint,
    AlmacenService,
    MessageService
  ]
})
/** Categorias component*/
export class CategoriasComponent implements AfterViewChecked, OnInit {
  categoriasArray: Categoria[];
  categoriaSelected: Categoria = new Categoria();
  isEdit: boolean = false;
  categoriasForBulkDelete: number[] = [];
  categoriaForm: FormGroup = new FormGroup({});
  messageIcon: string = "";
  selectedCategorias: Categoria[];

  acceptConfirm: any;

  cols: any[] = [
    { field: 'fc_NombreCategoria', header: 'Nombre Categoría', class: 'gridName' },
  ];

  /** Categorias ctor */
  constructor(private almacenService: AlmacenService, private messageService: MessageService) {

  }

  ngOnInit(): void {
    this.loadAllCategorias();

    this.categoriaForm = new FormGroup({
      categoriaName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
    });
  }

  ngAfterViewChecked(): void {
    if (document.querySelector('script[src="../../../assets/scripts/hoe.js"]') == null) {
      const node = document.createElement('script');
      node.src = '../../../assets/scripts/hoe.js';
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }  

  loadAllCategorias() {
    this.almacenService.getCategorias().subscribe((categorias: Categoria[]) => {
      this.onAllCategoriasLoadSuccessful(categorias);
    }, error => {
      this.showError(error);
    });
  }

  onAllCategoriasLoadSuccessful(categorias: Categoria[]) {
    this.categoriasArray = categorias;
  }

  showError(error: any) {
    this.messageIcon = 'pi-exclamation-triangle';
    this.messageService.add({
      key: 'aceptar',
      sticky: true,
      severity: 'error',
      summary: 'Error',
      detail: error
    });
  }

  newCategoria() {
    this.categoriaSelected = new Categoria();
    this.isEdit = true;
  }

  validateExistCategoria(categoria: Categoria) {
    if (typeof this.categoriasArray.find(x => x.fc_NombreCategoria.toLowerCase() == categoria.fc_NombreCategoria.toLowerCase() && x.fi_IdCatCategoria != categoria.fi_IdCatCategoria) !== 'undefined') {
      this.messageIcon = 'pi-exclamation-triangle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'error',
        summary: 'Error',
        detail: 'La Categoría ya existe'
      });
    }
    else {
      this.validateExistDisabledCategoria(categoria);
    }
  }

  validateExistDisabledCategoria(categoria: Categoria) {
    this.almacenService.getExistCategoria(categoria.fc_NombreCategoria).subscribe((existentCategoria: Categoria[]) => {
      if (existentCategoria.length > 0) {
        this.confirmDuplicateCategoria(existentCategoria[0]);
      }
      else {
        this.saveCategoria(categoria);
      }
    }, error => {
      this.showError(error);
    });
  }

  enableCategoria() {
    this.categoriaSelected.fb_Estatus = true;
    this.saveCategoria(this.categoriaSelected);

    this.rejectConfirm();
  }

  confirmDuplicateCategoria(categoria: Categoria) {
    this.categoriaSelected = categoria;
    this.acceptConfirm = this.enableCategoria;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: 'La Categoría ya existe, pero está deshabilitada',
      detail: '¿Deseas Habilitarla?',

    });
  }

  saveCategoria(categoria: Categoria) {
    this.almacenService.saveCategoria(categoria).subscribe((courseResult: number) => {
      this.messageIcon = 'pi-check-circle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'success',
        summary: 'Éxito',
        detail: 'Tus Cambios se han guardado con Éxito'
      });

      this.loadAllCategorias();
      this.categoriaForm.reset();
      this.isEdit = false;
    }, error => {
      this.showError(error);
    });
  }

  getCategoriaDetail(categoria: Categoria) {
    this.categoriaSelected = Object.assign({}, categoria);
    this.isEdit = true;
  }

  disableCategoria() {
    this.almacenService.getDeleteCategoria(this.categoriaSelected.fi_IdCatCategoria).subscribe(results => {
      var index = -1;
      index = this.categoriasArray.indexOf(this.categoriaSelected);
      if (index > -1) {
        this.categoriasArray.splice(index, 1);
      }

      this.rejectConfirm();
    }, error => {
      this.showError(error);
    });
  }

  deleteCategoria(categoria: Categoria) {

    this.categoriaSelected = categoria;
    this.acceptConfirm = this.disableCategoria;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: '¿Estas seguro?',
      detail: 'Eliminar Categoría',

    });
  }

  addTobulk(checkedCategoria: Categoria) {
    if (!checkedCategoria.isForBulkDelete) {
      checkedCategoria.isForBulkDelete = true;
      this.categoriasForBulkDelete.push(checkedCategoria.fi_IdCatCategoria);
    } else {
      checkedCategoria.isForBulkDelete = false;
      var index = -1;
      index = this.categoriasForBulkDelete.indexOf(checkedCategoria.fi_IdCatCategoria);
      if (index > -1) {
        this.categoriasForBulkDelete.splice(index, 1);
      }
    }
  }

  bulkDelete() {
    this.acceptConfirm = this.deleteBulk;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: '¿Estas seguro?',
      detail: 'Eliminar Categorías',

    });    
  }

  deleteBulk() {
    if (this.categoriasForBulkDelete.length > 0) {
      this.almacenService.getBulkDeleteCategoria(this.categoriasForBulkDelete).subscribe(results => {
        for (var i = 0; i < this.categoriasForBulkDelete.length; i++) {
          var index = -1;
          index = this.categoriasArray.indexOf(this.categoriasArray.find(x => x.fi_IdCatCategoria == this.categoriasForBulkDelete[i]));
          if (index > -1) {
            this.categoriasArray.splice(index, 1);
          }
        }
        this.bulkDeleteCompleted();
      }, error => {
        this.showError(error);
      });
    }
  }

  bulkDeleteCompleted() {
    this.categoriasForBulkDelete = [];
    this.rejectConfirm();
  }

  closeError() {
    this.messageService.clear('aceptar');
  }

  rejectConfirm() {
    this.messageService.clear('confirmation');
  }
}
