import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { Producto } from '../../../models/almacen/producto.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { AlmacenEndpoint } from '../../../services/almacen-endpoint.service';
import { EndpointFactory } from '../../../services/endpoint-factory.service';
import { MessageService } from 'primeng/api';
import { AlmacenService } from '../../../services/almacen.service';
import { Categoria } from '../../../models/almacen/categoria.model';
import { Marca } from '../../../models/almacen/marca.model';
import { UnidadMedida } from '../../../models/almacen/unidadMedida.model';

@Component({
  selector: 'almacen-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css'],
  providers: [
    EndpointFactory,
    AlmacenEndpoint,
    AlmacenService,
    MessageService
  ]
})
/** Productos component*/
export class ProductosComponent implements AfterViewChecked, OnInit {
  productosArray: Producto[];
  productoSelected: Producto = new Producto();
  isEdit: boolean = false;
  productosForBulkDelete: number[] = [];
  productoForm: FormGroup = new FormGroup({});
  messageIcon: string = "";
  selectedProductos: Producto[];

  categoriasArray: Categoria[];
  marcasArray: Marca[];
  unidadesMedidaArray: UnidadMedida[];

  acceptConfirm: any;

  public emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  cols: any[] = [
    { field: 'fc_Codigo', header: 'Código', class: 'gridName' },
    { field: 'fc_NombreProducto', header: 'Nombre Producto', class: 'gridName' },
    { field: 'fn_PrecioCompra', header: 'Precio Compra', class: 'Precio Compra' },
    { field: 'fn_PrecioVenta', header: 'Precio Venta', class: 'gridName' },
    { field: 'fi_Stock', header: 'Stock Actual', class: 'gridName' },
    { field: 'fi_StockMinimo', header: 'Stock Min.', class: 'gridName' },
    { field: 'nombreCategoria', header: 'Categoría', class: 'gridName' },
    { field: 'nombreMarca', header: 'Marca', class: 'gridName' },
  ];

  /** Productos ctor */
  constructor(private almacenService: AlmacenService, private messageService: MessageService) {

  }

  ngOnInit(): void {
    this.loadAllProductos();

    this.productoForm = new FormGroup({
      productoCodigo: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
      productoName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
      productoPrecioCompra: new FormControl('', [Validators.required, Validators.pattern("^[0-9\.]*$"), Validators.min(0), Validators.minLength(1), Validators.maxLength(100)]),
      productoPrecioVenta: new FormControl('', [Validators.required, Validators.pattern("^[0-9\.]*$"), Validators.min(0), Validators.minLength(1), Validators.maxLength(100)]),
      productoStock: new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.min(0), Validators.minLength(1), Validators.maxLength(4)]),
      productoStockMin: new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.min(0), Validators.minLength(1), Validators.maxLength(4)]),
      productoCategoria: new FormControl('', [Validators.required, Validators.min(1)]),
      productoMarca: new FormControl('', [Validators.required, Validators.min(1)]),
      productoUnidadMedida: new FormControl('', [Validators.required, Validators.min(1)]),
    });
  }

  ngAfterViewChecked(): void {
    if (document.querySelector('script[src="../../../assets/scripts/hoe.js"]') == null) {
      const node = document.createElement('script');
      node.src = '../../../assets/scripts/hoe.js';
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }  

  loadAllProductos() {
    this.almacenService.getProductos().subscribe((productos: Producto[]) => {
      this.onAllProductosLoadSuccessful(productos);
    }, error => {
      this.showError(error);
    });
  }

  onAllProductosLoadSuccessful(productos: Producto[]) {
    this.productosArray = productos;
  }

  showError(error: any) {
    this.messageIcon = 'pi-exclamation-triangle';
    this.messageService.add({
      key: 'aceptar',
      sticky: true,
      severity: 'error',
      summary: 'Error',
      detail: error
    });
  }

  newProducto() {
    this.productoSelected = new Producto();
    this.loadAllCategorias();
    this.loadAllMarcas();
    this.loadAllUnidadesMedida();
    this.isEdit = true;
  }

  validateExistProducto(producto: Producto) {
    if (typeof this.productosArray.find(x => x.fc_NombreProducto.toLowerCase() == producto.fc_NombreProducto.toLowerCase() && x.fi_IdCatProducto != producto.fi_IdCatProducto) !== 'undefined') {
      this.messageIcon = 'pi-exclamation-triangle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'error',
        summary: 'Error',
        detail: 'El Producto ya existe'
      });
    }
    else {
      this.validateExistDisabledProducto(producto);
    }
  }

  validateExistDisabledProducto(producto: Producto) {
    this.almacenService.getExistProducto(producto.fc_NombreProducto).subscribe((existentProductos: Producto[]) => {
      if (existentProductos.length > 0) {
        this.confirmDuplicateProducto(existentProductos[0]);
      }
      else {
        this.saveProducto(producto);
      }
    }, error => {
      this.showError(error);
    });
  }

  enableProducto() {
    this.productoSelected.fb_Estatus = true;
    this.saveProducto(this.productoSelected);

    this.rejectConfirm();
  }

  confirmDuplicateProducto(producto: Producto) {
    this.productoSelected = producto;
    this.acceptConfirm = this.enableProducto;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: 'El Producto ya existe, pero está deshabilitado',
      detail: '¿Deseas Habilitarlo?',

    });
  }

  saveProducto(producto: Producto) {
    this.almacenService.saveProducto(producto).subscribe((courseResult: number) => {
      this.messageIcon = 'pi-check-circle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'success',
        summary: 'Éxito',
        detail: 'Tus Cambios se han guardado con Éxito'
      });

      this.loadAllProductos();
      this.productoForm.reset();
      this.isEdit = false;
    }, error => {
      this.showError(error);
    });
  }

  getProductoDetail(producto: Producto) {
    this.productoSelected = Object.assign({}, producto);
    this.loadAllCategorias();
    this.loadAllMarcas();
    this.loadAllUnidadesMedida();
    this.isEdit = true;
  }

  loadAllCategorias() {
    this.almacenService.getCategorias().subscribe((categorias: Categoria[]) => {
      this.onAllCategoriasLoadSuccessful(categorias);
    }, error => {
      this.showError(error);
    });
  }

  onAllCategoriasLoadSuccessful(categorias: Categoria[]) {
    this.categoriasArray = categorias;
  }

  loadAllMarcas() {
    this.almacenService.getMarcas().subscribe((marcas: Marca[]) => {
      this.onAllMarcasLoadSuccessful(marcas);
    }, error => {
      this.showError(error);
    });
  }

  onAllMarcasLoadSuccessful(marcas: Marca[]) {
    this.marcasArray = marcas;
  }

  loadAllUnidadesMedida() {
    this.almacenService.getUnidadMedidas().subscribe((unidadesMedida: UnidadMedida[]) => {
      this.onAllUnidadesMedidaLoadSuccessful(unidadesMedida);
    }, error => {
      this.showError(error);
    });
  }

  onAllUnidadesMedidaLoadSuccessful(unidadesMedida: UnidadMedida[]) {
    this.unidadesMedidaArray = unidadesMedida;
  }

  disableProducto() {
    this.almacenService.getDeleteProducto(this.productoSelected.fi_IdCatProducto).subscribe(results => {
      var index = -1;
      index = this.productosArray.indexOf(this.productoSelected);
      if (index > -1) {
        this.productosArray.splice(index, 1);
      }

      this.rejectConfirm();
    }, error => {
      this.showError(error);
    });
  }

  deleteProducto(producto: Producto) {

    this.productoSelected = producto;
    this.acceptConfirm = this.disableProducto;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: '¿Estas seguro?',
      detail: 'Eliminar Producto',

    });
  }

  addTobulk(checkedProducto: Producto) {
    if (!checkedProducto.isForBulkDelete) {
      checkedProducto.isForBulkDelete = true;
      this.productosForBulkDelete.push(checkedProducto.fi_IdCatProducto);
    } else {
      checkedProducto.isForBulkDelete = false;
      var index = -1;
      index = this.productosForBulkDelete.indexOf(checkedProducto.fi_IdCatProducto);
      if (index > -1) {
        this.productosForBulkDelete.splice(index, 1);
      }
    }
  }

  bulkDelete() {
    this.acceptConfirm = this.deleteBulk;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: '¿Estas seguro?',
      detail: 'Eliminar Productos',

    });
  }

  deleteBulk() {
    if (this.productosForBulkDelete.length > 0) {
      this.almacenService.getBulkDeleteProducto(this.productosForBulkDelete).subscribe(results => {
        for (var i = 0; i < this.productosForBulkDelete.length; i++) {
          var index = -1;
          index = this.productosArray.indexOf(this.productosArray.find(x => x.fi_IdCatProducto == this.productosForBulkDelete[i]));
          if (index > -1) {
            this.productosArray.splice(index, 1);
          }
        }
        this.bulkDeleteCompleted();
      }, error => {
        this.showError(error);
      });
    }
  }

  bulkDeleteCompleted() {
    this.productosForBulkDelete = [];
    this.rejectConfirm();
  }

  closeError() {
    this.messageService.clear('aceptar');
  }

  rejectConfirm() {
    this.messageService.clear('confirmation');
  }
}
