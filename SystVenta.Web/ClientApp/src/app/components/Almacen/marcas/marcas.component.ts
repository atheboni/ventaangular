import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { Marca } from '../../../models/almacen/marca.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { AlmacenEndpoint } from '../../../services/almacen-endpoint.service';
import { EndpointFactory } from '../../../services/endpoint-factory.service';
import { AlmacenService } from '../../../services/almacen.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'almacen-marcas',
  templateUrl: './marcas.component.html',
  styleUrls: ['./marcas.component.css'],
  providers: [
    EndpointFactory,
    AlmacenEndpoint,
    AlmacenService,
    MessageService
  ]
})
/** Marcas component*/
export class MarcasComponent implements AfterViewChecked, OnInit {
  marcasArray: Marca[];
  marcaSelected: Marca = new Marca();
  isEdit: boolean = false;
  marcasForBulkDelete: number[] = [];
  marcaForm: FormGroup = new FormGroup({});
  messageIcon: string = "";
  selectedMarcas: Marca[];

  acceptConfirm: any;

  cols: any[] = [
    { field: 'fc_NombreMarca', header: 'Nombre Marca', class: 'gridName' },
  ];

  /** Marcas ctor */
  constructor(private almacenService: AlmacenService, private messageService: MessageService) {

  }

  ngOnInit(): void {
    this.loadAllMarcas();

    this.marcaForm = new FormGroup({
      marcaName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
    });
  }

  ngAfterViewChecked(): void {
    if (document.querySelector('script[src="../../../assets/scripts/hoe.js"]') == null) {
      const node = document.createElement('script');
      node.src = '../../../assets/scripts/hoe.js';
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }  

  loadAllMarcas() {
    this.almacenService.getMarcas().subscribe((marcas: Marca[]) => {
      this.onAllMarcasLoadSuccessful(marcas);
    }, error => {
      this.showError(error);
    });
  }

  onAllMarcasLoadSuccessful(marcas: Marca[]) {
    this.marcasArray = marcas;
  }

  showError(error: any) {
    this.messageIcon = 'pi-exclamation-triangle';
    this.messageService.add({
      key: 'aceptar',
      sticky: true,
      severity: 'error',
      summary: 'Error',
      detail: error
    });
  }

  newMarca() {
    this.marcaSelected = new Marca();
    this.isEdit = true;
  }

  validateExistMarca(marca: Marca) {
    if (typeof this.marcasArray.find(x => x.fc_NombreMarca.toLowerCase() == marca.fc_NombreMarca.toLowerCase() && x.fi_IdCatMarca != marca.fi_IdCatMarca) !== 'undefined') {
      this.messageIcon = 'pi-exclamation-triangle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'error',
        summary: 'Error',
        detail: 'La Marca ya existe'
      });
    }
    else {
      this.validateExistDisabledMarca(marca);
    }
  }

  validateExistDisabledMarca(marca: Marca) {
    this.almacenService.getExistMarca(marca.fc_NombreMarca).subscribe((existentMarca: Marca[]) => {
      if (existentMarca.length > 0) {
        this.confirmDuplicateMarca(existentMarca[0]);
      }
      else {
        this.saveMarca(marca);
      }
    }, error => {
      this.showError(error);
    });
  }

  enableMarca() {
    this.marcaSelected.fb_Estatus = true;
    this.saveMarca(this.marcaSelected);

    this.rejectConfirm();
  }

  confirmDuplicateMarca(marca: Marca) {
    this.marcaSelected = marca;
    this.acceptConfirm = this.enableMarca;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: 'La Marca ya existe, pero está deshabilitada',
      detail: '¿Deseas Habilitarla?',

    });
  }

  saveMarca(marca: Marca) {
    this.almacenService.saveMarca(marca).subscribe((courseResult: number) => {
      this.messageIcon = 'pi-check-circle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'success',
        summary: 'Éxito',
        detail: 'Tus Cambios se han guardado con Éxito'
      });

      this.loadAllMarcas();
      this.marcaForm.reset();
      this.isEdit = false;
    }, error => {
      this.showError(error);
    });
  }

  getMarcaDetail(marca: Marca) {
    this.marcaSelected = Object.assign({}, marca);
    this.isEdit = true;
  }

  disableMarca() {
    this.almacenService.getDeleteMarca(this.marcaSelected.fi_IdCatMarca).subscribe(results => {
      var index = -1;
      index = this.marcasArray.indexOf(this.marcaSelected);
      if (index > -1) {
        this.marcasArray.splice(index, 1);
      }

      this.rejectConfirm();
    }, error => {
      this.showError(error);
    });
  }

  deleteMarca(marca: Marca) {

    this.marcaSelected = marca;
    this.acceptConfirm = this.disableMarca;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: '¿Estas seguro?',
      detail: 'Eliminar Marca',

    });
  }

  addTobulk(checkedMarca: Marca) {
    if (!checkedMarca.isForBulkDelete) {
      checkedMarca.isForBulkDelete = true;
      this.marcasForBulkDelete.push(checkedMarca.fi_IdCatMarca);
    } else {
      checkedMarca.isForBulkDelete = false;
      var index = -1;
      index = this.marcasForBulkDelete.indexOf(checkedMarca.fi_IdCatMarca);
      if (index > -1) {
        this.marcasForBulkDelete.splice(index, 1);
      }
    }
  }

  bulkDelete() {
    this.acceptConfirm = this.deleteBulk;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: '¿Estas seguro?',
      detail: 'Eliminar Marcas',

    });
  }

  deleteBulk() {
    if (this.marcasForBulkDelete.length > 0) {
      this.almacenService.getBulkDeleteMarca(this.marcasForBulkDelete).subscribe(results => {
        for (var i = 0; i < this.marcasForBulkDelete.length; i++) {
          var index = -1;
          index = this.marcasArray.indexOf(this.marcasArray.find(x => x.fi_IdCatMarca == this.marcasForBulkDelete[i]));
          if (index > -1) {
            this.marcasArray.splice(index, 1);
          }
        }
        this.bulkDeleteCompleted();
      }, error => {
        this.showError(error);
      });
    }
  }

  bulkDeleteCompleted() {
    this.marcasForBulkDelete = [];
    this.rejectConfirm();
  }

  closeError() {
    this.messageService.clear('aceptar');
  }

  rejectConfirm() {
    this.messageService.clear('confirmation');
  }
}
