import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { UnidadMedida } from '../../../models/almacen/unidadMedida.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { AlmacenEndpoint } from '../../../services/almacen-endpoint.service';
import { EndpointFactory } from '../../../services/endpoint-factory.service';
import { AlmacenService } from '../../../services/almacen.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'almacen-unidades-medida',
  templateUrl: './unidades-medida.component.html',
  styleUrls: ['./unidades-medida.component.css'],
  providers: [
    EndpointFactory,
    AlmacenEndpoint,
    AlmacenService,
    MessageService
  ]
})
/** UnidadesMedida component*/
export class UnidadesMedidaComponent implements AfterViewChecked, OnInit {
  unidadesMedidaArray: UnidadMedida[];
  unidadMedidaSelected: UnidadMedida = new UnidadMedida();
  isEdit: boolean = false;
  unidadesMedidaForBulkDelete: number[] = [];
  unidadMedidaForm: FormGroup = new FormGroup({});
  messageIcon: string = "";
  selectedUnidadesMedida: UnidadMedida[];

  acceptConfirm: any;

  cols: any[] = [
    { field: 'fc_NombreUnidadMedida', header: 'Nombre Unidad de Medida', class: 'gridName' },
  ];

  /** UnidadesMedida ctor */
  constructor(private almacenService: AlmacenService, private messageService: MessageService) {

  }

  ngOnInit(): void {
    this.loadAllUnidadesMedida();

    this.unidadMedidaForm = new FormGroup({
      unidadMedidaName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
    });
  }

  ngAfterViewChecked(): void {
    if (document.querySelector('script[src="../../../assets/scripts/hoe.js"]') == null) {
      const node = document.createElement('script');
      node.src = '../../../assets/scripts/hoe.js';
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }  

  loadAllUnidadesMedida() {
    this.almacenService.getUnidadMedidas().subscribe((unidadesMedida: UnidadMedida[]) => {
      this.onAllUnidadesMedidaLoadSuccessful(unidadesMedida);
    }, error => {
      this.showError(error);
    });
  }

  onAllUnidadesMedidaLoadSuccessful(unidadesMedida: UnidadMedida[]) {
    this.unidadesMedidaArray = unidadesMedida;
  }

  showError(error: any) {
    this.messageIcon = 'pi-exclamation-triangle';
    this.messageService.add({
      key: 'aceptar',
      sticky: true,
      severity: 'error',
      summary: 'Error',
      detail: error
    });
  }

  newUnidadMedida() {
    this.unidadMedidaSelected = new UnidadMedida();
    this.isEdit = true;
  }

  validateExistUnidadMedida(unidadMedida: UnidadMedida) {
    if (typeof this.unidadesMedidaArray.find(x => x.fc_NombreUnidadMedida.toLowerCase() == unidadMedida.fc_NombreUnidadMedida.toLowerCase() && x.fi_IdCatUnidadMedida != unidadMedida.fi_IdCatUnidadMedida) !== 'undefined') {
      this.messageIcon = 'pi-exclamation-triangle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'error',
        summary: 'Error',
        detail: 'La Unidad de Medida ya existe'
      });
    }
    else {
      this.validateExistDisabledUnidadMedida(unidadMedida);
    }
  }

  validateExistDisabledUnidadMedida(unidadMedida: UnidadMedida) {
    this.almacenService.getExistUnidadMedida(unidadMedida.fc_NombreUnidadMedida).subscribe((existentUnidadMedida: UnidadMedida[]) => {
      if (existentUnidadMedida.length > 0) {
        this.confirmDuplicateUnidadMedida(existentUnidadMedida[0]);
      }
      else {
        this.saveUnidadMedida(unidadMedida);
      }
    }, error => {
      this.showError(error);
    });
  }

  enableUnidadMedida() {
    this.unidadMedidaSelected.fb_Estatus = true;
    this.saveUnidadMedida(this.unidadMedidaSelected);

    this.rejectConfirm();
  }

  confirmDuplicateUnidadMedida(unidadMedida: UnidadMedida) {
    this.unidadMedidaSelected = unidadMedida;
    this.acceptConfirm = this.enableUnidadMedida;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: 'La Unidad de Medida ya existe, pero está deshabilitada',
      detail: '¿Deseas Habilitarla?',

    });
  }

  saveUnidadMedida(unidadMedida: UnidadMedida) {
    this.almacenService.saveUnidadMedida(unidadMedida).subscribe((courseResult: number) => {
      this.messageIcon = 'pi-check-circle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'success',
        summary: 'Éxito',
        detail: 'Tus Cambios se han guardado con Éxito'
      });

      this.loadAllUnidadesMedida();
      this.unidadMedidaForm.reset();
      this.isEdit = false;
    }, error => {
      this.showError(error);
    });
  }

  getUnidadMedidaDetail(unidadMedida: UnidadMedida) {
    this.unidadMedidaSelected = Object.assign({}, unidadMedida);
    this.isEdit = true;
  }

  disableUnidadMedida() {
    this.almacenService.getDeleteUnidadMedida(this.unidadMedidaSelected.fi_IdCatUnidadMedida).subscribe(results => {
      var index = -1;
      index = this.unidadesMedidaArray.indexOf(this.unidadMedidaSelected);
      if (index > -1) {
        this.unidadesMedidaArray.splice(index, 1);
      }

      this.rejectConfirm();
    }, error => {
      this.showError(error);
    });
  }

  deleteUnidadMedida(unidadMedida: UnidadMedida) {

    this.unidadMedidaSelected = unidadMedida;
    this.acceptConfirm = this.disableUnidadMedida;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: '¿Estas seguro?',
      detail: 'Eliminar Unidad de Medida',

    });
  }

  addTobulk(checkedUnidadMedida: UnidadMedida) {
    if (!checkedUnidadMedida.isForBulkDelete) {
      checkedUnidadMedida.isForBulkDelete = true;
      this.unidadesMedidaForBulkDelete.push(checkedUnidadMedida.fi_IdCatUnidadMedida);
    } else {
      checkedUnidadMedida.isForBulkDelete = false;
      var index = -1;
      index = this.unidadesMedidaForBulkDelete.indexOf(checkedUnidadMedida.fi_IdCatUnidadMedida);
      if (index > -1) {
        this.unidadesMedidaForBulkDelete.splice(index, 1);
      }
    }
  }

  bulkDelete() {
    this.acceptConfirm = this.deleteBulk;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: '¿Estas seguro?',
      detail: 'Eliminar Unidades de Medida',

    });
  }

  deleteBulk() {
    if (this.unidadesMedidaForBulkDelete.length > 0) {
      this.almacenService.getBulkDeleteUnidadMedida(this.unidadesMedidaForBulkDelete).subscribe(results => {
        for (var i = 0; i < this.unidadesMedidaForBulkDelete.length; i++) {
          var index = -1;
          index = this.unidadesMedidaArray.indexOf(this.unidadesMedidaArray.find(x => x.fi_IdCatUnidadMedida == this.unidadesMedidaForBulkDelete[i]));
          if (index > -1) {
            this.unidadesMedidaArray.splice(index, 1);
          }
        }
        this.bulkDeleteCompleted();
      }, error => {
        this.showError(error);
      });
    }
  }

  bulkDeleteCompleted() {
    this.unidadesMedidaForBulkDelete = [];
    this.rejectConfirm();
  }

  closeError() {
    this.messageService.clear('aceptar');
  }

  rejectConfirm() {
    this.messageService.clear('confirmation');
  }
}
