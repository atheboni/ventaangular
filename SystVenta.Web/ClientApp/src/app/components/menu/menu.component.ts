import { Component, AfterViewChecked, OnInit } from '@angular/core';
import { MainService } from '../../services/main.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
/** Menu component*/
export class MenuComponent implements AfterViewChecked, OnInit {
  logo: string = "";

  /** Menu ctor */
  constructor(private mainService: MainService) {    
  }

  ngOnInit(): void {
    this.logo = this.mainService.logo;
  }

  ngAfterViewChecked(): void {    
    if (document.querySelector('script[src="../../../assets/scripts/hoe.js"]') == null) {
      const node = document.createElement('script');
      node.src = '../../../assets/scripts/hoe.js';
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }    
  }  
}
