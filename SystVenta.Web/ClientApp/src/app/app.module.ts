import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { MainService } from './services/main.service';
import { LocalStoreManager } from './services/local-store-manager.service';
import { AuthGuard } from './services/auth.guard';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { MenuComponent } from './components/menu/menu.component';

import { CategoriasComponent } from './components/Almacen/categorias/categorias.component';
import { ProductosComponent } from './components/Almacen/productos/productos.component';
import { MarcasComponent } from './components/Almacen/marcas/marcas.component';
import { UnidadesMedidaComponent } from './components/Almacen/unidades-medida/unidades-medida.component';

import { GestionComprasComponent } from './components/Compras/gestion-compras/gestion-compras.component';
import { NuevaCompraComponent } from './components/Compras/gestion-compras/nueva-compra.component';
import { ProveedoresComponent } from './components/Compras/proveedores/proveedores.component';

import { GestionVentasComponent } from './components/Ventas/gestion-ventas/gestion-ventas.component';
import { NuevaVentaComponent } from './components/Ventas/gestion-ventas/nueva-venta.component';
import { DevolucionesComponent } from './components/Ventas/devoluciones/devoluciones.component';
import { ClientesComponent } from './components/Ventas/clientes/clientes.component';

import { RepComprasComponent } from './components/Reportes/rep-compras/rep-compras.component';
import { RepVentasComponent } from './components/Reportes/rep-ventas/rep-ventas.component';
import { RepComprasVentasComponent } from './components/Reportes/rep-compras-ventas/rep-compras-ventas.component';
import { RepMayorStockComponent } from './components/Reportes/rep-mayor-stock/rep-mayor-stock.component';
import { RepMenorStockComponent } from './components/Reportes/rep-menor-stock/rep-menor-stock.component';
import { RepMasVendidosComponent } from './components/Reportes/rep-mas-vendidos/rep-mas-vendidos.component';

import { ConfiguracionGeneralComponent } from './components/Configuracion/configuracion-general/configuracion-general.component';
import { UsuariosComponent } from './components/Configuracion/usuarios/usuarios.component';
import { ComprobantesComponent } from './components/Configuracion/comprobantes/comprobantes.component';
import { MetodosPagoComponent } from './components/Configuracion/metodos-pago/metodos-pago.component';
import { RespaldoComponent } from './components/Configuracion/respaldo/respaldo.component';


import { TableModule } from 'primeng/table';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DialogModule } from 'primeng/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { ToastModule } from 'primeng/toast';
import { InputMaskModule } from 'primeng/inputmask';
import { ChartModule } from 'primeng/chart';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CalendarModule } from 'primeng/calendar';
import { FileUploadModule } from 'primeng/fileupload';

import { MonthTransform } from './pipes/month-transform.pipe';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MenuComponent,

    ProductosComponent,
    CategoriasComponent,
    MarcasComponent,
    UnidadesMedidaComponent,

    GestionComprasComponent,
    NuevaCompraComponent,
    ProveedoresComponent,

    GestionVentasComponent,
    NuevaVentaComponent,
    DevolucionesComponent,
    ClientesComponent,

    RepComprasComponent,
    RepVentasComponent,
    RepComprasVentasComponent,
    RepMayorStockComponent,
    RepMenorStockComponent,
    RepMasVendidosComponent,

    ConfiguracionGeneralComponent,
    UsuariosComponent,
    ComprobantesComponent,
    MetodosPagoComponent,
    RespaldoComponent,

    MonthTransform
    
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full', outlet: 'deslogueado' },

      { path: 'menu', component: MenuComponent, canActivate: [AuthGuard] },


      { path: 'productos', component: ProductosComponent, canActivate: [AuthGuard] },
      { path: 'categorias', component: CategoriasComponent, canActivate: [AuthGuard] },
      { path: 'marcas', component: MarcasComponent, canActivate: [AuthGuard] },
      { path: 'unidadesmedida', component: UnidadesMedidaComponent, canActivate: [AuthGuard] },

      { path: 'gestioncompras', component: GestionComprasComponent, canActivate: [AuthGuard] },
      { path: 'nuevacompra', component: NuevaCompraComponent, canActivate: [AuthGuard] },
      { path: 'proveedores', component: ProveedoresComponent, canActivate: [AuthGuard] },

      { path: 'gestionventas', component: GestionVentasComponent, canActivate: [AuthGuard] },
      { path: 'nuevaventa', component: NuevaVentaComponent, canActivate: [AuthGuard] },
      { path: 'devoluciones', component: DevolucionesComponent, canActivate: [AuthGuard] },
      { path: 'clientes', component: ClientesComponent, canActivate: [AuthGuard] },

      { path: 'repcompras', component: RepComprasComponent, canActivate: [AuthGuard] },
      { path: 'repventas', component: RepVentasComponent, canActivate: [AuthGuard] },
      { path: 'repcomprasventas', component: RepComprasVentasComponent, canActivate: [AuthGuard] },
      { path: 'repmayorstock', component: RepMayorStockComponent, canActivate: [AuthGuard] },
      { path: 'repmenorstock', component: RepMenorStockComponent, canActivate: [AuthGuard] },
      { path: 'repmasvendidos', component: RepMasVendidosComponent, canActivate: [AuthGuard] },

      { path: 'confgeneral', component: ConfiguracionGeneralComponent, canActivate: [AuthGuard] },
      { path: 'usuarios', component: UsuariosComponent, canActivate: [AuthGuard] },
      { path: 'comprobantes', component: ComprobantesComponent, canActivate: [AuthGuard] },
      { path: 'metodospago', component: MetodosPagoComponent, canActivate: [AuthGuard] },
      { path: 'respaldo', component: RespaldoComponent, canActivate: [AuthGuard] },      
    ]),
    ReactiveFormsModule,
    TableModule,
    ConfirmDialogModule,
    DialogModule,
    BrowserAnimationsModule,
    ToastModule,
    InputMaskModule,
    ChartModule,
    AutoCompleteModule,
    CalendarModule,
    FileUploadModule
  ],
  providers: [
    MainService,
    LocalStoreManager,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
