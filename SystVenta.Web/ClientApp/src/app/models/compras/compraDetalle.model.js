"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CompraDetalle = /** @class */ (function () {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    function CompraDetalle() {
        this.fb_Estatus = true;
        this.isForBulkDelete = false;
    }
    return CompraDetalle;
}());
exports.CompraDetalle = CompraDetalle;
//# sourceMappingURL=compraDetalle.model.js.map