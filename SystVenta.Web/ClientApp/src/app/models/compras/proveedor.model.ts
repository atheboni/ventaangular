export class Proveedor {
  // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
  constructor() {

  }

  public fi_IdCatProveedor: number;
  public fc_RazonSocial: string;
  public fc_NombreProveedor: string;
  public fc_RFC: string;
  public fc_Email: string;
  public fc_Direccion: string;
  public fc_Telefono: string;   
  public fb_Estatus: boolean = true;
  public isForBulkDelete: boolean = false;
}
