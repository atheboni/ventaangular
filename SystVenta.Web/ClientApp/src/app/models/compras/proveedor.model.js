"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Proveedor = /** @class */ (function () {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    function Proveedor() {
        this.fb_Estatus = true;
        this.isForBulkDelete = false;
    }
    return Proveedor;
}());
exports.Proveedor = Proveedor;
//# sourceMappingURL=proveedor.model.js.map