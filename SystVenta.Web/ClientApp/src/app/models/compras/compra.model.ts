import { CompraDetalle } from "./compraDetalle.model";

export class Compra {
  // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
  constructor() {

  }

  public fi_IdCnfCompra: number;
  public fc_NumeroCompra: string;
  public fn_DescuentoPorcentaje: number;
  public fn_DescuentoCantidad: number;  
  public fd_FechaCompra: Date = new Date();
  public metodoPago: number = 0;
  public comprobante: number = 0;
  public proveedor: number = 0;
  public fn_Total: number = 0;
  public fb_Estatus: boolean = true;
  public compraDetalles: CompraDetalle[] = [];
}
