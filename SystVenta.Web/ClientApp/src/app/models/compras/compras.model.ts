export class Compras {
  // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
  constructor() {

  }

  public fi_IdCnfCompra: number;
  public fc_NumeroCompra: string;
  public fd_FechaCompra: Date;
  public proveedor: string;
  public fn_Total: number;  
  public fb_Estatus: boolean = true;
  public isForBulkDelete: boolean = false;
}
