export class CompraDetalle {
  // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
  constructor() {

  }

  public fi_IdCnfCompraDetalle: number;
  public fc_Codigo: string;
  public fc_NombreProducto: string;
  public fi_Cantidad: number;
  public fn_PrecioCompra: number;
  public fn_Total: number;  
  public fb_Estatus: boolean = true;
  public isForBulkDelete: boolean = false;
}
