"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Compra = /** @class */ (function () {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    function Compra() {
        this.fd_FechaCompra = new Date();
        this.metodoPago = 0;
        this.comprobante = 0;
        this.proveedor = 0;
        this.fn_Total = 0;
        this.fb_Estatus = true;
        this.compraDetalles = [];
    }
    return Compra;
}());
exports.Compra = Compra;
//# sourceMappingURL=compra.model.js.map