"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Compras = /** @class */ (function () {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    function Compras() {
        this.fb_Estatus = true;
        this.isForBulkDelete = false;
    }
    return Compras;
}());
exports.Compras = Compras;
//# sourceMappingURL=compras.model.js.map