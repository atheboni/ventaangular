"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Usuario = /** @class */ (function () {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    function Usuario() {
        this.fi_NivelAcceso = 0;
        this.fb_Estatus = true;
        this.isForBulkDelete = false;
    }
    return Usuario;
}());
exports.Usuario = Usuario;
//# sourceMappingURL=usuario.model.js.map