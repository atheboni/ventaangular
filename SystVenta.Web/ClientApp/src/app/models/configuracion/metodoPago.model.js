"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MetodoPago = /** @class */ (function () {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    function MetodoPago() {
        this.fb_Estatus = true;
        this.isForBulkDelete = false;
    }
    return MetodoPago;
}());
exports.MetodoPago = MetodoPago;
//# sourceMappingURL=metodoPago.model.js.map