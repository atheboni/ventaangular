export class ConfiguracionGeneral {
  // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
  constructor() {

  }

  public fi_IdCnfGeneral: number;
  public fc_NombreEmpresa: string;  
  public fc_Logo: string;
  public imgBase64: string;
}
