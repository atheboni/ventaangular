export class Usuario {
  // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
  constructor() {

  }

  public fi_IdCatUsuario: number;
  public fc_NombreUsuario: string;
  public fc_Usuario: string;
  public fi_NivelAcceso: number = 0;
  public fc_Direccion: string;
  public fc_Telefono: string;
  public fc_Password: string;
  public fc_ConfirmPassword: string;
  public fb_Estatus: boolean = true;
  public isForBulkDelete: boolean = false;
}
