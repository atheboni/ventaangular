export class Login {
  // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
  constructor() {

  }

  public userName: string;
  public password: string; 
}
