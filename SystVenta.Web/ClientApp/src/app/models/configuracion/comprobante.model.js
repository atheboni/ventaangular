"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Comprobante = /** @class */ (function () {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    function Comprobante() {
        this.fb_Estatus = true;
        this.isForBulkDelete = false;
    }
    return Comprobante;
}());
exports.Comprobante = Comprobante;
//# sourceMappingURL=comprobante.model.js.map