export class MetodoPago {
  // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
  constructor() {

  }

  public fi_IdCatMetodoPago: number;
  public fc_NombreMetodoPago: string;  
  public fb_Estatus: boolean = true;
  public isForBulkDelete: boolean = false;
}
