export class ReporteCompras {
  // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
  constructor() {

  }

  public mes: number;
  public anio: number;
  public fn_Total: number;
  public Cantidad: number;
  public Porcentaje: number;
  
}
