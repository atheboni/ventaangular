export class ReporteFiltros {
  // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
  constructor() {

  }

  public fd_FechaDesde: Date;
  public fd_FechaHasta: Date;
  
}
