import { ReporteVentas } from "./reporteVentas.model";

export class ReporteMasVendidos {
  // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
  constructor() {

  }

  public fi_IdCatProducto: number;
  public fc_NombreProducto: string;
  public ventas: ReporteVentas[];
  
}
