export class ReporteComprasVentas {
  // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
  constructor() {

  }

  public mes: number;
  public Compras: number;
  public Ventas: number;
  
}
