"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Cliente = /** @class */ (function () {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    function Cliente() {
        this.fb_Estatus = true;
        this.isForBulkDelete = false;
    }
    return Cliente;
}());
exports.Cliente = Cliente;
//# sourceMappingURL=cliente.model.js.map