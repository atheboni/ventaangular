import { VentaDetalle } from "./ventaDetalle.model";

export class Venta {
  // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
  constructor() {

  }

  public fi_IdCnfVenta: number;
  public fc_NumeroVenta: string;
  public fn_DescuentoPorcentaje: number;
  public fn_DescuentoCantidad: number;  
  public fd_FechaVenta: Date = new Date();
  public metodoPago: number = 0;
  public comprobante: number = 0;
  public cliente: number = 0;
  public fn_Total: number = 0;
  public fb_Estatus: boolean = true;
  public ventaDetalles: VentaDetalle[] = [];
}
