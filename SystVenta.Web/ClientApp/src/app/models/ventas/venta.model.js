"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Venta = /** @class */ (function () {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    function Venta() {
        this.fd_FechaVenta = new Date();
        this.metodoPago = 0;
        this.comprobante = 0;
        this.cliente = 0;
        this.fn_Total = 0;
        this.fb_Estatus = true;
        this.ventaDetalles = [];
    }
    return Venta;
}());
exports.Venta = Venta;
//# sourceMappingURL=venta.model.js.map