"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Ventas = /** @class */ (function () {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    function Ventas() {
        this.fb_Estatus = true;
        this.isForBulkDelete = false;
    }
    return Ventas;
}());
exports.Ventas = Ventas;
//# sourceMappingURL=ventas.model.js.map