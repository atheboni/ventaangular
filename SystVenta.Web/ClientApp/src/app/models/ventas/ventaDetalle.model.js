"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VentaDetalle = /** @class */ (function () {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    function VentaDetalle() {
        this.fb_Estatus = true;
        this.isForBulkDelete = false;
    }
    return VentaDetalle;
}());
exports.VentaDetalle = VentaDetalle;
//# sourceMappingURL=ventaDetalle.model.js.map