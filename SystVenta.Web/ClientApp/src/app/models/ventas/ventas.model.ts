export class Ventas {
  // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
  constructor() {

  }

  public fi_IdCnfVenta: number;
  public fc_NumeroVenta: string;
  public fd_FechaVenta: Date;
  public cliente: string;
  public metodoPago: string;
  public fn_Total: number;  
  public fb_Estatus: boolean = true;
  public isForBulkDelete: boolean = false;
}
