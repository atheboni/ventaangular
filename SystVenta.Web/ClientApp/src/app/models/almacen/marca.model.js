"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Marca = /** @class */ (function () {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    function Marca() {
        this.fb_Estatus = true;
        this.isForBulkDelete = false;
    }
    return Marca;
}());
exports.Marca = Marca;
//# sourceMappingURL=marca.model.js.map