"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Categoria = /** @class */ (function () {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    function Categoria() {
        this.fb_Estatus = true;
        this.isForBulkDelete = false;
    }
    return Categoria;
}());
exports.Categoria = Categoria;
//# sourceMappingURL=categoria.model.js.map