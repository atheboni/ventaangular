export class Categoria {
  // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
  constructor() {

  }

  public fi_IdCatCategoria: number;
  public fc_NombreCategoria: string;  
  public fb_Estatus: boolean = true;
  public isForBulkDelete: boolean = false;
}
