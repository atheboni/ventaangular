"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Producto = /** @class */ (function () {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    function Producto() {
        this.categoria = 0;
        this.marca = 0;
        this.unidadMedida = 0;
        this.fb_Estatus = true;
        this.isForBulkDelete = false;
    }
    return Producto;
}());
exports.Producto = Producto;
//# sourceMappingURL=producto.model.js.map