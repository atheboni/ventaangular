export class Marca {
  // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
  constructor() {

  }

  public fi_IdCatMarca: number;
  public fc_NombreMarca: string;  
  public fb_Estatus: boolean = true;
  public isForBulkDelete: boolean = false;
}
