export class Producto {
  // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
  constructor() {

  }

  public fi_IdCatProducto: number;
  public fc_Codigo: string;
  public fc_NombreProducto: string;
  public fn_PrecioCompra: number;
  public fn_PrecioVenta: number;
  public fi_Stock: number;
  public fi_StockMinimo: number;
  public categoria: number = 0;
  public nombreCategoria: string;
  public marca: number = 0;
  public nombreMarca: string;
  public unidadMedida: number = 0;
  public nombreUnidadMedida: string;
  public fb_Estatus: boolean = true;
  public isForBulkDelete: boolean = false;
  public nombreAutocomplete: string;
}
