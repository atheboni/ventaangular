﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SystVenta.WebApi.Models;
using SystVenta.WebApi.Models.Configuracion;
using SystVenta.WebApi.ViewModels;

namespace SystVenta.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ConfiguracionController : ControllerBase
    {
        private readonly SystVentaDbContext _context;
        private IHostingEnvironment _hostingEnvironment;

        public ConfiguracionController(SystVentaDbContext context, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }

        #region Login
        [HttpPost]
        public IActionResult Login([FromBody]LoginViewModel loginViewModel)
        {
            try
            {
                var usuario = _context.Usuario.Where(m => m.fc_Usuario.ToUpper() == loginViewModel.userName.ToUpper() && m.fc_Password == loginViewModel.password);
                var result = Mapper.Map<IEnumerable<UsuarioViewModel>>(usuario);
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Comprobantes from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }
        #endregion

        #region General
        [HttpGet]
        public IActionResult GetConfiguracionGeneral()
        {
            try
            {
                var configuracionGeneral = _context.General;
                var result = Mapper.Map<IEnumerable<GeneralViewModel>>(configuracionGeneral);                

                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Configuracion General from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPost]
        public IActionResult SaveConfiguracionGeneral([FromBody]General newGeneral)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (newGeneral == null)
                        return BadRequest($"{nameof(newGeneral)} cannot be null");

                    _context.General.Update(newGeneral);

                    var result = _context.SaveChanges();
                    return Ok(result);
                }
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed saving Configuracion General: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }

            return BadRequest(ModelState);
        }

        [HttpPost, DisableRequestSizeLimit]
        public IActionResult UploadLogo()
        {
            try
            {
                var file = Request.Form.Files[0];

                using (var stream = new MemoryStream())
                {
                    file.CopyToAsync(stream);
                    var configuracionGeneral = _context.General.First();
                    configuracionGeneral.fc_Logo = stream.ToArray();
                }

                _context.SaveChanges();                
                return Ok("Upload Successful.");
            }
            catch (System.Exception ex)
            {
                return StatusCode(404, "Upload Failed: " + ex.Message);
            }
        }
        #endregion

        #region Comprobante       
        [HttpGet]
        public IActionResult GetComprobantes()
        {
            try
            {
                var comprobantes = _context.Comprobante.Where(m => m.fb_Estatus);
                return Ok(comprobantes);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Comprobantes from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet("{comprobanteId}")]
        public IActionResult GetComprobante(int comprobanteId)
        {
            try
            {
                var comprobante = _context.Comprobante.Find(comprobanteId);
                return Ok(comprobante);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Comprobante from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet("{comprobanteNombre}")]
        public IActionResult GetComprobanteExist(string comprobanteNombre)
        {
            try
            {
                var Comprobante = _context.Comprobante.Where(m => m.fc_NombreComprobante.ToUpper() == comprobanteNombre.ToUpper() && !m.fb_Estatus);
                return Ok(Comprobante);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting Comprobante from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPost]
        public IActionResult SaveComprobante([FromBody]Comprobante newComprobante)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (newComprobante == null)
                        return BadRequest($"{nameof(newComprobante)} cannot be null");

                    if (newComprobante.fi_IdCatComprobante == 0)
                        _context.Comprobante.Add(newComprobante);
                    else
                        _context.Comprobante.Update(newComprobante);

                    var result = _context.SaveChanges();
                    return Ok(result);
                }
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed saving Comprobante: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{comprobanteId}")]
        public IActionResult DeleteComprobante(int comprobanteId)
        {
            try
            {
                Comprobante deletedComprobante = _context.Comprobante.Find(comprobanteId);
                deletedComprobante.fb_Estatus = false;
                _context.Comprobante.Update(deletedComprobante);
                int result = _context.SaveChanges();
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed deleting Comprobante from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPut]
        public IActionResult BulkDeleteComprobante([FromBody]int[] deletedComprobantes)
        {
            try
            {
                foreach (int deletedComprobanteId in deletedComprobantes)
                {
                    Comprobante deletedComprobante = _context.Comprobante.Find(deletedComprobanteId);
                    deletedComprobante.fb_Estatus = false;
                    _context.Comprobante.Update(deletedComprobante);
                }

                int result = _context.SaveChanges();
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed deleting Comprobantes from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }
        #endregion

        #region MetodoPago       
        [HttpGet]
        public IActionResult GetMetodosPago()
        {
            try
            {
                var metodosPago = _context.MetodoPago.Where(m => m.fb_Estatus);
                return Ok(metodosPago);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for MetodosPago from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet("{metodoPagoId}")]
        public IActionResult GetMetodoPago(int metodoPagoId)
        {
            try
            {
                var MetodoPago = _context.MetodoPago.Find(metodoPagoId);
                return Ok(MetodoPago);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for MetodoPago from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet("{metodoPagoNombre}")]
        public IActionResult GetMetodoPagoExist(string metodoPagoNombre)
        {
            try
            {
                var MetodoPago = _context.MetodoPago.Where(m => m.fc_NombreMetodoPago.ToUpper() == metodoPagoNombre.ToUpper() && !m.fb_Estatus);
                return Ok(MetodoPago);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting MetodoPago from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPost]
        public IActionResult SaveMetodoPago([FromBody]MetodoPago newMetodoPago)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (newMetodoPago == null)
                        return BadRequest($"{nameof(newMetodoPago)} cannot be null");

                    if (newMetodoPago.fi_IdCatMetodoPago == 0)
                        _context.MetodoPago.Add(newMetodoPago);
                    else
                        _context.MetodoPago.Update(newMetodoPago);

                    var result = _context.SaveChanges();
                    return Ok(result);
                }
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed saving MetodoPago: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{metodoPagoId}")]
        public IActionResult DeleteMetodoPago(int metodoPagoId)
        {
            try
            {
                MetodoPago deletedMetodoPago = _context.MetodoPago.Find(metodoPagoId);
                deletedMetodoPago.fb_Estatus = false;
                _context.MetodoPago.Update(deletedMetodoPago);
                int result = _context.SaveChanges();
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed deleting MetodoPago from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPut]
        public IActionResult BulkDeleteMetodoPago([FromBody]int[] deletedMetodoPagos)
        {
            try
            {
                foreach (int deletedMetodoPagoId in deletedMetodoPagos)
                {
                    MetodoPago deletedMetodoPago = _context.MetodoPago.Find(deletedMetodoPagoId);
                    deletedMetodoPago.fb_Estatus = false;
                    _context.MetodoPago.Update(deletedMetodoPago);
                }

                int result = _context.SaveChanges();
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed deleting MetodoPagos from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }
        #endregion

        #region Usuario       
        [HttpGet]
        public IActionResult GetUsuarios()
        {
            try
            {
                var usuarios = _context.Usuario.Where(m => m.fb_Estatus);
                var result = Mapper.Map<IEnumerable<UsuarioViewModel>>(usuarios);
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Usuarios from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet("{usuarioId}")]
        public IActionResult GetUsuario(int usuarioId)
        {
            try
            {
                var usuario = _context.Usuario.Find(usuarioId);
                var result = Mapper.Map<UsuarioViewModel>(usuario);
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Usuario from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet("{usuarioNombre}")]
        public IActionResult GetUsuarioExist(string usuarioNombre)
        {
            try
            {
                var usuario = _context.Usuario.Where(m => m.fc_Usuario.ToUpper() == usuarioNombre.ToUpper() && !m.fb_Estatus);
                var result = Mapper.Map<IEnumerable<UsuarioViewModel>>(usuario);
                return Ok(result);

            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting Usuario from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPost]
        public IActionResult SaveUsuario([FromBody]Usuario newUsuario)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (newUsuario == null)
                        return BadRequest($"{nameof(newUsuario)} cannot be null");

                    if (newUsuario.fi_IdCatUsuario == 0)
                        _context.Usuario.Add(newUsuario);
                    else
                        _context.Usuario.Update(newUsuario);

                    var result = _context.SaveChanges();
                    return Ok(result);
                }
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed saving Usuario: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{usuarioId}")]
        public IActionResult DeleteUsuario(int usuarioId)
        {
            try
            {
                Usuario deletedUsuario = _context.Usuario.Find(usuarioId);
                deletedUsuario.fb_Estatus = false;
                _context.Usuario.Update(deletedUsuario);
                int result = _context.SaveChanges();
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed deleting Usuario from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPut]
        public IActionResult BulkDeleteUsuario([FromBody]int[] deletedUsuarios)
        {
            try
            {
                foreach (int deletedUsuarioId in deletedUsuarios)
                {
                    Usuario deletedUsuario = _context.Usuario.Find(deletedUsuarioId);
                    deletedUsuario.fb_Estatus = false;
                    _context.Usuario.Update(deletedUsuario);
                }

                int result = _context.SaveChanges();
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed deleting Usuarios from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }
        #endregion
    }
}