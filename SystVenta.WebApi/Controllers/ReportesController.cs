﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SystVenta.WebApi.Models;
using SystVenta.WebApi.ViewModels;

namespace SystVenta.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ReportesController : ControllerBase
    {
        private readonly SystVentaDbContext _context;

        public ReportesController(SystVentaDbContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult GetReporteCompras([FromBody]ReporteFiltrosViewModel reporteFiltros)
        {
            try
            {
                var query = _context.Compra
                    .Where(m => m.fd_FechaCompra.Date >= reporteFiltros.fd_FechaDesde.Date && m.fd_FechaCompra.Date <= reporteFiltros.fd_FechaHasta.Date.AddMonths(1).AddDays(-1))
                    .GroupBy(c => new { c.fd_FechaCompra.Month, c.fd_FechaCompra.Year })
                    .Select(g => new ReporteComprasViewModel()
                    {
                        Anio = g.Key.Year,
                        Mes = g.Key.Month,
                        fn_Total = g.Sum(m => m.fn_Total),
                        Cantidad = g.Count()
                    }).ToList();

                var totalAnual = query.Sum(m => m.fn_Total);

                foreach (ReporteComprasViewModel reporteCompras in query)
                {
                    reporteCompras.Porcentaje = (reporteCompras.fn_Total * 100) / totalAnual;
                }

                return Ok(query);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Reporte de Compras from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPost]
        public IActionResult GetReporteVentas([FromBody]ReporteFiltrosViewModel reporteFiltros)
        {
            try
            {
                var query = _context.Venta
                    .Where(m => m.fd_FechaVenta.Date >= reporteFiltros.fd_FechaDesde.Date && m.fd_FechaVenta.Date <= reporteFiltros.fd_FechaHasta.Date.AddMonths(1).AddDays(-1))
                    .GroupBy(c => new { c.fd_FechaVenta.Month, c.fd_FechaVenta.Year })
                    .Select(g => new ReporteVentasViewModel()
                    {
                        Anio = g.Key.Year,
                        Mes = g.Key.Month,
                        fn_Total = g.Sum(m => m.fn_Total),
                        Cantidad = g.Count()
                    }).ToList();

                var totalAnual = query.Sum(m => m.fn_Total);

                foreach (ReporteVentasViewModel reporteVentas in query)
                {
                    reporteVentas.Porcentaje = (reporteVentas.fn_Total * 100) / totalAnual;
                }

                return Ok(query);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Reporte de Ventas from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet]
        public IActionResult GetMayorStock()
        {
            try
            {
                var productos = _context.Producto
                    .Where(m => m.fb_Estatus)
                    .OrderByDescending(m => m.fi_Stock)
                    .ThenBy(m => m.fc_NombreProducto)
                    .Take(6);

                var result = Mapper.Map<IEnumerable<ProductoViewModel>>(productos);
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Reporte de Mayor Stock from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet]
        public IActionResult GetMenorStock()
        {
            try
            {
                var productos = _context.Producto
                    .Where(m => m.fb_Estatus)
                    .OrderBy(m => m.fi_Stock)
                    .ThenBy(m => m.fc_NombreProducto)
                    .Take(5);

                var result = Mapper.Map<IEnumerable<ProductoViewModel>>(productos);
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Reporte de Menor Stock from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet]
        public IActionResult GetMasVendidos()
        {
            try
            {
                var masVendidos = _context.CompraDetalle
                    .Include(m => m.Producto)
                    .Where(m => m.fb_Estatus)
                    .OrderByDescending(m => m.fi_Cantidad)
                    .ThenBy(m => m.Producto.fc_NombreProducto)                    
                    .Take(5)
                    .Select(p => new ReporteMasVendidosViewModel()
                    {
                        fi_IdCatProducto = p.Producto.fi_IdCatProducto,
                        fc_NombreProducto = p.Producto.fc_NombreProducto
                    }).Distinct().ToList();

                foreach (ReporteMasVendidosViewModel reporteMasVendidos in masVendidos)
                {                    
                    var obj = _context.VentaDetalle
                        .Include(m => m.Venta)
                        .Include(m => m.Producto)
                        .Where(m => m.Venta.fd_FechaVenta.Year == DateTime.Now.Year && m.Producto.fi_IdCatProducto == reporteMasVendidos.fi_IdCatProducto).ToList();

                    reporteMasVendidos.Ventas = obj.GroupBy(m => m.Venta.fd_FechaVenta.Month)
                        .Select(g => new ReporteVentasViewModel()
                        {
                            Mes = g.Key,
                            fn_Total = g.Sum(m => m.fn_Total),
                            Cantidad = g.Sum(m => m.fi_Cantidad)
                        }).ToList();
                }

                return Ok(masVendidos);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Reporte Mas Vendidos from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }
    }
}