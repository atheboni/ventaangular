﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SystVenta.WebApi.Models;
using SystVenta.WebApi.Models.Almacen;
using SystVenta.WebApi.ViewModels;

namespace SystVenta.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AlmacenController : ControllerBase
    {
        private readonly SystVentaDbContext _context;

        public AlmacenController(SystVentaDbContext context)
        {
            _context = context;
        }

        #region Categoria       
        [HttpGet]
        public IActionResult GetCategorias()
        {
            try
            {
                var categorias = _context.Categoria.Where(m => m.fb_Estatus);
                return Ok(categorias);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for categorias from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet("{categoriaId}")]
        public IActionResult GetCategoria(int categoriaId)
        {
            try
            {
                var categoria = _context.Categoria.Find(categoriaId);
                return Ok(categoria);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for categoria from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet("{categoriaNombre}")]
        public IActionResult GetCategoriaExist(string categoriaNombre)
        {
            try
            {
                var categoria = _context.Categoria.Where(m => m.fc_NombreCategoria.ToUpper() == categoriaNombre.ToUpper() && !m.fb_Estatus);
                return Ok(categoria);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting Categoria from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPost]
        public IActionResult SaveCategoria([FromBody]Categoria newCategoria)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (newCategoria == null)
                        return BadRequest($"{nameof(newCategoria)} cannot be null");

                    if (newCategoria.fi_IdCatCategoria == 0)
                        _context.Categoria.Add(newCategoria);
                    else
                        _context.Categoria.Update(newCategoria);

                    var result = _context.SaveChanges();
                    return Ok(result);
                }
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed saving categoria: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{categoriaId}")]
        public IActionResult DeleteCategoria(int categoriaId)
        {
            try
            {
                Categoria deletedCategoria = _context.Categoria.Find(categoriaId);
                deletedCategoria.fb_Estatus = false;
                _context.Categoria.Update(deletedCategoria);
                int result = _context.SaveChanges();
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed deleting categoria from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPut]
        public IActionResult BulkDeleteCategoria([FromBody]int[] deletedCategorias)
        {
            try
            {
                foreach (int deletedCategoriaId in deletedCategorias)
                {
                    Categoria deletedCategoria = _context.Categoria.Find(deletedCategoriaId);
                    deletedCategoria.fb_Estatus = false;
                    _context.Categoria.Update(deletedCategoria);
                }

                int result = _context.SaveChanges();
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed deleting Categorias from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }
        #endregion

        #region Marca       
        [HttpGet]
        public IActionResult GetMarcas()
        {
            try
            {
                var marcas = _context.Marca.Where(m => m.fb_Estatus);
                return Ok(marcas);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Marcas from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet("{marcaId}")]
        public IActionResult GetMarca(int marcaId)
        {
            try
            {
                var marca = _context.Marca.Find(marcaId);
                return Ok(marca);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Marca from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet("{marcaNombre}")]
        public IActionResult GetMarcaExist(string marcaNombre)
        {
            try
            {
                var marca = _context.Marca.Where(m => m.fc_NombreMarca.ToUpper() == marcaNombre.ToUpper() && !m.fb_Estatus);
                return Ok(marca);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting Marca from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPost]
        public IActionResult SaveMarca([FromBody]Marca newMarca)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (newMarca == null)
                        return BadRequest($"{nameof(newMarca)} cannot be null");

                    if (newMarca.fi_IdCatMarca == 0)
                        _context.Marca.Add(newMarca);
                    else
                        _context.Marca.Update(newMarca);

                    var result = _context.SaveChanges();
                    return Ok(result);
                }
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed saving Marca: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{marcaId}")]
        public IActionResult DeleteMarca(int marcaId)
        {
            try
            {
                Marca deletedMarca = _context.Marca.Find(marcaId);
                deletedMarca.fb_Estatus = false;
                _context.Marca.Update(deletedMarca);
                int result = _context.SaveChanges();
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed deleting Marca from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPut]
        public IActionResult BulkDeleteMarca([FromBody]int[] deletedMarcas)
        {
            try
            {
                foreach (int deletedMarcaId in deletedMarcas)
                {
                    Marca deletedMarca = _context.Marca.Find(deletedMarcaId);
                    deletedMarca.fb_Estatus = false;
                    _context.Marca.Update(deletedMarca);
                }

                int result = _context.SaveChanges();
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed deleting Marcas from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }
        #endregion

        #region Producto
        [HttpGet]
        public IActionResult GetProductos()
        {
            try
            {
                var productos = _context.Producto
                    .Include(m => m.Categoria)
                    .Include(m => m.Marca)
                    .Include(m => m.UnidadMedida)
                    .Where(m => m.fb_Estatus);
                var result = Mapper.Map<IEnumerable<ProductoViewModel>>(productos);
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Productos from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet("{productoId}")]
        public IActionResult GetProducto(int productoId)
        {
            try
            {
                var producto = _context.Producto
                    .Include(m => m.Categoria)
                    .Include(m => m.Marca)
                    .Include(m => m.UnidadMedida)
                    .Where(m=>m.fi_IdCatProducto == productoId).FirstOrDefault();
                var result = Mapper.Map<ProductoViewModel>(producto);
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Producto from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet("{productoNombre}")]
        public IActionResult GetProductoExist(string productoNombre)
        {
            try
            {
                var producto = _context.Producto
                    .Include(m => m.Categoria)
                    .Include(m => m.Marca)
                    .Include(m => m.UnidadMedida)
                    .Where(m => m.fc_NombreProducto.ToUpper() == productoNombre.ToUpper() && !m.fb_Estatus);
                var result = Mapper.Map<IEnumerable<ProductoViewModel>>(producto);
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting Producto from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPost]
        public IActionResult SaveProducto([FromBody]ProductoViewModel newProducto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (newProducto == null)
                        return BadRequest($"{nameof(newProducto)} cannot be null");

                    Producto producto = Mapper.Map<Producto>(newProducto);
                    Categoria categoria = _context.Categoria.Find(producto.Categoria.fi_IdCatCategoria);
                    Marca marca = _context.Marca.Find(producto.Marca.fi_IdCatMarca);
                    UnidadMedida unidadMedida = _context.UnidadMedida.Find(producto.UnidadMedida.fi_IdCatUnidadMedida);

                    producto.Categoria = categoria;
                    producto.Marca = marca;
                    producto.UnidadMedida = unidadMedida;

                    if (newProducto.fi_IdCatProducto == 0)
                        _context.Producto.Add(producto);
                    else
                        _context.Producto.Update(producto);

                    var result = _context.SaveChanges();
                    return Ok(result);
                }
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed saving Producto: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{productoId}")]
        public IActionResult DeleteProducto(int productoId)
        {
            try
            {
                Producto deletedProducto = _context.Producto.Find(productoId);
                deletedProducto.fb_Estatus = false;
                _context.Producto.Update(deletedProducto);
                int result = _context.SaveChanges();
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed deleting Producto from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPut]
        public IActionResult BulkDeleteProducto([FromBody]int[] deletedProductos)
        {
            try
            {
                foreach (int deletedProductoId in deletedProductos)
                {
                    Producto deletedProducto = _context.Producto.Find(deletedProductoId);
                    deletedProducto.fb_Estatus = false;
                    _context.Producto.Update(deletedProducto);
                }

                int result = _context.SaveChanges();
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed deleting Productos from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }
        #endregion

        #region UnidadMedida       
        [HttpGet]
        public IActionResult GetUnidadesMedida()
        {
            try
            {
                var unidadesMedia = _context.UnidadMedida.Where(m => m.fb_Estatus);
                return Ok(unidadesMedia);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for UnidadesMedida from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet("{unidadMedidaId}")]
        public IActionResult GetUnidadMedida(int unidadMedidaId)
        {
            try
            {
                var unidadMedida = _context.UnidadMedida.Find(unidadMedidaId);
                return Ok(unidadMedida);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for UnidadMedida from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet("{unidadMedidaNombre}")]
        public IActionResult GetUnidadMedidaExist(string unidadMedidaNombre)
        {
            try
            {
                var unidadMedida = _context.UnidadMedida.Where(m => m.fc_NombreUnidadMedida.ToUpper() == unidadMedidaNombre.ToUpper() && !m.fb_Estatus);
                return Ok(unidadMedida);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting UnidadMedida from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPost]
        public IActionResult SaveUnidadMedida([FromBody]UnidadMedida newUnidadMedida)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (newUnidadMedida == null)
                        return BadRequest($"{nameof(newUnidadMedida)} cannot be null");

                    if (newUnidadMedida.fi_IdCatUnidadMedida == 0)
                        _context.UnidadMedida.Add(newUnidadMedida);
                    else
                        _context.UnidadMedida.Update(newUnidadMedida);

                    var result = _context.SaveChanges();
                    return Ok(result);
                }
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed saving UnidadMedida: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{unidadMedidaId}")]
        public IActionResult DeleteUnidadMedida(int unidadMedidaId)
        {
            try
            {
                UnidadMedida deletedUnidadMedida = _context.UnidadMedida.Find(unidadMedidaId);
                deletedUnidadMedida.fb_Estatus = false;
                _context.UnidadMedida.Update(deletedUnidadMedida);
                int result = _context.SaveChanges();
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed deleting UnidadMedida from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPut]
        public IActionResult BulkDeleteUnidadMedida([FromBody]int[] deletedUnidadMedidas)
        {
            try
            {
                foreach (int deletedUnidadMedidaId in deletedUnidadMedidas)
                {
                    UnidadMedida deletedUnidadMedida = _context.UnidadMedida.Find(deletedUnidadMedidaId);
                    deletedUnidadMedida.fb_Estatus = false;
                    _context.UnidadMedida.Update(deletedUnidadMedida);
                }

                int result = _context.SaveChanges();
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed deleting UnidadMedidas from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }
        #endregion
    }
}