﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SystVenta.WebApi.Models;
using SystVenta.WebApi.Models.Almacen;
using SystVenta.WebApi.Models.Ventas;
using SystVenta.WebApi.ViewModels;

namespace SystVenta.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class VentasController : ControllerBase
    {
        private readonly SystVentaDbContext _context;

        public VentasController(SystVentaDbContext context)
        {
            _context = context;
        }

        #region Cliente       
        [HttpGet]
        public IActionResult GetClientes()
        {
            try
            {
                var clientes = _context.Cliente.Where(m => m.fb_Estatus);
                return Ok(clientes);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Clientes from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet("{clienteId}")]
        public IActionResult GetCliente(int clienteId)
        {
            try
            {
                var cliente = _context.Cliente.Find(clienteId);
                return Ok(cliente);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Cliente from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet("{clienteNombre}")]
        public IActionResult GetClienteExist(string clienteNombre)
        {
            try
            {
                var Cliente = _context.Cliente.Where(m => m.fc_NombreCliente.ToUpper() == clienteNombre.ToUpper() && !m.fb_Estatus);
                return Ok(Cliente);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting Cliente from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPost]
        public IActionResult SaveCliente([FromBody]Cliente newCliente)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (newCliente == null)
                        return BadRequest($"{nameof(newCliente)} cannot be null");

                    if (newCliente.fi_IdCatCliente == 0)
                        _context.Cliente.Add(newCliente);
                    else
                        _context.Cliente.Update(newCliente);

                    var result = _context.SaveChanges();
                    return Ok(result);
                }
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed saving Cliente: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{clienteId}")]
        public IActionResult DeleteCliente(int clienteId)
        {
            try
            {
                Cliente deletedCliente = _context.Cliente.Find(clienteId);
                deletedCliente.fb_Estatus = false;
                _context.Cliente.Update(deletedCliente);
                int result = _context.SaveChanges();
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed deleting Cliente from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPut]
        public IActionResult BulkDeleteCliente([FromBody]int[] deletedClientes)
        {
            try
            {
                foreach (int deletedClienteId in deletedClientes)
                {
                    Cliente deletedCliente = _context.Cliente.Find(deletedClienteId);
                    deletedCliente.fb_Estatus = false;
                    _context.Cliente.Update(deletedCliente);
                }

                int result = _context.SaveChanges();
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed deleting Clientes from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }
        #endregion

        #region Ventas
        [HttpGet]
        public IActionResult GetVentas()
        {
            try
            {
                var ventas = _context.Venta
                    .Include(m => m.Cliente)
                    .Include(m => m.MetodoPago)
                    .Where(m => m.fb_Estatus);

                var result = Mapper.Map<IEnumerable<VentasViewModel>>(ventas);
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Ventas from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet("{ventaId}")]
        public IActionResult GetVentaDetail(int ventaId)
        {
            try
            {
                var Venta = _context.Venta
                    .Include(m => m.Comprobante)
                    .Include(m => m.MetodoPago)
                    .Include(m => m.Cliente)
                    .Where(m => m.fi_IdCnfVenta == ventaId).FirstOrDefault();

                VentaViewModel VentaView = Mapper.Map<VentaViewModel>(Venta);

                var detalleVenta = _context.VentaDetalle
                    .Include(m => m.Producto)
                    .Where(m => m.Venta.fi_IdCnfVenta == ventaId);

                VentaView.VentaDetalles = Mapper.Map<IEnumerable<VentaDetalleViewModel>>(detalleVenta).ToList();

                return Ok(VentaView);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Ventas from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPost]
        public IActionResult SaveVenta([FromBody]VentaViewModel newVenta)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (newVenta == null)
                        return BadRequest($"{nameof(newVenta)} cannot be null");

                    Venta Venta = Mapper.Map<Venta>(newVenta);
                    Venta.MetodoPago = _context.MetodoPago.Find(Venta.MetodoPago.fi_IdCatMetodoPago);
                    Venta.Comprobante = _context.Comprobante.Find(Venta.Comprobante.fi_IdCatComprobante);
                    Venta.Cliente = _context.Cliente.Find(Venta.Cliente.fi_IdCatCliente);

                    List<VentaDetalle> VentaDetalles = new List<VentaDetalle>();

                    foreach (VentaDetalleViewModel VentaDetalle in newVenta.VentaDetalles)
                    {
                        VentaDetalle VentaDetalleAux = new VentaDetalle();
                        VentaDetalleAux = Mapper.Map<VentaDetalle>(VentaDetalle);
                        Producto producto = _context.Producto.Where(m => m.fc_Codigo.ToUpper() == VentaDetalleAux.Producto.fc_Codigo.ToUpper()).FirstOrDefault();
                        producto.fi_Stock -= VentaDetalleAux.fi_Cantidad;
                        VentaDetalleAux.Producto = producto;
                        VentaDetalleAux.Venta = Venta;

                        VentaDetalles.Add(VentaDetalleAux);
                    }

                    if (newVenta.fi_IdCnfVenta == 0)
                        _context.VentaDetalle.AddRange(VentaDetalles);
                    else
                    {
                        List<VentaDetalle> VentasDetalleToRemove = _context.VentaDetalle
                            .Include(m => m.Venta)
                            .Include(m => m.Producto)
                            .Where(m => m.Venta.fi_IdCnfVenta == Venta.fi_IdCnfVenta).ToList();
                        foreach (VentaDetalle VentaDetalle in VentasDetalleToRemove)
                        {
                            Producto producto = _context.Producto.Where(m => m.fi_IdCatProducto == VentaDetalle.Producto.fi_IdCatProducto).FirstOrDefault();
                            producto.fi_Stock += VentaDetalle.fi_Cantidad;
                        }

                        _context.VentaDetalle.RemoveRange(VentasDetalleToRemove);
                        _context.VentaDetalle.AddRange(VentaDetalles);
                    }

                    var result = _context.SaveChanges();
                    return Ok(result);
                }
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed saving Proveedor: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{ventaId}")]
        public IActionResult DeleteVenta(int ventaId)
        {
            try
            {
                Venta deletedVenta = _context.Venta.Find(ventaId);
                deletedVenta.fb_Estatus = false;
                _context.Venta.Update(deletedVenta);

                List<VentaDetalle> VentasDetalleToRemove = _context.VentaDetalle
                            .Include(m => m.Venta)
                            .Include(m => m.Producto)
                            .Where(m => m.Venta.fi_IdCnfVenta == ventaId).ToList();

                foreach (VentaDetalle VentaDetalle in VentasDetalleToRemove)
                {
                    VentaDetalle.fb_Estatus = false;

                    Producto producto = _context.Producto.Where(m => m.fi_IdCatProducto == VentaDetalle.Producto.fi_IdCatProducto).FirstOrDefault();
                    producto.fi_Stock += VentaDetalle.fi_Cantidad;
                }

                _context.VentaDetalle.UpdateRange(VentasDetalleToRemove);

                int result = _context.SaveChanges();
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed deleting Venta from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }
        #endregion
    }
}