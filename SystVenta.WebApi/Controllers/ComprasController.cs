﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SystVenta.WebApi.Models;
using SystVenta.WebApi.Models.Almacen;
using SystVenta.WebApi.Models.Compras;
using SystVenta.WebApi.ViewModels;

namespace SystVenta.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ComprasController : ControllerBase
    {
        private readonly SystVentaDbContext _context;

        public ComprasController(SystVentaDbContext context)
        {
            _context = context;
        }

        #region Proveedor       
        [HttpGet]
        public IActionResult GetProveedores()
        {
            try
            {
                var proveedores = _context.Proveedor.Where(m => m.fb_Estatus);
                return Ok(proveedores);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Proveedores from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet("{proveedorId}")]
        public IActionResult GetProveedor(int proveedorId)
        {
            try
            {
                var Proveedor = _context.Proveedor.Find(proveedorId);
                return Ok(Proveedor);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Proveedor from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet("{proveedorNombre}")]
        public IActionResult GetProveedorExist(string proveedorNombre)
        {
            try
            {
                var proveedor = _context.Proveedor.Where(m => m.fc_NombreProveedor.ToUpper() == proveedorNombre.ToUpper() && !m.fb_Estatus);
                return Ok(proveedor);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting Proveedor from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPost]
        public IActionResult SaveProveedor([FromBody]Proveedor newProveedor)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (newProveedor == null)
                        return BadRequest($"{nameof(newProveedor)} cannot be null");

                    if (newProveedor.fi_IdCatProveedor == 0)
                        _context.Proveedor.Add(newProveedor);
                    else
                        _context.Proveedor.Update(newProveedor);

                    var result = _context.SaveChanges();
                    return Ok(result);
                }
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed saving Proveedor: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{proveedorId}")]
        public IActionResult DeleteProveedor(int proveedorId)
        {
            try
            {
                Proveedor deletedProveedor = _context.Proveedor.Find(proveedorId);
                deletedProveedor.fb_Estatus = false;
                _context.Proveedor.Update(deletedProveedor);
                int result = _context.SaveChanges();
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed deleting Proveedor from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPut]
        public IActionResult BulkDeleteProveedor([FromBody]int[] deletedProveedors)
        {
            try
            {
                foreach (int deletedProveedorId in deletedProveedors)
                {
                    Proveedor deletedProveedor = _context.Proveedor.Find(deletedProveedorId);
                    deletedProveedor.fb_Estatus = false;
                    _context.Proveedor.Update(deletedProveedor);
                }

                int result = _context.SaveChanges();
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed deleting Proveedors from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }
        #endregion

        #region Compras
        [HttpGet]
        public IActionResult GetCompras()
        {
            try
            {
                var compras = _context.Compra
                    .Include(m => m.Proveedor)
                    .Where(m => m.fb_Estatus);

                var result = Mapper.Map<IEnumerable<ComprasViewModel>>(compras);
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Compras from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet("{compraId}")]
        public IActionResult GetCompraDetail(int compraId)
        {
            try
            {
                var compra = _context.Compra
                    .Include(m => m.Comprobante)
                    .Include(m => m.MetodoPago)
                    .Include(m => m.Proveedor)
                    .Where(m => m.fi_IdCnfCompra == compraId).FirstOrDefault();

                CompraViewModel compraView = Mapper.Map<CompraViewModel>(compra);

                var detalleCompra = _context.CompraDetalle
                    .Include(m => m.Producto)
                    .Where(m => m.Compra.fi_IdCnfCompra == compraId);

                compraView.CompraDetalles = Mapper.Map<IEnumerable<CompraDetalleViewModel>>(detalleCompra).ToList();

                return Ok(compraView);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Proveedor from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPost]
        public IActionResult SaveCompra([FromBody]CompraViewModel newCompra)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (newCompra == null)
                        return BadRequest($"{nameof(newCompra)} cannot be null");

                    Compra compra = Mapper.Map<Compra>(newCompra);
                    compra.MetodoPago = _context.MetodoPago.Find(compra.MetodoPago.fi_IdCatMetodoPago);
                    compra.Comprobante = _context.Comprobante.Find(compra.Comprobante.fi_IdCatComprobante);
                    compra.Proveedor = _context.Proveedor.Find(compra.Proveedor.fi_IdCatProveedor);

                    List<CompraDetalle> compraDetalles = new List<CompraDetalle>();

                    foreach(CompraDetalleViewModel compraDetalle in newCompra.CompraDetalles)
                    {
                        CompraDetalle compraDetalleAux = new CompraDetalle();
                        compraDetalleAux = Mapper.Map<CompraDetalle>(compraDetalle);
                        Producto producto = _context.Producto.Where(m => m.fc_Codigo.ToUpper() == compraDetalleAux.Producto.fc_Codigo.ToUpper()).FirstOrDefault();
                        producto.fi_Stock += compraDetalleAux.fi_Cantidad;
                        compraDetalleAux.Producto = producto;
                        compraDetalleAux.Compra = compra;

                        compraDetalles.Add(compraDetalleAux);
                    }

                    if (newCompra.fi_IdCnfCompra == 0)
                        _context.CompraDetalle.AddRange(compraDetalles);
                    else
                    {
                        List<CompraDetalle> comprasDetalleToRemove = _context.CompraDetalle
                            .Include(m => m.Compra)
                            .Include(m => m.Producto)
                            .Where(m => m.Compra.fi_IdCnfCompra == compra.fi_IdCnfCompra).ToList();
                        foreach(CompraDetalle compraDetalle in comprasDetalleToRemove)
                        {
                            Producto producto = _context.Producto.Where(m => m.fi_IdCatProducto == compraDetalle.Producto.fi_IdCatProducto).FirstOrDefault();
                            producto.fi_Stock -= compraDetalle.fi_Cantidad;
                        }

                        _context.CompraDetalle.RemoveRange(comprasDetalleToRemove);
                        _context.CompraDetalle.AddRange(compraDetalles);
                    }

                    var result = _context.SaveChanges();
                    return Ok(result);
                }
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed saving Proveedor: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{compraId}")]
        public IActionResult DeleteCompra(int compraId)
        {
            try
            {
                Compra deletedCompra = _context.Compra.Find(compraId);
                deletedCompra.fb_Estatus = false;
                _context.Compra.Update(deletedCompra);

                List<CompraDetalle> comprasDetalleToRemove = _context.CompraDetalle
                            .Include(m => m.Compra)
                            .Include(m => m.Producto)
                            .Where(m => m.Compra.fi_IdCnfCompra == compraId).ToList();

                foreach (CompraDetalle compraDetalle in comprasDetalleToRemove)
                {
                    compraDetalle.fb_Estatus = false;

                    Producto producto = _context.Producto.Where(m => m.fi_IdCatProducto == compraDetalle.Producto.fi_IdCatProducto).FirstOrDefault();
                    producto.fi_Stock -= compraDetalle.fi_Cantidad;
                }

                _context.CompraDetalle.UpdateRange(comprasDetalleToRemove);

                int result = _context.SaveChanges();
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed deleting Proveedor from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }
        #endregion
    }
}