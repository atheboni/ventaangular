﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystVenta.WebApi.ViewModels
{
    public class ReporteFiltrosViewModel
    {
        public DateTime fd_FechaDesde { get; set; }
        public DateTime fd_FechaHasta { get; set; }
    }
}
