﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystVenta.WebApi.ViewModels
{
    public class VentaViewModel
    {
        public int fi_IdCnfVenta { get; set; }
        public string fc_NumeroVenta { get; set; }
        public decimal? fn_DescuentoPorcentaje { get; set; }
        public decimal? fn_DescuentoCantidad { get; set; }
        public DateTime fd_FechaVenta { get; set; }
        public int MetodoPago { get; set; }
        public int Comprobante { get; set; }
        public int Cliente { get; set; }
        public decimal fn_Total { get; set; }
        public bool fb_Estatus { get; set; }
        public List<VentaDetalleViewModel> VentaDetalles { get; set; }
    }
}
