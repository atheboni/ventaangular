﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystVenta.WebApi.ViewModels
{
    public class ReporteComprasViewModel
    {
        public int Mes { get; set; }
        public decimal fn_Total { get; set; }
        public int Cantidad { get; set; }
        public decimal Porcentaje { get; set; }
        public int Anio { get; set; }
    }
}
