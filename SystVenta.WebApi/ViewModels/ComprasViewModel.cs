﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystVenta.WebApi.ViewModels
{
    public class ComprasViewModel
    {
        public int fi_IdCnfCompra { get; set; }
        public string fc_NumeroCompra { get; set; }
        public DateTime fd_FechaCompra { get; set; }        
        public string Proveedor { get; set; }
        public decimal fn_Total { get; set; }
    }
}
