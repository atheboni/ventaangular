﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystVenta.WebApi.ViewModels
{
    public class GeneralViewModel
    {
        public int fi_IdCnfGeneral { get; set; }
        public string fc_NombreEmpresa { get; set; }
        public byte[] fc_Logo { get; set; }
        public string imgBase64 { get; set; }
    }
}
