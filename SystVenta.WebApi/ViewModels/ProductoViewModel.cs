﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystVenta.WebApi.ViewModels
{
    public class ProductoViewModel
    {
        public int fi_IdCatProducto { get; set; }
        public string fc_Codigo { get; set; }
        public string fc_NombreProducto { get; set; }
        public decimal fn_PrecioCompra { get; set; }
        public decimal fn_PrecioVenta { get; set; }
        public int fi_Stock { get; set; }
        public int fi_StockMinimo { get; set; }
        public int Categoria { get; set; }
        public string NombreCategoria { get; set; }
        public int Marca { get; set; }
        public string NombreMarca { get; set; }
        public int UnidadMedida { get; set; }
        public string NombreUnidadMedida { get; set; }
        public bool fb_Estatus { get; set; }
    }
}
