﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystVenta.WebApi.ViewModels
{
    public class ReporteMasVendidosViewModel
    {
        public int fi_IdCatProducto { get; set; }
        public string fc_NombreProducto { get; set; }
        public List<ReporteVentasViewModel> Ventas { get; set; }
    }
}
