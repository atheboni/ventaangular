﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SystVenta.WebApi.Models.Almacen;
using SystVenta.WebApi.Models.Compras;
using SystVenta.WebApi.Models.Configuracion;
using SystVenta.WebApi.Models.Ventas;

namespace SystVenta.WebApi.ViewModels
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Usuario, UsuarioViewModel>()
                   .ForMember(d => d.fc_ConfirmPassword, map => map.Ignore())
                   .ReverseMap();

            CreateMap<General, GeneralViewModel>()
                   .ForMember(d => d.imgBase64, map => map.MapFrom(s => Convert.ToBase64String(s.fc_Logo)))
                   .ReverseMap();

            CreateMap<Producto, ProductoViewModel>()
                   .ForMember(d => d.Categoria, map => map.MapFrom(s => s.Categoria.fi_IdCatCategoria))
                   .ForMember(d => d.NombreCategoria, map => map.MapFrom(s => s.Categoria.fc_NombreCategoria))
                   .ForMember(d => d.Marca, map => map.MapFrom(s => s.Marca.fi_IdCatMarca))
                   .ForMember(d => d.NombreMarca, map => map.MapFrom(s => s.Marca.fc_NombreMarca))
                   .ForMember(d => d.UnidadMedida, map => map.MapFrom(s => s.UnidadMedida.fi_IdCatUnidadMedida))
                   .ForMember(d => d.NombreUnidadMedida, map => map.MapFrom(s => s.UnidadMedida.fc_NombreUnidadMedida));

            CreateMap<ProductoViewModel, Producto>()
                   .ForPath(d => d.Categoria.fi_IdCatCategoria, map => map.MapFrom(s => s.Categoria))
                   .ForPath(d => d.Marca.fi_IdCatMarca, map => map.MapFrom(s => s.Marca))
                   .ForPath(d => d.UnidadMedida.fi_IdCatUnidadMedida, map => map.MapFrom(s => s.UnidadMedida));

            CreateMap<Compra, ComprasViewModel>()
                   .ForMember(d => d.Proveedor, map => map.MapFrom(s => s.Proveedor.fc_NombreProveedor));

            CreateMap<Compra, CompraViewModel>()
                   .ForMember(d => d.MetodoPago, map => map.MapFrom(s => s.MetodoPago.fi_IdCatMetodoPago))
                   .ForMember(d => d.Comprobante, map => map.MapFrom(s => s.Comprobante.fi_IdCatComprobante))
                   .ForMember(d => d.Proveedor, map => map.MapFrom(s => s.Proveedor.fi_IdCatProveedor))
                   .ForMember(d => d.CompraDetalles, map => map.Ignore());

            CreateMap<CompraViewModel, Compra>()
                   .ForPath(d => d.MetodoPago.fi_IdCatMetodoPago, map => map.MapFrom(s => s.MetodoPago))
                   .ForPath(d => d.Comprobante.fi_IdCatComprobante, map => map.MapFrom(s => s.Comprobante))
                   .ForPath(d => d.Proveedor.fi_IdCatProveedor, map => map.MapFrom(s => s.Proveedor));

            CreateMap<CompraDetalle, CompraDetalleViewModel>()
                   .ForMember(d => d.fc_Codigo, map => map.MapFrom(s => s.Producto.fc_Codigo))
                   .ForMember(d => d.fc_NombreProducto, map => map.MapFrom(s => s.Producto.fc_NombreProducto));

            CreateMap<CompraDetalleViewModel, CompraDetalle>()
                   .ForPath(d => d.Producto.fc_Codigo, map => map.MapFrom(s => s.fc_Codigo));

            #region Ventas
            CreateMap<Venta, VentasViewModel>()
                   .ForMember(d => d.Cliente, map => map.MapFrom(s => s.Cliente.fc_NombreCliente))
                   .ForMember(d => d.MetodoPago, map => map.MapFrom(s => s.MetodoPago.fc_NombreMetodoPago));

            CreateMap<Venta, VentaViewModel>()
                   .ForMember(d => d.MetodoPago, map => map.MapFrom(s => s.MetodoPago.fi_IdCatMetodoPago))
                   .ForMember(d => d.Comprobante, map => map.MapFrom(s => s.Comprobante.fi_IdCatComprobante))
                   .ForMember(d => d.Cliente, map => map.MapFrom(s => s.Cliente.fi_IdCatCliente))
                   .ForMember(d => d.VentaDetalles, map => map.Ignore());

            CreateMap<VentaViewModel, Venta>()
                   .ForPath(d => d.MetodoPago.fi_IdCatMetodoPago, map => map.MapFrom(s => s.MetodoPago))
                   .ForPath(d => d.Comprobante.fi_IdCatComprobante, map => map.MapFrom(s => s.Comprobante))
                   .ForPath(d => d.Cliente.fi_IdCatCliente, map => map.MapFrom(s => s.Cliente));

            CreateMap<VentaDetalle, VentaDetalleViewModel>()
                   .ForMember(d => d.fc_Codigo, map => map.MapFrom(s => s.Producto.fc_Codigo))
                   .ForMember(d => d.fc_NombreProducto, map => map.MapFrom(s => s.Producto.fc_NombreProducto));

            CreateMap<VentaDetalleViewModel, VentaDetalle>()
                   .ForPath(d => d.Producto.fc_Codigo, map => map.MapFrom(s => s.fc_Codigo));
            #endregion
        }
    }
}
