﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystVenta.WebApi.ViewModels
{
    public class VentaDetalleViewModel
    {
        public int fi_IdCnfVentaDetalle { get; set; }
        public string fc_Codigo { get; set; }
        public string fc_NombreProducto { get; set; }
        public int fi_Cantidad { get; set; }
        public decimal fn_PrecioVenta { get; set; }
        public decimal fn_Total { get; set; }
        public bool fb_Estatus { get; set; }
    }
}
