﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystVenta.WebApi.ViewModels
{
    public class LoginViewModel
    {
        public string userName { get; set; }
        public string password { get; set; }
    }
}
