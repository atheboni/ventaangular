﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystVenta.WebApi.ViewModels
{
    public class VentasViewModel
    {
        public int fi_IdCnfVenta { get; set; }
        public string fc_NumeroVenta { get; set; }
        public DateTime fd_FechaVenta { get; set; }        
        public string Cliente { get; set; }
        public string MetodoPago { get; set; }
        public decimal fn_Total { get; set; }
    }
}
