﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystVenta.WebApi.ViewModels
{
    public class CompraViewModel
    {
        public int fi_IdCnfCompra { get; set; }
        public string fc_NumeroCompra { get; set; }
        public decimal? fn_DescuentoPorcentaje { get; set; }
        public decimal? fn_DescuentoCantidad { get; set; }
        public DateTime fd_FechaCompra { get; set; }
        public int MetodoPago { get; set; }
        public int Comprobante { get; set; }
        public int Proveedor { get; set; }
        public decimal fn_Total { get; set; }
        public bool fb_Estatus { get; set; }
        public List<CompraDetalleViewModel> CompraDetalles { get; set; }
    }
}
