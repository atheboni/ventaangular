﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystVenta.WebApi.Models.Configuracion
{
    public class MetodoPago
    {
        public int fi_IdCatMetodoPago { get; set; }        
        public string fc_NombreMetodoPago { get; set; }
        public bool fb_Estatus { get; set; }
    }
}
