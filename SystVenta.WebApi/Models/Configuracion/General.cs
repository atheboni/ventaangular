﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystVenta.WebApi.Models.Configuracion
{
    public class General
    {
        public int fi_IdCnfGeneral { get; set; }
        public string fc_NombreEmpresa { get; set; }
        public byte[] fc_Logo { get; set; }
    }
}
