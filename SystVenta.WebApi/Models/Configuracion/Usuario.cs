﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystVenta.WebApi.Models.Configuracion
{
    public class Usuario
    {
        public int fi_IdCatUsuario { get; set; }        
        public string fc_NombreUsuario { get; set; }
        public string fc_Usuario { get; set; }
        public int fi_NivelAcceso { get; set; }        
        public string fc_Direccion { get; set; }        
        public string fc_Telefono { get; set; }
        public string fc_Password { get; set; }
        public bool fb_Estatus { get; set; }
    }
}
