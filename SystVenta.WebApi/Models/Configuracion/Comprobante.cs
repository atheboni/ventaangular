﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystVenta.WebApi.Models.Configuracion
{
    public class Comprobante
    {
        public int fi_IdCatComprobante { get; set; }        
        public string fc_NombreComprobante { get; set; }
        public bool fb_Estatus { get; set; }
    }
}
