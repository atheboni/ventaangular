﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SystVenta.WebApi.Models.Configuracion;

namespace SystVenta.WebApi.Models.Compras
{
    public class Compra
    {
        public int fi_IdCnfCompra { get; set; }
        public string fc_NumeroCompra { get; set; }
        public decimal? fn_DescuentoPorcentaje { get; set; }
        public decimal? fn_DescuentoCantidad { get; set; }
        public DateTime fd_FechaCompra { get; set; }
        public MetodoPago MetodoPago { get; set; }        
        public Comprobante Comprobante { get; set; }        
        public Proveedor Proveedor { get; set; }        
        public decimal fn_Total { get; set; }
        public bool fb_Estatus { get; set; }
    }
}
