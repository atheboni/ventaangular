﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystVenta.WebApi.Models.Compras
{
    public class Proveedor
    {
        public int fi_IdCatProveedor { get; set; }
        public string fc_RazonSocial { get; set; }        
        public string fc_NombreProveedor { get; set; }        
        public string fc_RFC { get; set; }
        public string fc_Email { get; set; }
        public string fc_Direccion { get; set; }
        public string fc_Telefono { get; set; }
        public bool fb_Estatus { get; set; }
    }
}
