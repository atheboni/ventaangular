﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SystVenta.WebApi.Models.Almacen;

namespace SystVenta.WebApi.Models.Compras
{
    public class CompraDetalle
    {
        public int fi_IdCnfCompraDetalle { get; set; }
        public Compra Compra { get; set; }
        public Producto Producto { get; set; }
        public int fi_Cantidad { get; set; }
        public decimal fn_PrecioCompra { get; set; }
        public decimal fn_Total { get; set; }
        public bool fb_Estatus { get; set; }        
    }
}
