﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SystVenta.WebApi.Models.Almacen;

namespace SystVenta.WebApi.Models.Ventas
{
    public class VentaDetalle
    {
        public int fi_IdCnfVentaDetalle { get; set; }
        public Venta Venta { get; set; }
        public Producto Producto { get; set; }
        public int fi_Cantidad { get; set; }
        public decimal fn_PrecioVenta { get; set; }
        public decimal fn_Total { get; set; }
        public bool fb_Estatus { get; set; }
    }
}
