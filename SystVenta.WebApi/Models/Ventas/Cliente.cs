﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystVenta.WebApi.Models.Ventas
{
    public class Cliente
    {
        public int fi_IdCatCliente { get; set; }        
        public string fc_NombreCliente { get; set; }        
        public string fc_RFC { get; set; }
        public string fc_Email { get; set; }
        public string fc_Direccion { get; set; }
        public string fc_Telefono { get; set; }
        public bool fb_Estatus { get; set; }
    }
}
