﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SystVenta.WebApi.Models.Configuracion;

namespace SystVenta.WebApi.Models.Ventas
{
    public class Venta
    {
        public int fi_IdCnfVenta { get; set; }
        public string fc_NumeroVenta { get; set; }
        public decimal? fn_DescuentoPorcentaje { get; set; }
        public decimal? fn_DescuentoCantidad { get; set; }
        public DateTime fd_FechaVenta { get; set; }
        public MetodoPago MetodoPago { get; set; }
        public Comprobante Comprobante { get; set; }
        public Cliente Cliente { get; set; }
        public decimal fn_Total { get; set; }
        public bool fb_Estatus { get; set; }
    }
}
