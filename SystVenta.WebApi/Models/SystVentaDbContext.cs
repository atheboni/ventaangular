﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SystVenta.WebApi.Models.Almacen;
using SystVenta.WebApi.Models.Compras;
using SystVenta.WebApi.Models.Configuracion;
using SystVenta.WebApi.Models.Ventas;

namespace SystVenta.WebApi.Models
{
    public class SystVentaDbContext : DbContext
    {
        public SystVentaDbContext(DbContextOptions<SystVentaDbContext> options)
            : base(options)
        {
        }

        #region Almacen        
        public DbSet<Categoria> Categoria { get; set; }
        public DbSet<Marca> Marca { get; set; }
        public DbSet<Producto> Producto { get; set; }
        public DbSet<UnidadMedida> UnidadMedida { get; set; }
        #endregion

        #region Compras
        public DbSet<Proveedor> Proveedor { get; set; }
        public DbSet<Compra> Compra { get; set; }
        public DbSet<CompraDetalle> CompraDetalle { get; set; }
        #endregion

        #region Configuracion
        public DbSet<General> General { get; set; }
        public DbSet<Comprobante> Comprobante { get; set; }
        public DbSet<MetodoPago> MetodoPago { get; set; }       
        public DbSet<Usuario> Usuario { get; set; }
        #endregion

        #region Ventas
        public DbSet<Cliente> Cliente { get; set; }
        public DbSet<Venta> Venta { get; set; }
        public DbSet<VentaDetalle> VentaDetalle { get; set; }
        #endregion

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Server=(local);Database=SystVenta;User Id=sa;Password=dbmaster;MultipleActiveResultSets=true");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Almacen            
            modelBuilder.Entity<Categoria>(builder =>
            {
                builder.ToTable($"tbl_CatCategorias", $"Almacen");

                builder.HasKey(c => c.fi_IdCatCategoria);

                builder.HasIndex(c => c.fi_IdCatCategoria)
                    .IsUnique();

                builder.Property(c => c.fc_NombreCategoria)
                .HasColumnType("varchar")
                .IsRequired()
                .HasMaxLength(250);

                builder.Property(c => c.fb_Estatus)
                .IsRequired();
            });

            modelBuilder.Entity<Marca>(builder =>
            {
                builder.ToTable($"tbl_CatMarcas", $"Almacen");

                builder.HasKey(c => c.fi_IdCatMarca);

                builder.HasIndex(c => c.fi_IdCatMarca)
                    .IsUnique();

                builder.Property(c => c.fc_NombreMarca)
                .HasColumnType("varchar")
                .IsRequired()
                .HasMaxLength(250);

                builder.Property(c => c.fb_Estatus)
                .IsRequired();
            });

            modelBuilder.Entity<Producto>(builder =>
            {
                builder.ToTable($"tbl_CatProductos", $"Almacen");

                builder.HasKey(c => c.fi_IdCatProducto);

                builder.HasIndex(c => c.fi_IdCatProducto)
                    .IsUnique();

                builder.Property(c => c.fc_Codigo)
                .HasColumnType("varchar")
                .IsRequired()
                .HasMaxLength(250);

                builder.Property(c => c.fc_NombreProducto)
                .HasColumnType("varchar")
                .IsRequired()
                .HasMaxLength(250);

                builder.Property(c => c.fn_PrecioCompra)
                .HasColumnType("decimal(10,2)")
                .IsRequired()
                .HasMaxLength(250);

                builder.Property(c => c.fn_PrecioVenta)
                .HasColumnType("decimal(10,2)")
                .IsRequired();

                builder.Property(c => c.fi_Stock)
                .HasColumnType("int")
                .IsRequired();

                builder.Property(c => c.fi_StockMinimo)
                .HasColumnType("int")
                .IsRequired();

                builder.HasOne<Categoria>(c => c.Categoria)
                .WithMany()
                .HasForeignKey($"fi_IdCatCategoria");

                builder.HasOne<Marca>(c => c.Marca)
                .WithMany()
                .HasForeignKey($"fi_IdCatMarca");

                builder.HasOne<UnidadMedida>(c => c.UnidadMedida)
                .WithMany()
                .HasForeignKey($"fi_IdCatUnidadMedida");

                builder.Property(c => c.fb_Estatus)
                .IsRequired();
            });

            modelBuilder.Entity<UnidadMedida>(builder =>
            {
                builder.ToTable($"tbl_CatUnidadesMedida", $"Almacen");

                builder.HasKey(c => c.fi_IdCatUnidadMedida);

                builder.HasIndex(c => c.fi_IdCatUnidadMedida)
                    .IsUnique();

                builder.Property(c => c.fc_NombreUnidadMedida)
                .HasColumnType("varchar")
                .IsRequired()
                .HasMaxLength(250);

                builder.Property(c => c.fb_Estatus)
                .IsRequired();
            });
            #endregion

            #region Compras
            modelBuilder.Entity<Proveedor>(builder =>
            {
                builder.ToTable($"tbl_CatProveedores", $"Compras");

                builder.HasKey(c => c.fi_IdCatProveedor);

                builder.HasIndex(c => c.fi_IdCatProveedor)
                    .IsUnique();

                builder.Property(c => c.fc_RazonSocial)
                .HasColumnType("varchar")
                .HasMaxLength(250);

                builder.Property(c => c.fc_NombreProveedor)
                .HasColumnType("varchar")
                .IsRequired()
                .HasMaxLength(250);

                builder.Property(c => c.fc_RFC)
                .HasColumnType("varchar")
                .HasMaxLength(250);

                builder.Property(c => c.fc_Email)
                .HasColumnType("varchar")
                .HasMaxLength(120);

                builder.Property(c => c.fc_Direccion)
                .HasColumnType("varchar")
                .IsRequired()
                .HasMaxLength(250);

                builder.Property(c => c.fc_Telefono)
                .HasColumnType("varchar")
                .HasMaxLength(10);
               
                builder.Property(c => c.fb_Estatus)
                .IsRequired();
            });

            modelBuilder.Entity<Compra>(builder =>
            {
                builder.ToTable($"tbl_CnfCompras", $"Compras");

                builder.HasKey(c => c.fi_IdCnfCompra);

                builder.HasIndex(c => c.fi_IdCnfCompra)
                    .IsUnique();

                builder.Property(c => c.fc_NumeroCompra)
                .HasColumnType("varchar")
                .HasMaxLength(250);

                builder.Property(c => c.fn_DescuentoPorcentaje)
                .HasColumnType("decimal(10,2)");

                builder.Property(c => c.fn_DescuentoCantidad)
                .HasColumnType("decimal(10,2)");

                builder.Property(c => c.fd_FechaCompra)
                .HasColumnType("datetime")
                .IsRequired();

                builder.HasOne<MetodoPago>(c => c.MetodoPago)
                .WithMany()
                .HasForeignKey($"fi_IdCatMetodoPago");

                builder.HasOne<Comprobante>(c => c.Comprobante)
                .WithMany()
                .HasForeignKey($"fi_IdCatComprobante");

                builder.HasOne<Proveedor>(c => c.Proveedor)
                .WithMany()
                .HasForeignKey($"fi_IdCatProveedor");

                builder.Property(c => c.fn_Total)
                .HasColumnType("decimal(10,2)")
                .IsRequired();                

                builder.Property(c => c.fb_Estatus)
                .IsRequired();
            });

            modelBuilder.Entity<CompraDetalle>(builder =>
            {
                builder.ToTable($"tbl_CnfComprasDetalle", $"Compras");

                builder.HasKey(c => c.fi_IdCnfCompraDetalle);

                builder.HasIndex(c => c.fi_IdCnfCompraDetalle)
                    .IsUnique();

                builder.HasOne<Compra>(c => c.Compra)
                .WithMany()
                .HasForeignKey($"fi_IdCnfCompra");

                builder.HasOne<Producto>(c => c.Producto)
                .WithMany()
                .HasForeignKey($"fi_IdCatProducto");

                builder.Property(c => c.fi_Cantidad)
                .IsRequired();

                builder.Property(c => c.fn_PrecioCompra)
                .HasColumnType("decimal(10,2)")
                .IsRequired();               

                builder.Property(c => c.fn_Total)
                .HasColumnType("decimal(10,2)")
                .IsRequired();

                builder.Property(c => c.fb_Estatus)
                .IsRequired();
            });
            #endregion

            #region Configuracion            
            modelBuilder.Entity<General>(builder =>
            {
                builder.ToTable($"tbl_CnfGeneral", $"Configuracion");

                builder.HasKey(c => c.fi_IdCnfGeneral);

                builder.HasIndex(c => c.fi_IdCnfGeneral)
                    .IsUnique();

                builder.Property(c => c.fc_NombreEmpresa)
                .HasColumnType("varchar")
                .IsRequired()
                .HasMaxLength(250);

                builder.Property(c => c.fc_Logo)                
                .IsRequired();                
            });

            modelBuilder.Entity<Comprobante>(builder =>
            {
                builder.ToTable($"tbl_CatComprobantes", $"Configuracion");

                builder.HasKey(c => c.fi_IdCatComprobante);

                builder.HasIndex(c => c.fi_IdCatComprobante)
                    .IsUnique();

                builder.Property(c => c.fc_NombreComprobante)
                .HasColumnType("varchar")
                .IsRequired()
                .HasMaxLength(250);

                builder.Property(c => c.fb_Estatus)
                .IsRequired();
            });

            modelBuilder.Entity<MetodoPago>(builder =>
            {
                builder.ToTable($"tbl_CatMetodosPago", $"Configuracion");

                builder.HasKey(c => c.fi_IdCatMetodoPago);

                builder.HasIndex(c => c.fi_IdCatMetodoPago)
                    .IsUnique();

                builder.Property(c => c.fc_NombreMetodoPago)
                .HasColumnType("varchar")
                .IsRequired()
                .HasMaxLength(250);

                builder.Property(c => c.fb_Estatus)
                .IsRequired();
            });

            modelBuilder.Entity<Usuario>(builder =>
            {
                builder.ToTable($"tbl_CatUsuarios", $"Configuracion");

                builder.HasKey(c => c.fi_IdCatUsuario);

                builder.HasIndex(c => c.fi_IdCatUsuario)
                    .IsUnique();

                builder.Property(c => c.fc_NombreUsuario)
                .HasColumnType("varchar")
                .IsRequired()
                .HasMaxLength(250);

                builder.Property(c => c.fc_Usuario)
                .HasColumnType("varchar")
                .IsRequired()
                .HasMaxLength(20);

                builder.Property(c => c.fi_NivelAcceso)
                .IsRequired();

                builder.Property(c => c.fc_Direccion)
                .HasColumnType("varchar")
                .IsRequired()
                .HasMaxLength(250);

                builder.Property(c => c.fc_Telefono)
                .HasColumnType("varchar")
                .HasMaxLength(10);

                builder.Property(c => c.fc_Password)
                .HasColumnType("varchar")
                .IsRequired()
                .HasMaxLength(15);

                builder.Property(c => c.fb_Estatus)
                .IsRequired();
            });
            #endregion

            #region Ventas
            modelBuilder.Entity<Cliente>(builder =>
            {
                builder.ToTable($"tbl_CatClientes", $"Ventas");

                builder.HasKey(c => c.fi_IdCatCliente);

                builder.HasIndex(c => c.fi_IdCatCliente)
                    .IsUnique();                

                builder.Property(c => c.fc_NombreCliente)
                .HasColumnType("varchar")
                .IsRequired()
                .HasMaxLength(250);

                builder.Property(c => c.fc_RFC)
                .HasColumnType("varchar")
                .HasMaxLength(250);

                builder.Property(c => c.fc_Email)
                .HasColumnType("varchar")
                .HasMaxLength(120);

                builder.Property(c => c.fc_Direccion)
                .HasColumnType("varchar")
                .IsRequired()
                .HasMaxLength(250);

                builder.Property(c => c.fc_Telefono)
                .HasColumnType("varchar")
                .HasMaxLength(10);

                builder.Property(c => c.fb_Estatus)
                .IsRequired();
            });

            modelBuilder.Entity<Venta>(builder =>
            {
                builder.ToTable($"tbl_CnfVentas", $"Ventas");

                builder.HasKey(c => c.fi_IdCnfVenta);

                builder.HasIndex(c => c.fi_IdCnfVenta)
                    .IsUnique();

                builder.Property(c => c.fc_NumeroVenta)
                .HasColumnType("varchar")
                .HasMaxLength(250);

                builder.Property(c => c.fn_DescuentoPorcentaje)
                .HasColumnType("decimal(10,2)");

                builder.Property(c => c.fn_DescuentoCantidad)
                .HasColumnType("decimal(10,2)");

                builder.Property(c => c.fd_FechaVenta)
                .HasColumnType("datetime")
                .IsRequired();

                builder.HasOne<MetodoPago>(c => c.MetodoPago)
                .WithMany()
                .HasForeignKey($"fi_IdCatMetodoPago");

                builder.HasOne<Comprobante>(c => c.Comprobante)
                .WithMany()
                .HasForeignKey($"fi_IdCatComprobante");

                builder.HasOne<Cliente>(c => c.Cliente)
                .WithMany()
                .HasForeignKey($"fi_IdCatCliente");

                builder.Property(c => c.fn_Total)
                .HasColumnType("decimal(10,2)")
                .IsRequired();

                builder.Property(c => c.fb_Estatus)
                .IsRequired();
            });

            modelBuilder.Entity<VentaDetalle>(builder =>
            {
                builder.ToTable($"tbl_CnfVentasDetalle", $"Ventas");

                builder.HasKey(c => c.fi_IdCnfVentaDetalle);

                builder.HasIndex(c => c.fi_IdCnfVentaDetalle)
                    .IsUnique();

                builder.HasOne<Venta>(c => c.Venta)
                .WithMany()
                .HasForeignKey($"fi_IdCnfVenta");

                builder.HasOne<Producto>(c => c.Producto)
                .WithMany()
                .HasForeignKey($"fi_IdCatProducto");

                builder.Property(c => c.fi_Cantidad)
                .IsRequired();

                builder.Property(c => c.fn_PrecioVenta)
                .HasColumnType("decimal(10,2)")
                .IsRequired();

                builder.Property(c => c.fn_Total)
                .HasColumnType("decimal(10,2)")
                .IsRequired();

                builder.Property(c => c.fb_Estatus)
                .IsRequired();
            });
            #endregion
        }
    }
}
