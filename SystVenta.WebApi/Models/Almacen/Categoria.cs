﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystVenta.WebApi.Models.Almacen
{
    public class Categoria
    {
        public int fi_IdCatCategoria { get; set; }        
        public string fc_NombreCategoria { get; set; }
        public bool fb_Estatus { get; set; }
    }
}
