﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystVenta.WebApi.Models.Almacen
{
    public class Marca
    {
        public int fi_IdCatMarca { get; set; }        
        public string fc_NombreMarca { get; set; }
        public bool fb_Estatus { get; set; }
    }
}
