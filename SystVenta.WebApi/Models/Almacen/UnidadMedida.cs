﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystVenta.WebApi.Models.Almacen
{
    public class UnidadMedida
    {
        public int fi_IdCatUnidadMedida { get; set; }        
        public string fc_NombreUnidadMedida { get; set; }
        public bool fb_Estatus { get; set; }
    }
}
