﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystVenta.WebApi.Models.Almacen
{
    public class Producto
    {
        public int fi_IdCatProducto { get; set; }        
        public string fc_Codigo { get; set; }        
        public string fc_NombreProducto { get; set; }        
        public decimal fn_PrecioCompra { get; set; }
        public decimal fn_PrecioVenta { get; set; }
        public int fi_Stock { get; set; }
        public int fi_StockMinimo { get; set; }        
        public Categoria Categoria { get; set; }
        public Marca Marca { get; set; }
        public UnidadMedida UnidadMedida { get; set; }
        public bool fb_Estatus { get; set; }
    }
}
